cmake_minimum_required(VERSION 3.10)

project(FFTTest)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_FLAGS "-march=native -O2 -flto -pedantic -Wno-unknown-pragmas -Wno-unused-parameter")
string(APPEND CMAKE_EXE_LINKER_FLAGS "-fopenmp -lfftw3_omp -lfftw3 ")

find_package(MPI REQUIRED)

find_package(OpenMP)
set(CMAKE_PREFIX_PATH /home/marc/Installs/OpenBLAS)
find_package(LAPACK)
find_package(BLAS)
find_package(HDF5 REQUIRED)


include(CMakeParseArguments)
include(FindPackageHandleStandardArgs)

function(preferential_find_path VAR)
	cmake_parse_arguments(
		preferential_find_path
		""
		""
		"NAMES;PATHS;PATH_SUFFIXES"
		${ARGN}
	)

	string(REPLACE "~" "$ENV{HOME}" PATHS2 "${${preferential_find_path_PATHS}}")

	find_path(${VAR}
		NAMES ${preferential_find_path_NAMES}
		PATHS ${PATHS2}
		PATH_SUFFIXES ${preferential_find_path_PATH_SUFFIXES}
		NO_DEFAULT_PATH
	)
	if (${${VAR}} STREQUAL ${VAR}-NOTFOUND)
		find_path(${VAR}
			NAMES ${preferential_find_path_NAMES}
			PATH_SUFFIXES ${preferential_find_path_PATH_SUFFIXES}
			)
	endif()

	find_package_handle_standard_args(${preferential_find_path_NAMES}
		"Coult NOT find include directory for ${preferential_find_path_NAMES}. Please specify option -D${preferential_find_path_PATHS}=<path>."
		${VAR}
	)
endfunction(preferential_find_path)

function(preferential_find_library VAR)
	cmake_parse_arguments(
		preferential_find_library
		""
		""
		"NAMES;PATHS;PATH_SUFFIXES"
		${ARGN}
	)

	string(REPLACE "~" "$ENV{HOME}" PATHS2 "${${preferential_find_library_PATHS}}")

	find_library(${VAR}
		NAMES ${preferential_find_library_NAMES}
		PATHS ${PATHS2}
		PATH_SUFFIXES ${preferential_find_library_PATH_SUFFIXES}
		NO_DEFAULT_PATH
	)
	if (${${VAR}} STREQUAL ${VAR}-NOTFOUND)
		find_library(${VAR}
			NAMES ${preferential_find_library_NAMES}
			PATH_SUFFIXES ${preferential_find_library_PATH_SUFFIXES}
			)
	endif()
	find_package_handle_standard_args(${preferential_find_library_NAMES}
		"Coult NOT find library ${preferential_find_library_NAMES}. Please specify option -D${preferential_find_library_PATHS}=<path>."
		${VAR}
	)
endfunction(preferential_find_library)


preferential_find_path(FFTW_INCLUDE_DIRS
	NAMES fftw3.h
	PATHS FFTWPath
	PATH_SUFFIXES include api inc include/x86_64 include/x64
	)


preferential_find_library(FFTW_LIBRARIES
	NAMES fftw3_omp
	PATHS FFTWPath
	PATH_SUFFIXES lib lib64 lib/x86_64 lib/x64 lib/x86
	)

preferential_find_path(HIGHFIVE_INCLUDE_DIRS
	NAMES highfive/H5File.hpp
	PATHS HighFivePath
    )

preferential_find_path(TOMLPLUSPLUS_INCLUDE_DIRS
	NAMES toml.hpp
	PATHS TomlPlusPlusPath
    )

preferential_find_path(PCG_INCLUDE_DIRS
	NAMES pcg_random.hpp
	PATHS PCGPath
	PATH_SUFFIXES include
    )


message(STATUS ${BLAS_INCLUDE_DIRS})


if(LAPACK_FOUND AND BLAS_FOUND)
	set(lapackblas_libraries ${BLAS_LIBRARIES} ${LAPACK_LIBRARIES})
endif()

include_directories(. "/home/marc/Installs/OpenBLAS/include/" ${BLAS_INCLUDE_DIRS} ${HIGHFIVE_INCLUDE_DIRS} ${TOMLPLUSPLUS_INCLUDE_DIRS} ${PCG_INCLUDE_DIRS} src src/obdm src/matrix src/workspace src/HSfield src/rng src/toml-io src/tests src/analysis src/HStransforms)

add_subdirectory(src)

add_subdirectory(src/tests)

add_executable(Metropolis main-metropolis.cpp)
set_property(TARGET Metropolis PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
target_link_libraries(Metropolis PUBLIC metropolis_pp tempering ${lapackblas_libraries} ${FFTW_LIBRARIES} ${HDF5_LIBRARIES} MPI::MPI_CXX)

add_executable(Analysis main-analysis.cpp)
set_property(TARGET Analysis PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
target_link_libraries(Analysis PUBLIC metropolis_pp analysis ${lapackblas_libraries} ${FFTW_LIBRARIES} ${HDF5_LIBRARIES} MPI::MPI_CXX)
add_executable(Canonical main-canonical.cpp)
set_property(TARGET Canonical PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
target_link_libraries(Canonical PUBLIC metropolis_pp analysis ${lapackblas_libraries} ${FFTW_LIBRARIES} ${HDF5_LIBRARIES} MPI::MPI_CXX)

