# Cold Atoms on the Lattice

This repository provides functionality for sampling and analysis of cold atoms systems on a space time lattice in one to three spatial dimensions. 

### External dependencies
- [TomlPlusPlus](https://marzer.github.io/tomlplusplus/)
- [HighFive](https://github.com/BlueBrain/HighFive)
- [PCG](https://www.pcg-random.org/index.html)
- HDF5
- FFTW3
- BLAS
- LAPACK(e)
- CMake -- for compiling
