#include "params.h"

#include <mpi.h>
#include <omp.h>
#include <vector>
#include <complex>
#include <iomanip>
#include <iostream>

#include "rng.h"
#include "obdm.h"
#include "toml-io.h"
#include "workspace.h"
#include "measureTime.h"
#include "analysis.h"
#include "HStransforms.h"


template <typename T>
void RunTier1(Parameters const& params, HSTransform<T>& HStransform) {

    Observables<T> Obs(params, 1e-5, HStransform);

    int num_Configs = Obs.hst.GetHSfield().GetNumberOfConfigs();

    std::cout << "Init Obs" << std::endl;

    std::vector<cdouble> tmpDiagonal(params.Volume);
    std::vector<cdouble> tmpDiagonal2(params.Volume);
    std::vector<cdouble> tmpMatrix(params.Volume* params.Volume);
    std::vector<cdouble> Sign(1);

    //Statistics
    statistics PosDensStats_up(params.Volume);
    statistics PosDensStats_up_reweight(params.Volume);
    statistics MomDensStats_up(params.Volume);
    statistics PosDensStats_dn(params.Volume);
    statistics PosDensStats_dn_reweight(params.Volume);
    statistics MomDensStats_dn(params.Volume);
    statistics DDCorrelatior(params.Volume * params.Volume);
    statistics PositionDDCorrelator(params.Volume * params.Volume);
    statistics AverageSign(1);

    statistics PairMomentumCorrelator(params.Volume * params.Volume);
    statistics OnSitePositionPair(params.Volume * params.Volume);
    statistics OnSiteMomentumPair(params.Volume * params.Volume);

    statistics MomentumSapceMixedTerm_Up(params.Volume * params.Volume);
    statistics MomentumSapceMixedTerm_Dn(params.Volume * params.Volume);


    autocorrelation AcorrPos(num_Configs, params.Volume);
    int const rank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();

    std::ofstream ErrorFile(std::to_string(rank) + "_ErrourOutputs.txt");


    //need a way to get the number of samples from the HDF file
    for (int i = 0; i < num_Configs; i++){
        std::cout << "Processing configuration " << i << " of " << num_Configs << "\n";
        Obs.LoadNextConfiguration();

        std::cout << "LoadNextConfiguration" << std::endl;

        Sign[0] = Obs.ComputeSign();
        AverageSign.AddData(Sign);

        std::cout << "Sign of the current configuration is " <<  Sign[0] << "\n";

        Obs.ComputePositionDensity(tmpDiagonal, tmpDiagonal2);

        std::cout << "ComputePositionDensity" << std::endl;

        PosDensStats_up.AddData(tmpDiagonal);
        PosDensStats_dn.AddData(tmpDiagonal2);

        AcorrPos.AddData(i, tmpDiagonal);

        for (size_t j = 0; j < tmpDiagonal.size(); j++){
            tmpDiagonal[j] *= Sign[0];
            tmpDiagonal2[j] *= Sign[0];
        }

        PosDensStats_up_reweight.AddData(tmpDiagonal);
        PosDensStats_dn_reweight.AddData(tmpDiagonal2);

        Obs.ComputeDensityDensity(tmpMatrix);
        PositionDDCorrelator.AddData(tmpMatrix);

        Obs.OnSitePairPosition(tmpMatrix);

        bool AddThis = true;
        for (size_t j = 0; j < tmpMatrix.size();j++){
            tmpMatrix[j] *= Sign[0];
            if (std::abs(tmpMatrix[j]) > 10000){
                ErrorFile << "OnSitePairPosition " << " " << tmpMatrix[j] << " in configuration #" << i << " at pos " << j << "\n";
                AddThis = false;
            }
        }

        if (AddThis){
            OnSitePositionPair.AddData(tmpMatrix);
        }

        Obs.ComputTwoBodyMixedTerm(tmpMatrix, false);
        MomentumSapceMixedTerm_Up.AddData(tmpMatrix);


        Obs.ComputeMomentumDensityFromPosition(tmpDiagonal, tmpDiagonal2);
        MomDensStats_up.AddData(tmpDiagonal);
        MomDensStats_dn.AddData(tmpDiagonal2);

        Obs.ComputeDensityDensity(tmpMatrix);
        DDCorrelatior.AddData(tmpMatrix);

        Obs.PairMomentumDensity(tmpMatrix);
        PairMomentumCorrelator.AddData(tmpMatrix);

        //on site now in mom sapce
        Obs.OnSitePairPosition(tmpMatrix);
        OnSiteMomentumPair.AddData(tmpMatrix);

        Obs.ComputTwoBodyMixedTerm(tmpMatrix, true);
        MomentumSapceMixedTerm_Dn.AddData(tmpMatrix);
    }

    DDCorrelatior.Normalize();
    PosDensStats_up.Normalize();
    PosDensStats_up_reweight.Normalize();
    MomDensStats_up.Normalize();
    PosDensStats_dn.Normalize();
    PosDensStats_dn_reweight.Normalize();
    MomDensStats_dn.Normalize();
    PositionDDCorrelator.Normalize();
    PairMomentumCorrelator.Normalize();
    AverageSign.Normalize();
    OnSitePositionPair.Normalize();
    OnSiteMomentumPair.Normalize();
    MomentumSapceMixedTerm_Up.Normalize();
    MomentumSapceMixedTerm_Dn.Normalize();

    AcorrPos.ComputeAutocorrelation();
    
    int num_configs = Obs.hst.GetHSfield().GetNumberOfConfigs();
    for (int i = 0; i < params.Volume; i++){
        //std::cout << i <<"\n";
        std::transform(AcorrPos.data.begin() + i*num_configs, AcorrPos.data.begin() + (i+1)*num_configs,
                        AcorrPos.data.begin() + i*num_configs,
                        [i, &PosDensStats_up, &num_configs](cdouble x){
                        return x - std::sqrt(num_configs) * PosDensStats_up.Mean[i] * std::conj(PosDensStats_up.Mean[i]);
                        });
    }

    const std::string outfile = std::to_string(rank) + "_Observables.hdf5";
    DDCorrelatior.OutputHDF5(outfile, "DensityDensityCorrelator", params.Volume, params.Volume);
    PosDensStats_up.OutputHDF5(outfile, "Density_up");
    PosDensStats_up_reweight.OutputHDF5(outfile, "Density_up_reweight");
    MomDensStats_up.OutputHDF5(outfile, "Mom_Density_up");
    PosDensStats_dn.OutputHDF5(outfile, "Density_dn");
    PosDensStats_dn_reweight.OutputHDF5(outfile, "Density_dp_reweight");
    MomDensStats_dn.OutputHDF5(outfile, "Mom_Density_dn");
    PositionDDCorrelator.OutputHDF5(outfile, "PositionDensityDensityCorrelator", params.Volume, params.Volume);
    PairMomentumCorrelator.OutputHDF5(outfile, "PairMomentumCorrelator", params.Volume, params.Volume);
    AverageSign.OutputHDF5(outfile, "AverageSign");
    OnSitePositionPair.OutputHDF5(outfile, "OnSitePositionPair", params.Volume, params.Volume);
    OnSiteMomentumPair.OutputHDF5(outfile, "OnSiteMomentumPair", params.Volume, params.Volume);

    MomentumSapceMixedTerm_Up.OutputHDF5(outfile, "MomentumSapceMixedTerm_Up", params.Volume, params.Volume);
    MomentumSapceMixedTerm_Dn.OutputHDF5(outfile, "MomentumSapceMixedTerm_Dn", params.Volume, params.Volume);

    AcorrPos.OutputHDF5(outfile, "Density_up/PositionDensityAcorr");

}


template <typename T>
void RunTier2(Parameters const& params, HSTransform<T>& HStransform) {
    Observables<T> Obs(params, 1e-6, HStransform);

    int num_Configs = Obs.hst.GetHSfield().GetNumberOfConfigs();

    std::vector<cdouble> Mean_up(params.Volume);
    std::vector<cdouble> Mean_down(params.Volume);
    std::vector<cdouble> tmp(params.Volume);
    std::vector<cdouble> tmp2(params.Volume);
    std::vector<cdouble> tmpMatrix(params.Volume* params.Volume);


    //Statistics
    statistics ShotNoise(params.Volume * params.Volume);

    autocorrelation PositionAcorr(Obs.hst.GetHSfield().GetNumberOfConfigs(), params.Volume);

	int const rank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();
    const std::string outfile = std::to_string(rank) + "_Observables.hdf5";
    //Load the Momentum Density from the HDF file
    {
    HighFive::File file(outfile, HighFive::File::ReadOnly);
    auto group = file.getGroup("Mom_Density_up");
    auto dataset = group.getDataSet("Mean");
    dataset.read(Mean_up);
    auto group2 = file.getGroup("Mom_Density_dn");
    auto dataset2 = group2.getDataSet("Mean");
    dataset2.read(Mean_down);
    }


    //need a way to get the number of samples from the HDF file
    for (int i = 0; i < Obs.hst.GetHSfield().GetNumberOfConfigs(); i++){
        std::cout << "Processing configuration " << i << " of " << Obs.hst.GetHSfield().GetNumberOfConfigs() << "\n";
        Obs.LoadNextConfiguration();

        Obs.ComputePositionDensity(tmp, tmp2);

        Obs.ComputeShotNoiseDirect(tmpMatrix ,Mean_up , Mean_down);

        ShotNoise.AddData(tmpMatrix);
    }

    ShotNoise.Normalize();

    ShotNoise.OutputHDF5(outfile, "ShotNoise", params.Volume, params.Volume);

    PositionAcorr.OutputHDF5(outfile, "Density_up/PositionDensityAcorr");
}

template <BP bp, HST hs>
void Run(Parameters params){
	int const rank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();
    const std::string outfile = std::to_string(rank) + "_Observables.hdf5";
    std::ifstream file(outfile);
    if (!file) {
        std::cout << "Creating file " << outfile << "\n";
        HighFive::File HDFfile(outfile, HighFive::File::Create);
        params.SaveToHDF(outfile);
    }
    else {
        std::cout << "Opening file " << outfile << "\n";
    }
    
    //Check if parameters exist in the HDF file
    bool MomDensity_exists = false;
    bool ShotNoise_exists = false;
    {
        HighFive::File HDFfile(outfile, HighFive::File::ReadWrite);
        if (HDFfile.exist("parameters") == false)
            params.SaveToHDF(outfile);
        else {
            std::cout << "Parameters already exist in file, reading parameters\n";
            params.ReadFromHDF(outfile);
        }
        if (HDFfile.exist("Mom_Density_up") == true && HDFfile.exist("Mom_Density_dn") == true)
            MomDensity_exists = true;
        if (HDFfile.exist("ShotNoise") == true)
            ShotNoise_exists = true;
    }


	typename HSTTraits<bp, hs>::type hst{params};

    if (MomDensity_exists == false){
        std::cout << "MomDensity does not exist, running tier 1\n";
        RunTier1(params, hst);
    }
    else {
        std::cout << "MomDensity exists, skipping tier 1\n";
    }

    if (ShotNoise_exists == false){
        std::cout << "ShotNoise does not exist, running tier 2\n";
        RunTier2(params, hst);
    }
    else {
        std::cout << "ShotNoise exists, skipping tier 2\n";
    }

}

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Please provide input configuration file.\nExiting.\n";
		exit(1);
	}
	MPI_Init(NULL, NULL);
	fftw_init_threads();
	fftw_plan_with_nthreads(1);//omp_get_max_threads());
	#pragma omp barrier

    #if __has_include(<mkl.h>)
    #else
    std::cout << "FFTW planner threads: " << fftw_planner_nthreads() << std::endl;
    #endif
    
	std::cout << "\n";
    
    auto params = Parameters(argv[1], true);
    
    WS::Initialise(params);
	RNG::Initialise(params.seed);

    std::cout << "Number of Unique OBDMs " << WS::UniqueOBDMs() << std::endl;

    //Put something to check if the HDF file exists
    //If it does, then run the Tier2

	if (params.muUp == params.muDn)
		Run<BP::Balanced, HST::Discrete>(params);
	else
		Run<BP::Polarised, HST::Discrete>(params);

    WS::Finalise();
    
	fftw_cleanup_threads();
	std::cout << "FFT final\n\n";

	MPI_Finalize();

	return 0;
}
