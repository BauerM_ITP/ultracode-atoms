#include "params.h"

#include "mpi.h"
#include <omp.h>
#include <vector>
#include <iomanip>
#include <complex>
#include <iostream>

#include "rng.h"
#include "obdm.h"
#include "toml-io.h"
#include "workspace.h"
#include "measureTime.h"
#include "analysis.h"

template <typename T>
void RunTier1(Parameters const& params, HSTransform<T>& hst) {
    CanonicalObservables<T> CanObs(params, 1e-6, hst , params.TargetN_up, params.TargetN_down, params.FourierMax_up, params.FourierMax_down);

    int num_Configs = CanObs.hst.GetHSfield().GetNumberOfConfigs();

    statistics WeightRatio((params.n_max_up - params.n_min_up + 1) * (params.n_max_down - params.n_min_down + 1));;
    statistics Density_up(params.Volume);
    statistics Density_dn(params.Volume);
    statistics Mom_Density_up(params.Volume);
    statistics Mom_Density_dn(params.Volume);
    statistics DnsityDensity(params.Volume * params.Volume);
    statistics PositionDensityDensity(params.Volume * params.Volume);
    statistics Energy(1);

    std::vector<cdouble> Density_up_tmp(params.Volume);
    std::vector<cdouble> Density_dn_tmp(params.Volume);
    std::vector<cdouble> Mom_Density_up_tmp(params.Volume);
    std::vector<cdouble> Mom_Density_dn_tmp(params.Volume);
    std::vector<cdouble> DnsityDensity_tmp(params.Volume * params.Volume);
    std::vector<cdouble> WeightRatioTmp((params.n_max_up - params.n_min_up + 1) * (params.n_max_down - params.n_min_down + 1));
    std::vector<cdouble> EnergyTmp(1);

    int const rank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();

    const int start_config = 0;

    for (int i = start_config; i < num_Configs; i++){

        if (rank == 0){
            std::cout << "\n\nProcessing configuration " << i << " of " << num_Configs << "\n";
        }
        CanObs.LoadNextConfiguration(start_config);

        EnergyTmp[0] = 0;
        CanObs.ComputeWeightRatios(WeightRatioTmp, params.n_min_up, params.n_max_up, params.n_min_down, params.n_max_down);
        MeasureTimeX<1>([&]{
        CanObs.ComputeFourierTwoBody();
        }, "ComputeFourierTwoBody");
        CanObs.GetPositionDensity(Density_up_tmp, Density_dn_tmp);

        cdouble UP_sum=0;
        //compute sum over CanObs.TempDiag
        for (int x = 0; x < params.Volume; x++){
            UP_sum += Density_up_tmp[x].real();
        }

        if (rank == 0){
            std::cout << "Approx overlap " << UP_sum.real() / double (params.TargetN_up) << std::endl;
        }

        if (rank == 0){
            std::cout << "Target PNum check " << UP_sum / CanObs.Tmp_Weight_down / CanObs.Tmp_Weight_up << std::endl;
            std::cout << "Temp Weight " <<  CanObs.Tmp_Weight_down * CanObs.Tmp_Weight_up << std::endl;
        }

        cdouble WeightSum = 0;

        for (auto& w : WeightRatioTmp){
            WeightSum += w;
            if (w.real() < 0 && abs(w.real()) > 1e-6){
                std::cout << "Negative weight " << w << std::endl;
            }
        }

        if (rank == 0){
            std::cout << "WeightSum " << WeightSum << std::endl;

            std::cout << "Target Weight" << WeightRatioTmp[params.TargetN_down + (params.n_max_down - params.n_min_down + 1) * params.TargetN_up] << std::endl;
        }

        CanObs.AddPotentialEnergy(EnergyTmp[0], Density_up_tmp, Density_dn_tmp);
        CanObs.GetDensityDensiy(DnsityDensity_tmp);
        PositionDensityDensity.AddData(DnsityDensity_tmp);
        CanObs.GetMomentumDensity(Mom_Density_up_tmp, Mom_Density_dn_tmp);
        CanObs.AddKineticEnergy(EnergyTmp[0], Mom_Density_up_tmp, Mom_Density_dn_tmp);
        CanObs.GetDensityDensiy(DnsityDensity_tmp);
        DnsityDensity.AddData(DnsityDensity_tmp);


        WeightRatio.AddData(WeightRatioTmp);
        Density_up.AddData(Density_up_tmp);
        Density_dn.AddData(Density_dn_tmp);
        Mom_Density_up.AddData(Mom_Density_up_tmp);
        Mom_Density_dn.AddData(Mom_Density_dn_tmp);
        Energy.AddData(EnergyTmp);
    }

    WeightRatio.Normalize();
    Density_up.Normalize();
    Density_dn.Normalize();
    Mom_Density_up.Normalize();
    Mom_Density_dn.Normalize();
    DnsityDensity.Normalize();
    PositionDensityDensity.Normalize();
    Energy.Normalize();


    const std::string outfile = std::to_string(rank) + "_CanonicalObservables.hdf5";
    WeightRatio.OutputHDF5(outfile, "WeightRatio", params.n_max_up - params.n_min_up + 1, (params.n_max_down - params.n_min_down + 1));
    Density_up.OutputHDF5(outfile, "Density_up");
    Density_dn.OutputHDF5(outfile, "Density_dn");
    Mom_Density_up.OutputHDF5(outfile, "Mom_Density_up");
    Mom_Density_dn.OutputHDF5(outfile, "Mom_Density_dn");
    DnsityDensity.OutputHDF5(outfile, "DensityDensityCorrelator", params.Volume, params.Volume);
    Energy.OutputHDF5(outfile, "Energy");

}

template <typename T>
void RunTier2(Parameters const& params, HSTransform<T>& hst) {
    CanonicalObservables<T> CanObs(params, 1e-6, hst , params.TargetN_up, params.TargetN_down, params.FourierMax_up, params.FourierMax_down);

    int num_Configs = CanObs.hst.GetHSfield().GetNumberOfConfigs();

    std::vector<cdouble> Mean_up(params.Volume);
    std::vector<cdouble> Mean_dn(params.Volume);
    std::vector<cdouble> WeightRatio((params.n_max_up - params.n_min_up + 1) * (params.n_max_down - params.n_min_down + 1));

    std::vector<cdouble> tmpMatrix(params.Volume* params.Volume);

    //Statistics
    statistics ShotNoise(params.Volume * params.Volume);
	int const rank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();
    const std::string outfile = std::to_string(rank) + "_CanonicalObservables.hdf5";

    {
    HighFive::File file(outfile, HighFive::File::ReadOnly);
    auto group_up = file.getGroup("Mom_Density_up");
    auto dataset_up = group_up.getDataSet("Mean");
    dataset_up.read(Mean_up);

    auto group_dn = file.getGroup("Mom_Density_dn");
    auto dataset_dn = group_dn.getDataSet("Mean");
    dataset_dn.read(Mean_dn);

    auto group_weight = file.getGroup("WeightRatio");
    auto dataset_weight = group_weight.getDataSet("Mean");
    dataset_weight.read(WeightRatio.data());
    }
    
    //DN major layout for the Weight Ratios
    int Weight_position_up = params.TargetN_up - params.n_min_up;
    int Weight_position_dn = params.TargetN_down - params.n_min_down;

    for (int i = 0; i < params.Volume; i++){
            Mean_dn[i] /= WeightRatio[Weight_position_dn + (params.n_max_down - params.n_min_down + 1)* Weight_position_up];
            Mean_up[i] /= WeightRatio[Weight_position_dn + (params.n_max_down - params.n_min_down + 1) * Weight_position_up];
    }

    std::cout << WeightRatio[Weight_position_up + (params.n_max_up - params.n_min_up + 1) * Weight_position_dn] << std::endl;

    //for now only compute the density Density correlator, shot noise direectly is a massive headache
    
    for (int i = 0; i < num_Configs; i++){
        //std::cout << "Processing configuration " << i << " of " << num_Configs << "\n";
        CanObs.LoadNextConfiguration();

        CanObs.ComputeFourierTwoBody();
  
        CanObs.GetShotNoise(tmpMatrix, Mean_up, Mean_dn);
        ShotNoise.AddData(tmpMatrix);

    }

    ShotNoise.Normalize();

    ShotNoise.OutputHDF5(outfile, "ShotNoise", params.Volume, params.Volume);
}

template <BP bp, HST hs>
void Run(Parameters params){
    //chek if file with name CanonicalObservables.hdf5 exists, if not create it and write parameters,
    //if it does exist, open it in ReadWrite mode
    int const rank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();
    const std::string outfile = std::to_string(rank) + "_CanonicalObservables.hdf5";
    std::ifstream file(outfile);
    if (!file) {
        std::cout << "Creating file " << rank << "_CanonicalObservables.hdf5\n";
        HighFive::File HDFfile(outfile, HighFive::File::Create);
        params.SaveToHDF(outfile);
    }
    else {
        std::cout << "Opening file " << rank << "_CanonicalObservables.hdf5\n";
    }
    
    //Check if parameters exist in the HDF file
    bool MomDensity_exists = false;
    bool ShotNoise_exists = false;
    {
        HighFive::File HDFfile(outfile, HighFive::File::ReadWrite);
        if (HDFfile.exist("parameters") == false)
            params.SaveToHDF(outfile);
        else {
            std::cout << "Parameters already exist in file, reading parameters\n";
            params.ReadFromHDF(outfile);
        }
        if (HDFfile.exist("Mom_Density_up") == true && HDFfile.exist("Mom_Density_dn") == true)
            MomDensity_exists = true;
        if (HDFfile.exist("ShotNoise") == true)
            ShotNoise_exists = true;
    }


	typename HSTTraits<bp, hs>::type hst{params};

    if (MomDensity_exists == false){
        std::cout << "MomDensity does not exist, running tier 1\n";
        RunTier1(params, hst);
    }
    else {
        std::cout << "MomDensity exists, skipping tier 1\n";
    }

    if (ShotNoise_exists == false){
        std::cout << "ShotNoise does not exist, running tier 2\n";
        RunTier2(params, hst);
    }
    else {
        std::cout << "ShotNoise exists, skipping tier 2\n";
    }

        std::cout << rank << std::endl;

    int const rankP = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();

    std::cout << rankP <<  " finished" <<std::endl;

}

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Please provide input configuration file.\nExiting.\n";
		exit(1);
	}
	MPI_Init(NULL, NULL);
	fftw_init_threads();
	fftw_plan_with_nthreads(1);//omp_get_max_threads());
	#pragma omp barrier

    #if __has_include(<mkl.h>)
    #else
    std::cout << "FFTW planner threads: " << fftw_planner_nthreads() << std::endl;
    #endif
    
	std::cout << "\n";
    
    auto params = Parameters(argv[1], true);
    
    WS::Initialise(params);
	RNG::Initialise(params.seed);

	if (params.muUp == params.muDn)
		Run<BP::Balanced, HST::Discrete>(params);
	else
		Run<BP::Polarised, HST::Discrete>(params);

    WS::Finalise();
    
	fftw_cleanup_threads();
	std::cout << "FFT final\n\n";

	MPI_Finalize();

	return 0;
}
