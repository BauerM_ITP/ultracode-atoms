/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "params.h"

#include "mpi.h"
#include <omp.h>
#include <vector>
#include <iomanip>
#include <complex>
#include <iostream>

#include "rng.h"
#include "obdm.h"
#include "toml-io.h"
#include "workspace.h"
#include "measureTime.h"

#include "HStransforms.h"


//
template <typename T>
void Metropolis(HSTransform<T>& hst, int const nSweeps, double const threshold);
//
template <typename T>
void Temper(HSTransform<T>& hst, double const threshold);

template <BP bp, HST hs>
void Run(Parameters const& params) {
	typename HSTTraits<bp, hs>::type test{params};

    std::cout << "Is resume true? " << params.resumeFromHDF << "\n";

	if (params.resumeFromHDF == true) {
        std::cout << "Reading from HDF metr\n";
		test.hsfield.ReadLastConfigFromHDF();
		RNG::ReadState(params.hdfOutName);
	}
    else
        test.hsfield.BiasedRandom();
	//Temper(test, 10e-1);
    std::cout << "start Metropolis\n"; 
    std::cout << "using threshold: " << 1e-5 << "\n";

	Metropolis(test, params.NumberOfSweeps, 1e-5);

	int const rank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();
    std::cout << rank << " finished Metropolis\n";
}

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Please provide input configuration file.\nExiting.\n";
		exit(1);
	}

	MPI_Init(NULL, NULL);
    omp_set_num_threads(1);
	fftw_init_threads();
	fftw_plan_with_nthreads(1);//omp_get_max_threads());
	#pragma omp barrier

	#if __has_include(<mkl.h>)
	#else
	std::cout << "FFTW planner threads: " << fftw_planner_nthreads() << std::endl;
	#endif

    //set openblas number of threads
    #if __has_include(<mkl.h>)
    mkl_set_num_threads(omp_get_max_threads());
    #else
    //openblas_set_num_threads(1);
    #endif

	
	std::cout << "\n";

	auto params = Parameters(argv[1]);
	if (params.useParamsFromHDF == true){
		params.ReadFromHDF();
        std::cout << "Reading from HDF\n";
    }
	else
		params.SaveToHDF();

	WS::Initialise(params);

    WS::InitialisePP(params);
	RNG::Initialise(params.seed);

	std::cout << "Coupling U" << params.U << "\n"; 

	for (auto e : params.Sizes) std::cout << e << '\t';
	std::cout <<"\n\n\n\n"<< params.Volume << '\n';

	if (params.muUp == params.muDn)
		Run<BP::Balanced, HST::Discrete>(params);
	else
		Run<BP::Polarised, HST::Discrete>(params);


	WS::Finalise();

	fftw_cleanup_threads();
	std::cout << "FFT final\n\n";

	MPI_Finalize();

	return 0;
}
