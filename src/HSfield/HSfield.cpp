/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>

#include "rng.h"
#include "HSfield.h"
#include "highfive/H5File.hpp"

using namespace std::chrono_literals;
namespace HF = HighFive;

//General note: some of these 'Filling' operations could be defined as directly acting on a matrix,
//so the step of actually filling into another space in memory might be avoided

/*
//Fill values for the external potential
void FillExternalPotential(std::vector<double> &ExPot){
	for (int ii = 0; ii < ExPot.size(); ++ii) {
		double val = 0.0;

		double const x = static_cast<double>(ii) - static_cast<double>(ExPot.size()) / 2;

		val += x*x*omega;
		ExPot[ii] = std::exp(-val);
	}
}
*/

/*
//Fills the interaction Potential at a given time slice, this should have options to fill 
//for various HS transforms, this is unsafe if TimeSlice is out of range!
void FillPotential(cdouble* OutPot, double* ExPot, cdouble* PhiIn, int TimeSlice){
	#pragma omp prallel for 
	for (int i = 0; i < Msize; i++){
		OutPot[i] = (1.0 + g * sin(PhiIn[i + TimeSlice * Msize])) * ExPot[i];
	}
}

//CHECK IF THIS IS A CORRECT TRANSFORM!
void FillPotentialDiscrete(cdouble* OutPot, double* ExPot, cdouble* PhiIn, int TimeSlice){
	//#pragma omp prallel for 
	for (int i = 0; i < Msize; i++){
		OutPot[i] = exp(((gam * PhiIn[i + TimeSlice * Msize] - anisotropy * U / 2.0))) * ExPot[i];
	}
}

//Maybe its better to fill potential batch wise?
void FillPartialPotentialDiscrete(cdouble* OutPot, double* ExPot, cdouble* PhiIn, int t0, int tmax){
	for (int TimeSlice = t0; TimeSlice < tmax; TimeSlice++){
		for (int i = 0; i < Msize; i++){
			OutPot[TimeSlice * Msize + i] = (exp((gam * PhiIn[TimeSlice * Msize + i] - anisotropy * U)/2.0)) * ExPot[i];
		}
	}
}
*/

HSfield::HSfield(Parameters p)
	: params{p}
	, Phi{std::vector<signed char>(params.Volume*params.Nt)}
	, Proposal{std::vector<signed char>(params.Volume)}
    , GlobalProposal{std::vector<signed char>(params.Volume*params.Nt)}
	, PotentialPrime{DiagonalMatrix<cdouble>{params.Volume}}
	, Potential{std::vector<cdouble>(params.Volume)}
	, FullPotential{std::vector<cdouble>(params.Volume*params.Nt)}
	, EnergyValues{std::vector<double>(params.Volume)}
	, ExternalPotential{std::vector<double>(params.Volume)}
	, HSWeight{1.0}
	, gam{std::acosh(std::exp(-params.anisotropy * params.U/2.))}
    , A_dens{sqrt(std::exp(-params.anisotropy * params.U) - 1.0)}
{
	SetEnergyValues();
}

void HSfield::SetEnergyValues(){
    std::cout << "Setting K,Vext,Gamma \n";
    gam = std::acosh(std::exp(-params.anisotropy * params.U/2.));

    int const N = EnergyValues.size();
	bool IsHubbard = false;

	if (IsHubbard == false){
	std::cout << "Dispersion set to p^2" << "\n";
		for (int i = 0; i < EnergyValues.size(); i++){
			double p2 = 0;
			int tmp = static_cast<int>(i);
			std::for_each(params.Sizes.crbegin(), params.Sizes.crend(),
					[&tmp, &p2](int const n) {
					int const intMom = tmp % n;
					double const p = (intMom <= std::floor(n/2) ? intMom : intMom - n) * 2.0 * M_PI / n;

                    //std::cout << p << "\n";

					p2 += p*p;
					tmp /= n;
					});

			//EnergyValues[i] = std::exp(-params.anisotropy*((p2 )/(2.0) - params.mu)) / (1.0 * N);
			EnergyValues[i] = std::exp(-params.anisotropy*((p2 )/(2.0))) / (1.0 * N);
		}
	}
	else if (IsHubbard == true){
		std::cout << "Dispersion set to Hubbard" << "\n";
		for (int i = 0; i < EnergyValues.size(); i++){
			double p2 = 0;
			int tmp = static_cast<int>(i);
			std::for_each(params.Sizes.crbegin(), params.Sizes.crend(),
					[&tmp, &p2](int const n) {
					int const intMom = tmp % n;
					double const p = (intMom <= std::floor(n/2) ? intMom : intMom - n) * 2.0 * M_PI / n;

					p2 -= 2.0 * std::cos(p);
					tmp /= n;
					});
			
			EnergyValues[i] = std::exp(-params.anisotropy*p2) / (1.0 * N);
		}
	}

	std::cout << "PERFOMING A TRUNCATION ON THE POTENTIAL \n";

	for (int ii = 0; ii < ExternalPotential.size(); ++ii) {
		std::vector<double> pos(params.Sizes.size(), 0.);
		int index = ii;
		//lattice centered around zero
		std::transform(params.Sizes.crbegin(), params.Sizes.crend(), pos.rbegin(),
							[&index] (const int n) -> double {
							double const x = static_cast<double>(index % n) - static_cast<double>(n)/2.;
							index /= n;
							return x;
						});

		double val = std::accumulate(pos.cbegin(), pos.cend(), 0.0,
			[omega = params.omega](double const oldVal, double const newVal)
			{ return oldVal + omega * newVal*newVal; });
		if (val * params.DecomposeEvery > params.ExtPotentialCutoff) val = 20.0 / params.DecomposeEvery;

		ExternalPotential[ii] = std::exp(-val);
	}

}

//Generates a new proposal and computes the Ratio of the porposal weight of the new and old proposal distribution
//MEGRE THIS WITH FILL PROPOSAL
void HSfield::ForceBiasProposalAndRatio(int Size, cdouble* DensUp, cdouble* DensDn, cdouble &Ratio, int timeSlice) {
	// Draw random numbers according to (e^{\gamma(n_up - n_dn -1))
	Ratio = 1.0;
	for (int i = 0; i < Size; i++){
		if (RNG::Uniform() < (1.0 / (1.0 + exp(-2.*gam*(DensUp[i] + DensDn[i] - 1.0 ))).real()))
			Proposal[i] = 1;
		else
			Proposal[i] = -1;

		//Sign changed here too!!!
		Ratio *= exp(gam * (static_cast<double>(Phi[i + timeSlice * Size] - Proposal[i]))
				* (DensUp[i] + DensDn[i] - 1.0 ));
	}
}

void HSfield::ForceBias_BDM(int Size, cdouble* DensUp, cdouble* DensDn, cdouble &Ratio, int timeSlice) {
    Ratio = 1.0;
    cdouble Norm = 0.0;
        for (int i = 0; i < Size; i++){
            Norm = (1.0 + A_dens * DensUp[i]) * (1.0 + A_dens * DensDn[i]) + (1.0 - A_dens * DensUp[i]) * (1.0 - A_dens * DensDn[i]);
            if (RNG::Uniform() < ((1.0 + A_dens * DensUp[i]) * (1.0 + A_dens * DensDn[i]) / Norm).real())
                Proposal[i] = 1;
            else
                Proposal[i] = -1;

            Ratio *= (1.0+ static_cast<double>(Phi[i + timeSlice * Size]) * A_dens * DensUp[i]) * (1.0 + static_cast<double>(Phi[i + timeSlice * Size]) * A_dens * DensDn[i]) 
                        / ( (1.0+ static_cast<double>(Proposal[i]) * A_dens * DensUp[i]) * (1.0 + static_cast<double>(Proposal[i]) * A_dens * DensDn[i]) );
        }

}

void HSfield::BiasedRandomProposal(int Size, cdouble &Ratio, int timeSlice){
    Ratio = 1.0;
    for (int i = 0; i < Size; i++){
        if (RNG::Uniform() < 0.5)
            Proposal[i] = 1;
        else
            Proposal[i] = -1;

        //Sign changed here too!!!
        Ratio *= 1;
    }
}

//Fills a proposal for the discrete HS transform, use this to flip AND flip back if needed, via 
//LeftMultiplyBy / LeftMultiplyByInverseDiagonal
void HSfield::FillProposalDifferenceDiscrete(int TimeSlice) {
	for (int i = 0; i < Proposal.size(); i++){
		PotentialPrime[i] = exp(gam * (static_cast<double>(Proposal[i] - Phi[i + TimeSlice * params.Volume])));
	}
}

//Proposal Differencec Bulgac, Drut, magerski
void HSfield::FillProposalDifferenceDiscrete_BDM(int TimeSlice) {
    for (int i = 0; i < Proposal.size(); i++){
        PotentialPrime[i] = (1.0 + A_dens * static_cast<double>(Proposal[i])) / (1.0 + A_dens * static_cast<double>(Phi[i + TimeSlice * params.Volume]));
    }
}

void HSfield::ComputeHSRatio(int TimeSlice) {
	HSWeight = 1.0;

	for (int i = 0; i < Proposal.size(); i++){
		HSWeight *= exp(- gam * (static_cast<double>(Proposal[i] - Phi[i + TimeSlice * params.Volume])));
	}
}

void HSfield::ComputeHSRatio_BDM(int TimeSlice) {
    HSWeight = 1.0;
}

void HSfield::ComputeGlobalLogHSRatio() {
    HSWeight = 0.0;

    std::cout << "Computing Global Log HS Ratio not tested" << "\n";

    for (int TimeSlice = 0; TimeSlice < params.Nt; TimeSlice++)
        for (int i = 0; i < Proposal.size(); i++){
            HSWeight -= gam * (static_cast<double>(Phi[i + TimeSlice * params.Volume]));
        }

}

void HSfield::ComputeGlobalLogHSRatio_BDM() {
    HSWeight = 0.0;

}

void HSfield::SaveToHDF() const {
	std::vector<std::size_t> dims;
	dims.push_back(params.Nt);
	for (auto e : params.Sizes) dims.push_back(e);

	bool success = false;
	int tries = 0;
	while (!success && tries < 10) {
		try {
			auto file = HF::File{params.hdfOutName, HF::File::ReadWrite};
			auto group = [&file]{
				if (file.exist("configs") == true)
					return file.getGroup("configs");
				else
					return file.createGroup("configs");
			}();

			std::stringstream name;
			name << "Phi_" << std::setfill('0') << std::setw(8) << group.getNumberObjects();

			auto dataset = group.createDataSet<signed char>(name.str(), HF::DataSpace(dims));
			dataset.write_raw(Phi.data());
			success = true;
		} catch (HF::FileException& e) {
			++tries;
			std::cerr << "# -- Error trying to write data to HDF.\n# -- Retrying.\n";
			std::this_thread::sleep_for(10s);
		}
	}
	if (!success && tries >= 10)
		std::cerr << "# -- Skipped writing configuration due to problems accessing the file.\n";
}

int HSfield::GetNumberOfConfigs() const {
	try {
		HF::File file(params.hdfOutName, HF::File::ReadOnly);
		auto group = file.getGroup("configs");

		return group.getNumberObjects();//group.getNumberObjects();
	} catch (std::exception& e) {
		std::cerr << "# -- No configurations found in HDF file " << params.hdfOutName << "\n";
		std::cerr << "# -- Error message: " << e.what() << '\n';
		return -1;
	}
}

void HSfield::ReadFromHDF(int const configNumber) {
	bool success = false;
	int tries = 0;
    std::cout << "Trying read from HDF" << "\n";
	while (!success && tries < 10) {
		try {
			auto file = HF::File{params.hdfOutName, HF::File::ReadOnly};
			auto group = file.getGroup("configs");

			std::stringstream name;
			name << "Phi_" << std::setfill('0') << std::setw(8) << configNumber;

			auto dataset = group.getDataSet(name.str());
			dataset.read(Phi.data());
			success = true;
            std::cout << "Read from HDF" << "\n"; 
		} catch (std::exception& e) {
			++tries;
			std::cerr << "# -- Error trying to read data from HDF.\n# -- Retrying.\n";
			std::cerr << "# -- Error message: " << e.what() << '\n';
			std::this_thread::sleep_for(10s);
		}
	}
	if (!success && tries >= 10)
		std::cerr << "# -- Gave up on reading configuration due to problems accessing the file.\n";
}

void HSfield::ReadLastConfigFromHDF() {
	int const nConfigs = GetNumberOfConfigs();
	if (nConfigs > 0)
		ReadFromHDF(nConfigs - 1);
}

//Standard density channel HS Transfrom!
void HSfield::FillPotentialDiscrete(int const TimeSlice){
	//#pragma omp prallel for 
	for (int i = 0; i < Potential.size(); i++){
		Potential[i] = exp(((gam * static_cast<double>(Phi[i + TimeSlice * params.Volume]) - params.anisotropy * params.U / 2.0))) * ExternalPotential[i];
	}
}

// HS Transfrom as in Bulgak, Drut, Magerski
void HSfield::FillHsPotentialDiscrrete_BDM(int const TimeSlice){
    //#pragma omp prallel for 
    for (int i = 0; i < Potential.size(); i++){
        Potential[i] =  (1. + A_dens * static_cast<double>(Phi[i + TimeSlice * params.Volume])) * ExternalPotential[i];
    }
}

//Maybe template this into FillPotentialDiscrete
void HSfield::FillHsPotential(int const TimeSlice){
    //#pragma omp prallel for 
    for (int i = 0; i < Potential.size(); i++){
        Potential[i] = exp(((gam * static_cast<double>(Phi[i + TimeSlice * params.Volume]) - params.anisotropy * params.U / 2.0)));
    }
}

// HS Transfrom as in Bulgak, Drut, Magerski
void HSfield::FillHsPotential_BDM(int const TimeSlice){
    //#pragma omp prallel for 
    for (int i = 0; i < Potential.size(); i++){
        Potential[i] =  (1. + A_dens * static_cast<double>(Phi[i + TimeSlice * params.Volume]));
    }
}

void HSfield::Accept(int const TimeSlice) {
	auto const Msize = std::distance(Proposal.cbegin(), Proposal.cend());
	std::copy(Proposal.cbegin(), Proposal.cend(), Phi.begin() + TimeSlice * Msize);
}

void HSfield::SetRandom() {
	for (auto& e : Phi)
		e = (RNG::Normal() > 0.0 ? -1.0 : 1.0);
}

void HSfield::BiasedRandom(){
    double const P_up = (1.0/(1.0 + exp(2.0 * gam))).real();
    std::cout << "P_up = " << P_up << "\n";

    for (auto& e : Phi)
        e = (RNG::Uniform() > P_up ? -1.0 : 1.0);
}

void HSfield::SetAnisotropy(double NewAnisotropy) {
    std::cout << "Changing anisotropy to " <<  NewAnisotropy << "\n";

    params.SetAnisotropy(NewAnisotropy);
    SetEnergyValues();
}
