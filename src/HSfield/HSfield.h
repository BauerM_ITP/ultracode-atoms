/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef HSFIELD_H
#define HSFIELD_H

#include "params.h"
#include <vector>
#include <string>
#include <complex>

#include "toml-io.h"
#include "matrix_diag.h"

struct HSfield {
	Parameters params;

	std::vector<signed char> Phi, Proposal, GlobalProposal;

	DiagonalMatrix<cdouble> PotentialPrime;
	std::vector<cdouble>	Potential, FullPotential;
	std::vector<double>		EnergyValues, ExternalPotential;

	cdouble HSWeight;

	cdouble gam;
    cdouble A_dens;

	HSfield(Parameters  p);
    void SetEnergyValues();
	void ForceBiasProposalAndRatio(int Size, cdouble* DensUp, cdouble* DensDn, cdouble &Ratio, int timeSlice);
    void ForceBias_BDM(int Size, cdouble* DensUp, cdouble* DensDn, cdouble &Ratio, int timeSlice);
    void BiasedRandomProposal(int Size, cdouble &Ratio, int timeSlice);
	void FillProposalDifferenceDiscrete(int TimeSlice);
    void FillProposalDifferenceDiscrete_BDM(int TimeSlice);
	void ComputeHSRatio(int TimeSlice);
    void ComputeHSRatio_BDM(int TimeSlice);
    void ComputeGlobalLogHSRatio();
    void ComputeGlobalLogHSRatio_BDM();
	void FillPotentialDiscrete(int const TimeSlice);
    void FillHsPotentialDiscrrete_BDM(int const TimeSlice);
    void FillHsPotential(int const TimeSlice);
    void FillHsPotential_BDM(int const TimeSlice);
	void Accept(int const TimeSlice);
	void SetRandom();
    void BiasedRandom();
    void SetAnisotropy(double NewAnisotropy);

	void SaveToHDF() const;
	int GetNumberOfConfigs() const;
	void ReadFromHDF(int const configNumber);
	void ReadLastConfigFromHDF();
};
#endif
