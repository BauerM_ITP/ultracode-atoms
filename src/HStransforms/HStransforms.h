/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef HSTRANSFORMS_H
#define HSTRANSFORMS_H

#include <vector>

#include "obdm.h"
#include "HSfield.h"
#include "matrix-diag.h"
#include "workspace.h"

template <typename T>
struct HSTransform {
	// from mini_OBDM

    template <typename T1, typename T2>
	void ConstructDensityMatrix(int const i, CMatrixBase<T1>& Uout, CMatrixBase<T2>& Work) const
	{ static_cast<T const&>(*this).ConstructDensityMatrix(i, Uout, Work); }

	void CopyFromStorage()
	{ static_cast<T&>(*this).CopyFromStorage(); }
	void CopyToStorage() const
	{ static_cast<T const&>(*this).CopyToStorage(); }

	void Reset()
	{ static_cast<T&>(*this).Reset(); }
	void Truncate(std::vector<int> const& truncations)
	{ static_cast<T&>(*this).Truncate(truncations); }
	//

	//
	template <typename dir>
	void Evolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end)
	{ static_cast<T&>(*this).Evolve(begin, end); }
	template <typename dir>
	void Devolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end)
	{ static_cast<T&>(*this).Devolve(begin, end); }
	//
	
	//
	template <typename dir>
	void EvolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work)
	{ static_cast<T&>(*this).EvolveRightByTranspose(begin, end, Work); }
	template <typename dir>
	void DevolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work)
	{ static_cast<T&>(*this).DevolveRightByTranspose(begin, end, Work); }

	void ReverseWrap(int const tmax, int const t0, MatrixFFT& Work)
	{ static_cast<T&>(*this).ReverseWrap(tmax, t0, Work); }
	void Wrap(int const tmax, int const t0, MatrixFFT& Work)
	{ static_cast<T&>(*this).Wrap(tmax, t0, Work); }

	template <typename T1, typename T2>
	void DecomposeEvolution(std::vector<int>& truncations, double tolerance, CMatrixBase<T1>& Work,
			CMatrixBase<T2>& Work2)
	{ static_cast<T&>(*this).DecomposeEvolution(truncations, tolerance, Work, Work2); }
	template <typename T1, typename T2>
	void DecomposeEvolutionRight(std::vector<int>& truncations, double tolerance,
			CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2)
	{ static_cast<T&>(*this).DecomposeEvolutionRight(truncations, tolerance, Work, Work2); }
    template <typename T1, typename T2>
    int DecomposeProduct(double tolerance, mini_OBDM<>& StoredDecomp, CMatrixBase<T1>& Work,
			CMatrixBase<T2>& Work2)
	{ return static_cast<T&>(*this).DecomposeProduct(tolerance, StoredDecomp, Work, Work2); }

	//
	template <typename T1>
	void ComputeReducedDensityMatrix(CMatrixBase<T1>& Work) const
	{ return static_cast<T const&>(*this).ComputeReducedDensityMatrix(Work); }

    template <typename T1>
	void PartialReducedDensityMatrix(CMatrixBase<T1>& Work, const int timeslice)
	{ return static_cast<T&>(*this).PartialReducedDensityMatrix(Work, timeslice); }


	void OnePlusRLD(Matrix& Work) const
	{ return static_cast<T const&>(*this).OnePlusRLD(Work); }

    template <typename T1>
    inline cdouble SignedOnePlusRLD(mini_OBDM<double>& out, CMatrixBase<T1>& Work) const
    { return static_cast<T const&>(*this).SignedOnePlusRLD(out, Work); }


    template <typename T1>
    void PerformEigenDRL(mini_OBDM<cdouble>& out, CMatrixBase<T1>& Work)
	{ return static_cast<T&>(*this).PerformEigenDRL(out, Work); }

    template <typename T1, typename T2>
    void RecoverEigenVectors(mini_OBDM<cdouble>& EigenDecomp, CMatrixBase<T1>& Work1, CMatrixBase<T2>& Work2)
	{ return static_cast<T&>(*this).RecoverEigenVectors(EigenDecomp, Work1, Work2); }
	//

	// -- begin HSfield related functions
	void SetAnisotropy(double newAnisotropy)
	{ static_cast<T&>(*this).SetAnisotropy(newAnisotropy); }

	void ProposeUpdate(cdouble& Ratio, int timeSlice)
	{ static_cast<T&>(*this).ProposeUpdate(Ratio, timeSlice); }
    void ProposeUpdateNaiveBiased(cdouble& Ratio, int timeSlice)
	{ static_cast<T&>(*this).ProposeUpdateNaiveBiased(Ratio, timeSlice); }

	void ApplyUpdate()
	{ static_cast<T&>(*this).ApplyUpdate(); }
	void RevertUpdate()
	{ static_cast<T&>(*this).RevertUpdate(); }
	void Accept(int const TimeSlice)
	{ static_cast<T&>(*this).Accept(TimeSlice); }


	cdouble GetHSWeight() const
	{ return static_cast<T const&>(*this).GetHSWeight(); }

	void SaveToHDF() const
	{ static_cast<T const&>(*this).SaveToHDF(); }
	// -- end HSfield related functions

	HSfield& GetHSfield() { return static_cast<T&>(*this).GetHSfield(); }
	OBDM& GetSpinUpOBDM() { return static_cast<T&>(*this).GetSpinUpOBDM(); }
	OBDM& GetSpinDnOBDM() { return static_cast<T&>(*this).GetSpinDnOBDM(); }
	std::vector<cdouble>& DensUp() { return static_cast<T&>(*this).DensUp(); }
	std::vector<cdouble>& DensDn() { return static_cast<T&>(*this).DensDn(); }
	Parameters& GetParams() { return static_cast<T&>(*this).GetParams(); }
	int NumberOfOBDMs() const { return static_cast<T const&>(*this).NumberOfOBDMs(); }

	//
	template <typename T1>
	void ComposeDensityMatrixDiagonal(CMatrixBase<T1>& Work)
	{ static_cast<T&>(*this).ComposeDensityMatrixDiagonal(Work); }

	cdouble GetDeterminant(std::vector<int> const& truncations) const
	{ return static_cast<T const&>(*this).GetDeterminant(truncations); }
	//
};

struct DiscreteBalanced : public HSTransform<DiscreteBalanced> {
	HSfield hsfield;
	OBDM obdm;
	std::vector<cdouble> dens;

	DiscreteBalanced(Parameters p)
	: hsfield{p}
	, obdm{p, hsfield}
	, dens{std::vector<cdouble>(p.Volume)}
	{ std::fill(dens.begin(), dens.end(), 0.0); }

	// -- begin OBDM related functions
	// from mini_OBDM

	inline void CopyFromStorage()
	{ obdm.CopyFrom(WS::Storage()); }
	inline void CopyToStorage() const
	{ WS::Storage().CopyFrom(obdm); }

	inline void Reset()
	{ obdm.Reset(); std::fill(dens.begin(), dens.end(), 0.0); }
	inline void Truncate(std::vector<int> const& truncations)
	{ obdm.Truncate(truncations[0]); }
	//

	//
	template <typename dir>
	inline void Evolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end)
	{ obdm.Evolve(begin, end, hsfield.params.muFactorUp); }
	template <typename dir>
	inline void Devolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end)
	{ obdm.Devolve(begin, end, hsfield.params.muFactorUp); }
	//

	//
	template <typename dir>
	inline void EvolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work)
	{ obdm.EvolveRightByTranspose(begin, end, Work, hsfield.params.muFactorUp); }

	template <typename dir>
	inline void DevolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work)
	{ obdm.DevolveRightByTranspose(begin, end, Work, hsfield.params.muFactorUp); }
	
	inline void ReverseWrap(int const tmax, int const t0, MatrixFFT& Work)
	{ obdm.ReverseWrap(tmax, t0, Work, hsfield.params.muFactorUp); }

	inline void Wrap(int const tmax, int const t0, MatrixFFT& Work)
	{ obdm.Wrap(tmax, t0, Work, hsfield.params.muFactorUp); }
	//

	//
	template <typename T1, typename T2>
	inline void DecomposeEvolution(std::vector<int>& truncations, double tolerance,
			CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2)
	{ truncations[0] = obdm.DecomposeEvolution(tolerance, Work, Work2); }
	template <typename T1, typename T2>
	inline void DecomposeEvolutionRight(std::vector<int>& truncations, double tolerance,
			CMatrixBase<T1>& Work,CMatrixBase<T2>& Work2)
	{ truncations[0] = obdm.DecomposeEvolutionRight(tolerance, Work, Work2); }
    template <typename T1, typename T2>
    inline int DecomposeProduct(double tolerance, mini_OBDM<>& StoredDecomp ,CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2)
	{ return obdm.DecomposeProduct(tolerance, StoredDecomp, Work, Work2); }
	// TODO: what to do here w.r.t. return value?
	//

	//
	template <typename T1>
	inline void ComputeReducedDensityMatrix(CMatrixBase<T1>& Work) const
	{ obdm.ComputeReducedDensityMatrix(WS::tmp_obdm(0), Work); }

    template <typename T1>
	inline void PartialReducedDensityMatrix(CMatrixBase<T1>& Work, const int timeslice)
	{ obdm.PartialReducedDensityMatrix(WS::tmp_obdm(0), Work, timeslice); }

	inline void OnePlusRLD(Matrix& Work) const
	{ obdm.OnePlusRLD(WS::rDiag(), Work); }

    template <typename T>
    inline cdouble SignedOnePlusRLD(mini_OBDM<double>& out, CMatrixBase<T>& Work) const
    { return obdm.SignedOnePlusRLD(out, Work); }

    template <typename T>
    inline void PerformEigenDRL(mini_OBDM<cdouble>& out, CMatrixBase<T>& Work)
	{ obdm.PerformEigenDRL(out, Work); }

    template <typename T1, typename T2>
    inline void RecoverEigenVectors(mini_OBDM<cdouble>& EigenDecomp, CMatrixBase<T1>& Work1, CMatrixBase<T2>& Work2)
	{ obdm.RecoverEigenVectors(EigenDecomp, Work1, Work2); }
	//

	// -- end OBDM related functions

	// -- begin HSfield related functions
	inline void SetAnisotropy(double newAnisotropy)
	{ hsfield.SetAnisotropy(newAnisotropy); }

	inline void Accept(int const TimeSlice)
	{ hsfield.Accept(TimeSlice); }

	inline void ProposeUpdateNaiveBiased(cdouble& Ratio, int timeSlice) {
		hsfield.BiasedRandomProposal(dens.size(), Ratio, timeSlice);
		hsfield.FillProposalDifferenceDiscrete_BDM(timeSlice);
		hsfield.ComputeHSRatio_BDM(timeSlice);
	}
    inline void ProposeUpdate(cdouble& Ratio, int timeSlice) {
		hsfield.ForceBias_BDM(dens.size(), dens.data(), dens.data(), Ratio, timeSlice);
		hsfield.FillProposalDifferenceDiscrete_BDM(timeSlice);
		hsfield.ComputeHSRatio_BDM(timeSlice);
	}
	inline void ApplyUpdate()
	{ RightMultiplyByDiagonal(obdm.R, hsfield.PotentialPrime); }
	inline void RevertUpdate()
	{ RightMultiplyByInverseDiagonal(obdm.R, hsfield.PotentialPrime); }

	inline cdouble GetHSWeight() const
	{ return hsfield.HSWeight; }

	inline void SaveToHDF() const
	{ hsfield.SaveToHDF(); }
	// -- end HSfield related functions

	inline HSfield& GetHSfield() { return hsfield; }
	inline OBDM& GetSpinUpOBDM() { return obdm; }
	inline OBDM& GetSpinDnOBDM() { return obdm; }
	inline std::vector<cdouble>& DensUp() { return dens; }
	inline std::vector<cdouble>& DensDn() { return dens; }
	inline Parameters& GetParams() { return hsfield.params; }
	inline int NumberOfOBDMs() const { return 1; }

	//
	template <typename T>
	inline void ComposeDensityMatrixDiagonal(CMatrixBase<T>& Work)
	{ WS::tmp_obdm(0).RecomposeDiagonal(dens, Work); }

    template <typename T1, typename T2>
	inline void ConstructDensityMatrix(int const i, CMatrixBase<T1>& Uout,
			CMatrixBase<T2>& Work) const
	{ WS::tmp_obdm(0).ReconstructU(Uout, Work); }

	inline cdouble GetDeterminant(std::vector<int> const& truncations) const {
		cdouble LogDet = 0.0;
		for (int i = 0; i < truncations[0]; ++i)
			LogDet += std::log(static_cast<cdouble>(WS::rDiag()[i]));
		return 2.0 * LogDet;
	}
	//
};

//

struct DiscretePolarised : public HSTransform<DiscretePolarised> {
	HSfield hsfield;
	OBDM up, dn;
	std::vector<cdouble> densUp, densDn;

	DiscretePolarised(Parameters p)
	: hsfield{p}
	, up{p, hsfield}
	, dn{p, hsfield}
	, densUp{std::vector<cdouble>(p.Volume)}
	, densDn{std::vector<cdouble>(p.Volume)}
	{ std::fill(densUp.begin(), densUp.end(), 0.0); std::fill(densDn.begin(), densDn.end(), 0.0); }

	// -- begin OBDM related functions
	// from mini_OBDM

	inline void CopyFromStorage()
	{ up.CopyFrom(WS::Storage(0)); dn.CopyFrom(WS::Storage(1)); }
	inline void CopyToStorage() const
	{ WS::Storage(0).CopyFrom(up); WS::Storage(1).CopyFrom(dn); }

	inline void Reset()
	{ up.Reset(); std::fill(densUp.begin(), densUp.end(), 0.0);
	  dn.Reset(); std::fill(densDn.begin(), densDn.end(), 0.0); }
	inline void Truncate(std::vector<int> const& truncations)
	{ up.Truncate(truncations[0]); dn.Truncate(truncations[1]); }
	//

	//
	template <typename dir>
	inline void Evolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end)
	{ up.Evolve(begin, end, hsfield.params.muFactorUp); dn.Evolve(begin, end, hsfield.params.muFactorDn); }
	template <typename dir>
	inline void Devolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end)
	{ up.Devolve(begin, end, hsfield.params.muFactorUp); dn.Devolve(begin, end, hsfield.params.muFactorDn); }
	//

	//
	template <typename dir>
	inline void EvolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work)
	{ up.EvolveRightByTranspose(begin, end, Work, hsfield.params.muFactorUp);
	  dn.EvolveRightByTranspose(begin, end, Work, hsfield.params.muFactorDn); }

	template <typename dir>
	inline void DevolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work)
	{ up.DevolveRightByTranspose(begin, end, Work, hsfield.params.muFactorUp);
	  dn.DevolveRightByTranspose(begin, end, Work, hsfield.params.muFactorDn); }
	
	inline void ReverseWrap(int const tmax, int const t0, MatrixFFT& Work)
	{ up.ReverseWrap(tmax, t0, Work, hsfield.params.muFactorUp);
	  dn.ReverseWrap(tmax, t0, Work, hsfield.params.muFactorDn); }

	inline void Wrap(int const tmax, int const t0, MatrixFFT& Work)
	{ up.Wrap(tmax, t0, Work, hsfield.params.muFactorUp);
	  dn.Wrap(tmax, t0, Work, hsfield.params.muFactorDn); }
	//

	//
	template <typename T1, typename T2>
	inline void DecomposeEvolution(std::vector<int>& truncations, double tolerance,
			CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2)
	{ truncations[0] = up.DecomposeEvolution(tolerance, Work, Work2);
	  truncations[1] = dn.DecomposeEvolution(tolerance, Work, Work2); }
	template <typename T1, typename T2>
	inline void DecomposeEvolutionRight(std::vector<int>& truncations, double tolerance,
			CMatrixBase<T1>& Work,CMatrixBase<T2>& Work2)
	{ truncations[0] = up.DecomposeEvolutionRight(tolerance, Work, Work2);
	  truncations[1] = dn.DecomposeEvolutionRight(tolerance, Work, Work2); }
	/* ??????
    template <typename T1, typename T2>
    inline int DecomposeProduct(double tolerance, mini_OBDM<>& StoredDecomp ,CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2)
	{ return up.DecomposeProduct(tolerance, StoredDecomp, Work, Work2); }
	*/
	// TODO: what to do here w.r.t. return value?
	//

	//
	template <typename T1>
	inline void ComputeReducedDensityMatrix(CMatrixBase<T1>& Work) const
	{ up.ComputeReducedDensityMatrix(WS::tmp_obdm(0), Work);
	  dn.ComputeReducedDensityMatrix(WS::tmp_obdm(1), Work); }

    template <typename T1>
	inline void PartialReducedDensityMatrix(CMatrixBase<T1>& Work, const int timeslice)
	{ up.PartialReducedDensityMatrix(WS::tmp_obdm(0), Work, timeslice);
	  dn.PartialReducedDensityMatrix(WS::tmp_obdm(1), Work, timeslice); }


	inline void OnePlusRLD(Matrix& Work) const
	{ up.OnePlusRLD(WS::rDiag(0), Work); dn.OnePlusRLD(WS::rDiag(1), Work); }

    template <typename T>
    inline cdouble SignedOnePlusRLD(mini_OBDM<double>& out, CMatrixBase<T>& Work) const
    { cdouble const S1 = up.SignedOnePlusRLD(out, Work); cdouble const S2 = dn.SignedOnePlusRLD(out, Work); return S1 * S2; }

	/*
    template <typename T>
    inline void PerformEigenDRL(mini_OBDM<cdouble>& out, CMatrixBase<T>& Work)
	{ up.PerformEigenDRL(out, Work); }
	*/

	/*
    template <typename T1, typename T2>
    inline void RecoverEigenVectors(mini_OBDM<cdouble>& EigenDecomp, CMatrixBase<T1>& Work1, CMatrixBase<T2>& Work2)
	{ up.RecoverEigenVectors(EigenDecomp, Work1, Work2); }
	*/
	//

	// -- end OBDM related functions

	// -- begin HSfield related functions
	inline void SetAnisotropy(double newAnisotropy)
	{ hsfield.SetAnisotropy(newAnisotropy); }

	inline void Accept(int const TimeSlice)
	{ hsfield.Accept(TimeSlice); }

	inline void ProposeUpdate(cdouble& Ratio, int timeSlice) {
		hsfield.ForceBias_BDM(densUp.size(), densUp.data(), densDn.data(), Ratio,
				timeSlice);
		hsfield.FillProposalDifferenceDiscrete_BDM(timeSlice);
		hsfield.ComputeHSRatio_BDM(timeSlice);
	}
    inline void ProposeUpdateNaiveBiased(cdouble& Ratio, int timeSlice) {
		hsfield.BiasedRandomProposal(densUp.size(), Ratio, timeSlice);
		hsfield.FillProposalDifferenceDiscrete_BDM(timeSlice);
		hsfield.ComputeHSRatio_BDM(timeSlice);
	}
	inline void ApplyUpdate()
	{ RightMultiplyByDiagonal(up.R, hsfield.PotentialPrime);
	  RightMultiplyByDiagonal(dn.R, hsfield.PotentialPrime); }
	inline void RevertUpdate()
	{ RightMultiplyByInverseDiagonal(up.R, hsfield.PotentialPrime);
	  RightMultiplyByInverseDiagonal(dn.R, hsfield.PotentialPrime); }

	inline cdouble GetHSWeight() const
	{ return hsfield.HSWeight; }

	inline void SaveToHDF() const
	{ hsfield.SaveToHDF(); }
	// -- end HSfield related functions

	inline HSfield& GetHSfield() { return hsfield; }
	inline OBDM& GetSpinUpOBDM() { return up; }
	inline OBDM& GetSpinDnOBDM() { return dn; }
	inline std::vector<cdouble>& DensUp() { return densUp; }
	inline std::vector<cdouble>& DensDn() { return densDn; }
	inline Parameters& GetParams() { return hsfield.params; }
	inline int NumberOfOBDMs() const { return 2; }

	//
	template <typename T>
	inline void ComposeDensityMatrixDiagonal(CMatrixBase<T>& Work)
	{ WS::tmp_obdm(0).RecomposeDiagonal(densUp, Work);
	  WS::tmp_obdm(1).RecomposeDiagonal(densDn, Work); }

    template <typename T1, typename T2>
	inline void ConstructDensityMatrix(int const i, CMatrixBase<T1>& Uout,
			CMatrixBase<T2>& Work) const
	{ WS::tmp_obdm(i).ReconstructU(Uout, Work); }

	inline cdouble GetDeterminant(std::vector<int> const& truncations) const {
		cdouble LogDet = 0.0;
		for (int i = 0; i < truncations[0]; ++i)
			LogDet += std::log(static_cast<cdouble>(WS::rDiag(0)[i]));
		for (int i = 0; i < truncations[1]; ++i)
			LogDet += std::log(static_cast<cdouble>(WS::rDiag(1)[i]));
		return LogDet;
	}
	//
};

enum class BP  { Balanced, Polarised };
enum class HST { Discrete };

template <BP bp, HST hs> struct HSTTraits { };
template<> struct HSTTraits<BP::Balanced,  HST::Discrete> { using type = DiscreteBalanced;  };
template<> struct HSTTraits<BP::Polarised, HST::Discrete> { using type = DiscretePolarised; };

#endif
