/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include <fftw3.h>
#include "highfive/H5File.hpp"
#include <vector>
#include <complex>
#include <memory>
#include "matrix_fft.h"
#include "rng.h"
#include "obdm.h"
#include "fakeIter.h"
#include "workspace.h"
#include "measureTime.h"
#include "matrix-diag.h"
#include "HStransforms.h"

/*struct PlanDeleter {
    void operator()(fftw_plan plan) const;
};*/

typedef std::complex<double> cdouble;
using plan_ptr = std::unique_ptr<fftw_plan_s, PlanDeleter>;   

std::vector<int> VectorFromIndex(int index, std::vector<int> Sizes);
int NegativeIndexFromVector(std::vector<int>& vec, std::vector<int> Sizes);

//struct for autocorrelation
//works with data in complex<double> format, since 
//fftw changes behavior depending on the type of data
struct autocorrelation
{   
    unsigned int data_len;
    int number_observables;
    std::vector<cdouble> data;
    plan_ptr pfwd, pbwd;

    //constructor
    //add an access pattern for the part of the data to store
    autocorrelation(int data_len, int number_observables);
    
    //add data to autocorrelation
    void AddData(int observable_index, std::vector<cdouble> const& dataIn);

    void ComputeAutocorrelation();

    void OutputHDF5(std::string const& filename, std::string const& ObsName);
};

struct statistics
{
    int number_observables;
    std::vector<cdouble> Var;
    std::vector<cdouble> VarImag;
    std::vector<cdouble> Mean;

    //constructor
    statistics(int observables);
    statistics(std::vector<cdouble> const& MeanIn);
    
    //add data to autocorrelation
    void AddData(std::vector<cdouble> const& DataIn);

    //void AddDataComplex(std::vector<cdouble> const& DataIn);

    void Normalize();

    //write to hdf5 file, writes Stat and Mean
    //might want to add an potion here to write in the form of an array
    //the we can view heatmaps of the data
    void OutputHDF5(std::string const& filename, std::string const& ObsName);
    void OutputHDF5(std::string const& filename, std::string const& ObsName, unsigned int const& Vol, unsigned int const& Vol2);


    private:
    int sample_counter;
};

template <typename T>
struct Observables
{
    Parameters parameters;
    double const threshold;
    HSTransform<T>& hst;
    std::vector<cdouble> TempMatrix;
    std::vector<cdouble> TempDiag;
    std::vector<cdouble> TempDiag2;

    //parameters for canoical projection
    int particle_number_min;
    int particle_number_max;
    int fourier_sum_max;

    Observables(Parameters const& parametersIn, double truncationThresholdIN, HSTransform<T>& hst);

    void LoadNextConfiguration();

    cdouble ComputeSign();
    void ComputePositionDensity(std::vector<cdouble>& PositionDensity_up, std::vector<cdouble>& PositionDensity_dn);
    void ComputeMomentumDensityFromPosition(std::vector<cdouble>& MomentumDensity_up , std::vector<cdouble>& MomentumDensity_dn);


    void ComputeEnergy();
    void ComputeDensityDensity(std::vector<cdouble>& DensityDensity);
    void PairMomentumDensity(std::vector<cdouble>& PairMomentumDensity);
    void OnSitePairPosition(std::vector<cdouble>& PairPosition);
    void ComputeShotNoiseDirect(std::vector<cdouble>& ShotNoise, std::vector<cdouble>& MeanDensUp, std::vector<cdouble>& MeanDensDn);
    void ComputePairMomentum();

    void ComputTwoBodyMixedTerm(std::vector<cdouble>& TwoBodyMixedTerm, bool is_down = false);

    private:
    int config_counter;

};

template <typename T>
struct CanonicalObservables
{
    Parameters parameters;
    double const threshold;
    HSTransform<T>& hst;
    mini_OBDM<cdouble> eigenObdm_up;
    mini_OBDM<cdouble> eigenObdm_down;
    std::vector<cdouble> TempMatrix;
    std::vector<cdouble> TempDiag;
    std::vector<cdouble> TempDiag2;
    std::vector<cdouble> DiagStore_up;
    std::vector<cdouble> DiagStore_down;
    DiagonalMatrix<double> TempDiagR;
    cdouble Tmp_Weight_up;
    cdouble Tmp_Weight_down;
    int N_UP;
    int N_DN;

    std::vector<cdouble> Dispersion;
    std::vector<cdouble> Trap;


    //parameters for canoical projection
    int UP_fourier_sum_max;
    int DN_fourier_sum_max;

    CanonicalObservables(Parameters const& parametersIn, double truncationThresholdIN, HSTransform<T>& hstIn,
                    int const& N_UP, int const& N_DN , int const& UP_fourier_sum_max_in, int const& DN_fourier_sum_max_in);

    void LoadNextConfiguration(int start_config = 0);

    void ComputeWeightRatios(std::vector<cdouble>& WeightRatios,
                        int const& UP_particle_number_min, int const& UP_particle_number_max,
                        int const& DN_particle_number_min, int const& DN_particle_number_max);

    void ComputeFourierTwoBody();

    void AddPotentialEnergy(cdouble& Energy, std::vector<cdouble> const& PositionDensity_up, std::vector<cdouble> const& PositionDensity_dn);
    void AddKineticEnergy(cdouble& Energy, std::vector<cdouble> const& MomentumDensity_up, std::vector<cdouble> const& MomentumDensity_dn);

    void GetPositionDensity(std::vector<cdouble>& PositionDensity_up, std::vector<cdouble>& PositionDensity_dn);
    void GetMomentumDensity(std::vector<cdouble>& MomentumDensity_up , std::vector<cdouble>& MomentumDensity_dn);

    void GetDensityDensiy(std::vector<cdouble>& DensityDensity);
    void GetShotNoise(std::vector<cdouble>& ShotNoise, std::vector<cdouble>& MeanDens_up, std::vector<cdouble>& MeanDens_dn);
    void GetPairMomentum(std::vector<cdouble>& PairMomentum);

    private:
    int config_counter;

};