#include "analysis.h"

std::vector<int> VectorFromIndex(int index, std::vector<int> Sizes){
    int Dims = Sizes.size();
    std::vector<int> result(Dims);
    for (int i = Dims - 1; i >= 0; --i){
        result[i] = index % Sizes[i];
        index /= Sizes[i];
    }
    return result;
};
 
int NegativeIndexFromVector(std::vector<int>& vec, std::vector<int> Sizes){
    int result = 0;
    int tmp = 0;

    for (int i = 0; i < vec.size(); ++i){
        vec[i] = (Sizes[i] - vec[i]) % Sizes[i];

        tmp = 1;
        for (int j = i+1; j < vec.size(); ++j){
            tmp *= Sizes[j];
        }
        result += vec[i] * tmp;
    }
    
    return result;
};