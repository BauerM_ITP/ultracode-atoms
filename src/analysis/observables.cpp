/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "analysis.h"

//Constructor for Observable class
template<typename T>
Observables<T>::Observables(Parameters const& parametersIn, double truncationThresholdIN, HSTransform<T>& hstIn)
    :
    parameters{parametersIn},
    threshold(truncationThresholdIN),
    hst{hstIn}, 
    TempMatrix(parameters.Volume * parameters.Volume),
    TempDiag(parameters.Volume),
    TempDiag2(parameters.Volume),
    config_counter(0)
    { }

template Observables<DiscreteBalanced>::Observables(Parameters const& parametersIn, double truncationThresholdIN, HSTransform<DiscreteBalanced>& hstIn);
template Observables<DiscretePolarised>::Observables(Parameters const& parametersIn, double truncationThresholdIN, HSTransform<DiscretePolarised>& hstIn);

template<typename T>
void Observables<T>::LoadNextConfiguration()
{   
    int const Msize = parameters.Volume;
	int const Decompositions = parameters.Nt / parameters.DecomposeEvery;
	int const DecomposeEvery = parameters.DecomposeEvery;

    hst.GetHSfield().ReadFromHDF(config_counter);
    config_counter++;

    //Compute Umatrix from the config

    hst.Reset();
    std::vector<int> Truncations(2);
    Truncations[0] = hst.GetSpinUpOBDM().D.MaxCols();
    Truncations[1] = hst.GetSpinDnOBDM().D.MaxCols();

    for (int i = 0; i < Decompositions; ++i)
        {
            auto range = FakeIota{DecomposeEvery * i, DecomposeEvery * (i + 1)};
			hst.Evolve(range.cbegin(), range.cend()); 

            hst.DecomposeEvolution(Truncations, threshold, WS::tmp1(), WS::tmp2());
        }
    
    hst.CopyToStorage();
}

template void Observables<DiscreteBalanced>::LoadNextConfiguration();
template void Observables<DiscretePolarised>::LoadNextConfiguration();

template<typename T>
cdouble Observables<T>::ComputeSign(){
    return hst.SignedOnePlusRLD(WS::tmp_obdm(0), WS::tmp1());
}

template cdouble Observables<DiscreteBalanced>::ComputeSign();
template cdouble Observables<DiscretePolarised>::ComputeSign();


template<typename T>
void Observables<T>::ComputePositionDensity(std::vector<cdouble>& PositionDensity_up, std::vector<cdouble>& PositionDensity_dn){
    hst.ComputeReducedDensityMatrix(WS::tmp1());

    if (hst.NumberOfOBDMs() == 1){
        std::cout << "Computing position density from reduced density matrix UniqueOBDMs() == 1" << std::endl;
        WS::tmp_obdm(0).RecomposeDiagonal(PositionDensity_up, WS::tmp1());
        WS::tmp_obdm(0).RecomposeDiagonal(PositionDensity_dn, WS::tmp1());
    }
    else{
        std::cout << "Computing position density from reduced density matrix UniqueOBDMs() == 2" << std::endl;
        std::cout << WS::UniqueOBDMs() << std::endl;
        WS::tmp_obdm(0).RecomposeDiagonal(PositionDensity_up, WS::tmp1());
        WS::tmp_obdm(1).RecomposeDiagonal(PositionDensity_dn, WS::tmp1());
    }

}

template void Observables<DiscreteBalanced>::ComputePositionDensity(std::vector<cdouble>& PositionDensity_up, std::vector<cdouble>& PositionDensity_dn);
template void Observables<DiscretePolarised>::ComputePositionDensity(std::vector<cdouble>& PositionDensity_up, std::vector<cdouble>& PositionDensity_dn);


//This assumes that the posiiton density is in memory
template<typename T>
void Observables<T>::ComputeMomentumDensityFromPosition(std::vector<cdouble>& MomentumDensity_up , std::vector<cdouble>& MomentumDensity_dn){
    //computes the FFT of the position density matrix
    for (int i = 0; i < hst.NumberOfOBDMs() ; i++){
        WS::tmp_obdm(i).Q.ExecuteFwd();
        WS::tmp1().Truncate(WS::tmp_obdm(i).Q.Rows(), WS::tmp_obdm(i).Q.Cols());
        WS::tmp1().TransposeFrom(WS::tmp_obdm(i).R);
        WS::tmp1().ExecuteBwd();
        WS::tmp_obdm(i).R.TransposeFrom(WS::tmp1());

        for (auto& q : WS::tmp_obdm(i).Q)
            q /= sqrt(parameters.Volume);

        for (auto& r : WS::tmp_obdm(i).R)
            r /= sqrt(parameters.Volume);
    }

    WS::tmp1().ResetSize();

    if (hst.NumberOfOBDMs() == 1){
        WS::tmp_obdm(0).RecomposeDiagonal(MomentumDensity_up, WS::tmp1());
        WS::tmp_obdm(0).RecomposeDiagonal(MomentumDensity_dn, WS::tmp1());
    }
    else{
        WS::tmp_obdm(0).RecomposeDiagonal(MomentumDensity_up, WS::tmp1());
        WS::tmp_obdm(1).RecomposeDiagonal(MomentumDensity_dn, WS::tmp1());
    }

}

template void Observables<DiscreteBalanced>::ComputeMomentumDensityFromPosition(std::vector<cdouble>& MomentumDensity_up , std::vector<cdouble>& MomentumDensity_dn);
template void Observables<DiscretePolarised>::ComputeMomentumDensityFromPosition(std::vector<cdouble>& MomentumDensity_up , std::vector<cdouble>& MomentumDensity_dn);

//This need the momentum space density of bot up and down species to be in memory
//from them the shot noise is computed
template<typename T>
void Observables<T>::ComputeDensityDensity(std::vector<cdouble>& DensityDensity){
    
    if (hst.NumberOfOBDMs() == 1){
        WS::tmp_obdm(0).RecomposeDiagonal(TempDiag, WS::tmp1());
        WS::tmp_obdm(0).RecomposeDiagonal(TempDiag2, WS::tmp1());
    }
    else{
        WS::tmp_obdm(0).RecomposeDiagonal(TempDiag, WS::tmp1());
        WS::tmp_obdm(1).RecomposeDiagonal(TempDiag2, WS::tmp1());
    }
    //outer product of the diagonal elements
    //TODO
    //Maybe already "roll" this, so zero momentum is in the middle
    for (int i = 0; i < parameters.Volume; ++i){
        for (int j = 0; j < parameters.Volume; ++j){
            DensityDensity[i + j * parameters.Volume] = TempDiag[i] * TempDiag2[j];
        }
    }   
}

template void Observables<DiscreteBalanced>::ComputeDensityDensity(std::vector<cdouble>& DensityDensity);
template void Observables<DiscretePolarised>::ComputeDensityDensity(std::vector<cdouble>& DensityDensity);

template<typename T>
void Observables<T>::PairMomentumDensity(std::vector<cdouble>& PairMomentumDensity){
    if (hst.NumberOfOBDMs() == 1){
        WS::tmp_obdm(0).ReconstructU(WS::Storage(0).Q ,WS::tmp1());
        WS::tmp_obdm(0).ReconstructU(WS::Storage(0).R ,WS::tmp2());
    }
    else{
        WS::tmp_obdm(0).ReconstructU(WS::Storage(0).Q ,WS::tmp1());
        WS::tmp_obdm(1).ReconstructU(WS::Storage(0).R ,WS::tmp2());
    }

    
    std::vector <int> tmp_vec(parameters.Sizes.size());
    int tmp_index;
    std::vector <int> tmp_vec2(parameters.Sizes.size());
    int tmp_index2;
    for (int k1 = 0; k1 < parameters.Volume; ++k1){
        tmp_vec = VectorFromIndex(k1, parameters.Sizes);
        tmp_index = NegativeIndexFromVector(tmp_vec, parameters.Sizes);

        //std::cout << k1 << " " << tmp_index << std::endl;

        for (int k2 = 0; k2 < parameters.Volume; ++k2){
            tmp_vec2 = VectorFromIndex(k2, parameters.Sizes);
            tmp_index2 = NegativeIndexFromVector(tmp_vec2, parameters.Sizes);

            //std::cout << k2 << " " << tmp_index2 << std::endl;

            PairMomentumDensity[k1 + k2 * parameters.Volume] = WS::Storage(0).Q[k1 + k2 * parameters.Volume] * WS::Storage(0).R[tmp_index + tmp_index2 * parameters.Volume];
        }
    }
    
}

template void Observables<DiscreteBalanced>::PairMomentumDensity(std::vector<cdouble>& PairMomentumDensity);
template void Observables<DiscretePolarised>::PairMomentumDensity(std::vector<cdouble>& PairMomentumDensity);

template<typename T>
void Observables<T>::OnSitePairPosition(std::vector<cdouble>& PairPosition){
    if (hst.NumberOfOBDMs() == 1){
        WS::tmp_obdm(0).ReconstructU(WS::Storage(0).Q ,WS::tmp1());
        WS::tmp_obdm(0).ReconstructU(WS::Storage(0).R ,WS::tmp2());
    }
    else{
        WS::tmp_obdm(0).ReconstructU(WS::Storage(0).Q ,WS::tmp1());
        WS::tmp_obdm(1).ReconstructU(WS::Storage(0).R ,WS::tmp2());
    }

    for (int x1 = 0; x1 < parameters.Volume; ++x1){
        for (int x2 = 0; x2 < parameters.Volume; ++x2){
            PairPosition[x1 + x2 * parameters.Volume] = WS::Storage(0).R[x1 + x2 * parameters.Volume] * WS::Storage(0).Q[x1 + x2 * parameters.Volume];
        }
    }
}

template void Observables<DiscreteBalanced>::OnSitePairPosition(std::vector<cdouble>& PairPosition);
template void Observables<DiscretePolarised>::OnSitePairPosition(std::vector<cdouble>& PairPosition);

template<typename T>
void Observables<T>::ComputTwoBodyMixedTerm(std::vector<cdouble>& TwoBodyMixedTerm, bool is_down){

    //if bool is true and sytem is imbalanced, load down spin stuff
    if (is_down && hst.NumberOfOBDMs() == 2){
        WS::tmp_obdm(1).ReconstructU(WS::Storage(0).Q ,WS::tmp1());
    }
    else{
        WS::tmp_obdm(0).ReconstructU(WS::Storage(0).Q ,WS::tmp1());
    }

    for (int x1 = 0; x1 < parameters.Volume; ++x1){
        for (int x2 = 0; x2 < parameters.Volume; ++x2){
            if (x1 != x2)
                TwoBodyMixedTerm[x1 + x2 * parameters.Volume] = -WS::Storage(0).Q[x2 + x1 * parameters.Volume] * WS::Storage(0).Q[x1 + x2 * parameters.Volume];
            else
                TwoBodyMixedTerm[x1 + x2 * parameters.Volume] = WS::Storage(0).Q[x2 + x1 * parameters.Volume] * (1.0 - WS::Storage(0).Q[x1 + x2 * parameters.Volume]);
        }
    }
}

template void Observables<DiscreteBalanced>::ComputTwoBodyMixedTerm(std::vector<cdouble>& TwoBodyMixedTerm, bool is_down);
template void Observables<DiscretePolarised>::ComputTwoBodyMixedTerm(std::vector<cdouble>& TwoBodyMixedTerm, bool is_down);

template<typename T>
void Observables<T>::ComputeShotNoiseDirect(std::vector<cdouble>& ShotNoise, std::vector<cdouble>& MeanDensUp, std::vector<cdouble>& MeanDensDn){
    hst.ComputeReducedDensityMatrix(WS::tmp1());

    for (int i = 0; i < hst.NumberOfOBDMs() ; i++){
        WS::tmp_obdm(i).Q.ExecuteFwd();
        WS::tmp1().Truncate(WS::tmp_obdm(i).Q.Rows(), WS::tmp_obdm(i).Q.Cols());
        WS::tmp1().TransposeFrom(WS::tmp_obdm(i).R);
        WS::tmp1().ExecuteBwd();
        WS::tmp_obdm(i).R.TransposeFrom(WS::tmp1());

        for (auto& q : WS::tmp_obdm(i).Q)
            q /= sqrt(parameters.Volume);

        for (auto& r : WS::tmp_obdm(i).R)
            r /= sqrt(parameters.Volume);
    }

    WS::tmp1().ResetSize();


    if (hst.NumberOfOBDMs()  == 1){
        WS::tmp_obdm(0).RecomposeDiagonal(TempDiag, WS::tmp1());
        WS::tmp_obdm(0).RecomposeDiagonal(TempDiag2, WS::tmp1());
    }
    else{
        WS::tmp_obdm(0).RecomposeDiagonal(TempDiag, WS::tmp1());
        WS::tmp_obdm(1).RecomposeDiagonal(TempDiag2, WS::tmp1());
    }
    //outer product of the diagonal elements
    //TODO
    //Maybe already "roll" this, so zero momentum is in the middle
    for (int i = 0; i < parameters.Volume; ++i){
        for (int j = 0; j < parameters.Volume; ++j){
            ShotNoise[i + j * parameters.Volume] = (TempDiag[i] - MeanDensUp[i] )  * (TempDiag2[j] - MeanDensDn[j]);
        }
    }
}

template void Observables<DiscreteBalanced>::ComputeShotNoiseDirect(std::vector<cdouble>& ShotNoise, std::vector<cdouble>& MeanDensUp, std::vector<cdouble>& MeanDensDn);
template void Observables<DiscretePolarised>::ComputeShotNoiseDirect(std::vector<cdouble>& ShotNoise, std::vector<cdouble>& MeanDensUp, std::vector<cdouble>& MeanDensDn);

template<typename T>
CanonicalObservables<T>::CanonicalObservables(Parameters const& parametersIn, double truncationThresholdIN, HSTransform<T>& hstIn,
                    int const& N_UP, int const& N_DN , int const& UP_fourier_sum_max_in, int const& DN_fourier_sum_max_in):
    parameters{parametersIn},
    threshold(truncationThresholdIN),
    hst(hstIn),
    eigenObdm_down(parameters),
    eigenObdm_up(parameters),
    TempMatrix(parameters.Volume * parameters.Volume),
    TempDiag(parameters.Volume),
    TempDiag2(parameters.Volume),
    DiagStore_up(parameters.Volume),
    DiagStore_down(parameters.Volume),
    TempDiagR(parameters.Volume),
    Tmp_Weight_up(1.0),
    Tmp_Weight_down(1.0),
    UP_fourier_sum_max(UP_fourier_sum_max_in),
    DN_fourier_sum_max(DN_fourier_sum_max_in),
    N_UP(N_UP),
    N_DN(N_DN),
    config_counter(0)
    { 
        //Initialise dispersion and trap
        Dispersion.resize(parameters.Volume);
        Trap.resize(parameters.Volume);

        for (int i = 0; i < Dispersion.size(); i++){
			double p2 = 0;
			int tmp = static_cast<int>(i);
			std::for_each(parameters.Sizes.crbegin(), parameters.Sizes.crend(),
					[&tmp, &p2](int const n) {
					int const intMom = tmp % n;
					double const p = (intMom <= std::floor(n/2) ? intMom : intMom - n) * 2.0 * M_PI / n;

					p2 += p*p;
					tmp /= n;
					});

			Dispersion[i] = (p2 )/(2.0);
		}

        std::cout << "Dispersion print\n";
        for (auto& i : Dispersion) std::cout << i << std::endl;
        std::cout << "Dispersion print end\n";

        for (int ii = 0; ii < Trap.size(); ++ii) {
            std::vector<double> pos(parameters.Sizes.size(), 0.);
            int index = ii;
            //lattice centered around zero
            std::transform(parameters.Sizes.crbegin(), parameters.Sizes.crend(), pos.rbegin(),
                                [&index] (const int n) -> double {
                                double const x = static_cast<double>(index % n) - static_cast<double>(n)/2.;
                                index /= n;
                                return x;
                            });

            double val = std::accumulate(pos.cbegin(), pos.cend(), 0.0,
                [omega = parameters.omega](double const oldVal, double const newVal)
                { return oldVal + omega * newVal*newVal; });

            if (val * parameters.DecomposeEvery > parameters.ExtPotentialCutoff) val = parameters.ExtPotentialCutoff / parameters.DecomposeEvery;

            Trap[ii] = val / parameters.anisotropy;
	    }

    }


template CanonicalObservables<DiscreteBalanced>::CanonicalObservables(Parameters const& parametersIn, double truncationThresholdIN, HSTransform<DiscreteBalanced>& hstIn,
                    int const& N_UP, int const& N_DN , int const& UP_fourier_sum_max_in, int const& DN_fourier_sum_max_in);
template CanonicalObservables<DiscretePolarised>::CanonicalObservables(Parameters const& parametersIn, double truncationThresholdIN, HSTransform<DiscretePolarised>& hstIn,
                    int const& N_UP, int const& N_DN , int const& UP_fourier_sum_max_in, int const& DN_fourier_sum_max_in);

template<typename T>
void CanonicalObservables<T>::LoadNextConfiguration(int start_config)
{   
    int const Msize = parameters.Volume;
    int const Decompositions = parameters.Nt / parameters.DecomposeEvery;
    int const DecomposeEvery = parameters.DecomposeEvery;

    hst.GetHSfield().ReadFromHDF(config_counter + start_config);
    config_counter++;

    //Compute Umatrix from the config

    hst.Reset();
    std::vector<int> Truncations(2);
    Truncations[0] = hst.GetSpinUpOBDM().D.MaxCols();
    Truncations[1] = hst.GetSpinDnOBDM().D.MaxCols();

    for (int i = 0; i < Decompositions; ++i)
        {
            auto range = FakeIota{DecomposeEvery * i, DecomposeEvery * (i + 1)};
			hst.Evolve(range.cbegin(), range.cend()); 

            hst.DecomposeEvolution(Truncations, threshold, WS::tmp1(), WS::tmp2());
        }

    std::cout << Truncations[0] << " " << Truncations[1] << std::endl;
    std::cout << hst.GetSpinUpOBDM().Q.Cols() << " " << hst.GetSpinDnOBDM().Q.Cols() << std::endl;
    
    //hst.CopyToStorage();

}

template void CanonicalObservables<DiscreteBalanced>::LoadNextConfiguration(int start_config);
template void CanonicalObservables<DiscretePolarised>::LoadNextConfiguration(int start_config);

template<typename T>
void CanonicalObservables<T>::ComputeWeightRatios(std::vector<cdouble>& WeightRatios,
                        int const& UP_particle_number_min, int const& UP_particle_number_max,
                        int const& DN_particle_number_min, int const& DN_particle_number_max){
    
    cdouble UP_varphi = cdouble(0,1) * 2.0 * M_PI / (double) UP_fourier_sum_max;
    cdouble DN_varphi = cdouble(0,1) * 2.0 * M_PI / (double) DN_fourier_sum_max;

    if (DN_particle_number_max > DN_fourier_sum_max || UP_particle_number_max > UP_fourier_sum_max){
        std::cout << "ERROR: Particle number larger than fourier sum" << std::endl;
        exit(1);
    }

    hst.GetSpinUpOBDM().PerformEigenDRL(eigenObdm_up, WS::tmp1());
    hst.GetSpinDnOBDM().PerformEigenDRL(eigenObdm_down, WS::tmp1());

    hst.GetSpinUpOBDM().RecoverEigenVectors(eigenObdm_up, WS::tmp1(), WS::tmp2());
    hst.GetSpinDnOBDM().RecoverEigenVectors(eigenObdm_down, WS::tmp1(), WS::tmp2());

    eigenObdm_up.Truncate(hst.GetSpinUpOBDM().Q.Cols());
    eigenObdm_down.Truncate(hst.GetSpinDnOBDM().Q.Cols());

    cdouble tmpDetRatio = 1.0;
    for (auto& r: WeightRatios)
        r = 0.0; 

    std::vector<cdouble> tmpWeightRatioUp(UP_particle_number_max + 1 - UP_particle_number_min, 0.0);
    std::vector<cdouble> tmpWeightRatioDown(DN_particle_number_max + 1 - DN_particle_number_min, 0.0);

    for (int N_UP = UP_particle_number_min; N_UP < UP_particle_number_max + 1; ++N_UP){
        for (int m = 0; m < UP_fourier_sum_max; ++m){
            tmpDetRatio = 1.0;
            for (int i = 0; i < eigenObdm_up.D.Cols(); ++i) {
                tmpDetRatio *= (1.0 + exp(-(double) m * UP_varphi)*eigenObdm_up.D[i]) / (1.0 + eigenObdm_up.D[i]);
            }

            tmpDetRatio *= 1.0 / (double) UP_fourier_sum_max
                                * exp(UP_varphi * ((double) m * (double) N_UP));
            tmpWeightRatioUp[N_UP - UP_particle_number_min] += tmpDetRatio;
        }
    }

    for (int N_DN = DN_particle_number_min; N_DN < DN_particle_number_max + 1; ++N_DN){
        for (int n = 0; n < DN_fourier_sum_max; ++n){
            tmpDetRatio = 1.0;
            for (int i = 0; i < eigenObdm_down.D.Cols(); ++i) {
                tmpDetRatio *= (1.0 + exp(-(double) n * DN_varphi)*eigenObdm_down.D[i]) / (1.0 + eigenObdm_down.D[i]);
            }

            tmpDetRatio *= 1.0 / (double) DN_fourier_sum_max
                                * exp(DN_varphi * ((double) n * (double) N_DN));

            tmpWeightRatioDown[N_DN - DN_particle_number_min] += tmpDetRatio;
        }

    }

    for (int N_UP = UP_particle_number_min; N_UP < UP_particle_number_max + 1; ++N_UP){
        for (int N_DN = DN_particle_number_min; N_DN < DN_particle_number_max + 1; ++N_DN){
            WeightRatios[(N_UP - UP_particle_number_min) * (DN_particle_number_max - DN_particle_number_min + 1) + (N_DN - DN_particle_number_min)] = tmpWeightRatioUp[N_UP - UP_particle_number_min] * tmpWeightRatioDown[N_DN - DN_particle_number_min];
        }
    }

}

template void CanonicalObservables<DiscreteBalanced>::ComputeWeightRatios(std::vector<cdouble>& WeightRatios,
                        int const& UP_particle_number_min, int const& UP_particle_number_max,
                        int const& DN_particle_number_min, int const& DN_particle_number_max);
template void CanonicalObservables<DiscretePolarised>::ComputeWeightRatios(std::vector<cdouble>& WeightRatios,
                        int const& UP_particle_number_min, int const& UP_particle_number_max,
                        int const& DN_particle_number_min, int const& DN_particle_number_max);  


template <typename T>
void CanonicalObservables<T>::ComputeFourierTwoBody(){
    cdouble UP_varphi = cdouble(0,1) * 2.0 * M_PI / (double) UP_fourier_sum_max;
    cdouble DN_varphi = cdouble(0,1) * 2.0 * M_PI / (double) DN_fourier_sum_max;

    for (int x = 0; x < DiagStore_up.size(); ++x){
        DiagStore_up[x] = 0.0;
        DiagStore_down[x] = 0.0;
        TempDiag[x] = 0.0;
        TempDiag2[x] = 0.0;
    }

    hst.ComputeReducedDensityMatrix(WS::tmp1());

    hst.ComposeDensityMatrixDiagonal(WS::tmp1());

    cdouble Nup = 0.0;

    for (auto& n: hst.DensUp())
        Nup += n;

    std::cout << "Nup form QR: " << Nup << std::endl;

    //compute the eigen decomposition of the up and down density matrix
    hst.GetSpinUpOBDM().PerformEigenDRL(eigenObdm_up, WS::tmp1());
    hst.GetSpinDnOBDM().PerformEigenDRL(eigenObdm_down, WS::tmp1());

    hst.GetSpinUpOBDM().RecoverEigenVectors(eigenObdm_up, WS::tmp1(), WS::tmp2());
    hst.GetSpinDnOBDM().RecoverEigenVectors(eigenObdm_down, WS::tmp1(), WS::tmp2());

    eigenObdm_up.Truncate(hst.GetSpinUpOBDM().Q.Cols());
    eigenObdm_down.Truncate(hst.GetSpinDnOBDM().Q.Cols());

    Nup = 0.0;

    for (int i = 0; i < eigenObdm_up.D.Cols(); ++i){
        Nup += eigenObdm_up.D[i] / (eigenObdm_up.D[i] + 1.0);
    }

    std::cout << "Nup from Eigen: " << Nup << std::endl;


    cdouble UpSign = 1.0;
    cdouble DownSign = 1.0;
    //std::cout << "Number of Evals UP: " << eigenObdm_up.D.Cols() << std::endl;
    //std::cout << "Number of Evals DOWN: " << eigenObdm_down.D.Cols() << std::endl;

    for (int i = 0; i < eigenObdm_up.D.Cols(); ++i){
        UpSign *= eigenObdm_up.D[i] / abs(eigenObdm_up.D[i]);
    }

    std::cout << "UpSign: " << UpSign << std::endl;

    cdouble fulldet = 1.0;

    for (int i = 0; i < eigenObdm_down.D.Cols(); ++i){
        DownSign *= eigenObdm_down.D[i] / abs(eigenObdm_down.D[i]);
        fulldet *= (1.0 + eigenObdm_down.D[i]);
        if (1. + eigenObdm_down.D[i].real() < 0.0)
            std::cout << "Negative eigenvalue: " << eigenObdm_down.D[i] << std::endl;
        /*if (abs(eigenObdm_down.D[i].imag()) / abs(eigenObdm_down.D[i]) > 0.00001)
            std::cout << "Imaginary eigenvalue: " << eigenObdm_down.D[i] << std::endl;*/
        if (abs(1.0+eigenObdm_down.D[i]) < 0.5)
            std::cout << "Small eigenvalue 1+U: " << 1.0 + eigenObdm_down.D[i] << std::endl;
    }

    hst.GetHSfield().ComputeGlobalLogHSRatio_BDM();
    //std::cout << "Full Log HS Weight: " << hst.GetHSfield().HSWeight << "\n";

    //std::cout << "Full determinant: " << fulldet << std::endl;

    std::cout << "DownSign: " << DownSign << std::endl;

        //fourier sum for the ratio of determiants
    Tmp_Weight_up = 0.0;
    Tmp_Weight_down = 0.0;

    cdouble Tmp_Weight_up2 = 1.0;
    cdouble Tmp_Weight_down2 = 1.0;

    //Fourier sum for up and down spins have to be fully separated in this case!!!!
    for (int m = 0; m < UP_fourier_sum_max; ++m){
        Tmp_Weight_up2 = 1.0;

        for (int i = 0; i < eigenObdm_up.D.Cols(); ++i)
            {
                Tmp_Weight_up2 *= (1.0 + exp(-(double) m * UP_varphi)*eigenObdm_up.D[i]) / (1.0 + eigenObdm_up.D[i]);
                TempDiag[i] = 1.0 / (1.0 + exp((double) m * UP_varphi) / eigenObdm_up.D[i]);
            }
        

        Tmp_Weight_up2 *= 1.0 / (double) UP_fourier_sum_max
                        * exp(UP_varphi * ((double) m * (double) N_UP));

        for (int i = 0; i < eigenObdm_up.D.Cols(); ++i)
            {
                DiagStore_up[i] += TempDiag[i] * Tmp_Weight_up2;
            }
            Tmp_Weight_up += Tmp_Weight_up2;
    }

    for (int n = 0; n < DN_fourier_sum_max; ++n){
        Tmp_Weight_down2 = 1.0;

        for (int i = 0; i < eigenObdm_down.D.Cols(); ++i)
            {
                Tmp_Weight_down2 *= (1.0 + exp(-(double) n * DN_varphi)*eigenObdm_down.D[i]) / (1.0 + eigenObdm_down.D[i]);
                TempDiag2[i] = 1.0 / (1.0 + exp((double) n * DN_varphi) / eigenObdm_down.D[i]);
            }
        

        Tmp_Weight_down2 *= 1.0 / (double) DN_fourier_sum_max
                        * exp(DN_varphi * ((double) n * (double) N_DN));

        for (int i = 0; i < eigenObdm_down.D.Cols(); ++i)
            {
                DiagStore_down[i] += TempDiag2[i] * Tmp_Weight_down2;
            }

        Tmp_Weight_down += Tmp_Weight_down2;
    }


}

template void CanonicalObservables<DiscreteBalanced>::ComputeFourierTwoBody();
template void CanonicalObservables<DiscretePolarised>::ComputeFourierTwoBody();


template <typename T>
void  CanonicalObservables<T>::AddPotentialEnergy(cdouble& Energy, std::vector<cdouble> const& PositionDensity_up, std::vector<cdouble> const& PositionDensity_dn){
    for (int x = 0; x < parameters.Volume; ++x){
        Energy += (PositionDensity_up[x] + PositionDensity_dn[x]) * Trap[x];
        Energy += parameters.U * (PositionDensity_up[x] * PositionDensity_dn[x]) / Tmp_Weight_up / Tmp_Weight_down;
    }
}

template void CanonicalObservables<DiscreteBalanced>::AddPotentialEnergy(cdouble& Energy, std::vector<cdouble> const& PositionDensity_up, std::vector<cdouble> const& PositionDensity_dn);
template void CanonicalObservables<DiscretePolarised>::AddPotentialEnergy(cdouble& Energy, std::vector<cdouble> const& PositionDensity_up, std::vector<cdouble> const& PositionDensity_dn);

template <typename T>
void CanonicalObservables<T>::AddKineticEnergy(cdouble& Energy, std::vector<cdouble> const& MomentumDensity_up, std::vector<cdouble> const& MomentumDensity_dn){
    for (int k = 0; k < parameters.Volume; ++k){
        Energy += (MomentumDensity_up[k] + MomentumDensity_dn[k]) * Dispersion[k];
    }
}

template void CanonicalObservables<DiscreteBalanced>::AddKineticEnergy(cdouble& Energy, std::vector<cdouble> const& MomentumDensity_up, std::vector<cdouble> const& MomentumDensity_dn);
template void CanonicalObservables<DiscretePolarised>::AddKineticEnergy(cdouble& Energy, std::vector<cdouble> const& MomentumDensity_up, std::vector<cdouble> const& MomentumDensity_dn);

template <typename T>
void CanonicalObservables<T>::GetPositionDensity(std::vector<cdouble>& PositionDensity_up, std::vector<cdouble>& PositionDensity_down){
    for (int x = 0; x < parameters.Volume; ++x){
        PositionDensity_up[x] = DiagStore_up[x] * Tmp_Weight_down;
        PositionDensity_down[x] = DiagStore_down[x] * Tmp_Weight_up;
    }

    eigenObdm_up.RecomposeDiagonal(PositionDensity_up, PositionDensity_up , WS::tmp1());
    eigenObdm_down.RecomposeDiagonal(PositionDensity_down, PositionDensity_down , WS::tmp1());
}

template void CanonicalObservables<DiscreteBalanced>::GetPositionDensity(std::vector<cdouble>& PositionDensity_up, std::vector<cdouble>& PositionDensity_down);
template void CanonicalObservables<DiscretePolarised>::GetPositionDensity(std::vector<cdouble>& PositionDensity_up, std::vector<cdouble>& PositionDensity_down);

template <typename T>
void CanonicalObservables<T>::GetMomentumDensity(std::vector<cdouble>& MomentumDensity_up , std::vector<cdouble>& MomentumDensity_dn){
    //Compute fourier transform in the Q and R matrices in eigenObdm_up and eigenObdm_down
    //and store the result in the Q and R matrices of the eigenObdm_up and eigenObdm_down
    
    eigenObdm_up.Q.ExecuteFwd();
    WS::tmp1().Truncate(eigenObdm_up.Q.Rows(), eigenObdm_up.Q.Cols());
    WS::tmp1().TransposeFrom(eigenObdm_up.R);
    WS::tmp1().ExecuteBwd();
    eigenObdm_up.R.TransposeFrom(WS::tmp1());

    for (auto& q : eigenObdm_up.Q)
            q /= sqrt(parameters.Volume);

        for (auto& r : eigenObdm_up.R)
            r /= sqrt(parameters.Volume);

    eigenObdm_down.Q.ExecuteFwd();
    WS::tmp1().Truncate(eigenObdm_down.Q.Rows(), eigenObdm_down.Q.Cols());
    WS::tmp1().TransposeFrom(eigenObdm_down.R);
    WS::tmp1().ExecuteBwd();
    eigenObdm_down.R.TransposeFrom(WS::tmp1());

    for (auto& q : eigenObdm_down.Q)
            q /= sqrt(parameters.Volume);

        for (auto& r : eigenObdm_down.R)
            r /= sqrt(parameters.Volume);

    WS::tmp1().ResetSize();

    for (int x = 0; x < parameters.Volume; ++x){
        MomentumDensity_up[x] = DiagStore_up[x] * Tmp_Weight_down;
        MomentumDensity_dn[x] = DiagStore_down[x] * Tmp_Weight_up;
    }

    eigenObdm_up.RecomposeDiagonal(MomentumDensity_up, MomentumDensity_up , WS::tmp1());
    eigenObdm_down.RecomposeDiagonal(MomentumDensity_dn, MomentumDensity_dn , WS::tmp1());

}

template void CanonicalObservables<DiscreteBalanced>::GetMomentumDensity(std::vector<cdouble>& MomentumDensity_up , std::vector<cdouble>& MomentumDensity_dn);
template void CanonicalObservables<DiscretePolarised>::GetMomentumDensity(std::vector<cdouble>& MomentumDensity_up , std::vector<cdouble>& MomentumDensity_dn);

//function assumes that the matrices Q and R of eigenObdm_up and eigenObdm_down are already in momentum space
template <typename T>
void CanonicalObservables<T>::GetDensityDensiy(std::vector<cdouble>& DensityDensiy){
    for (int x = 0; x < parameters.Volume; ++x){
        TempDiag[x] = DiagStore_up[x];
        TempDiag2[x] = DiagStore_down[x];
    }

    eigenObdm_up.RecomposeDiagonal(TempDiag, TempDiag , WS::tmp1());
    eigenObdm_down.RecomposeDiagonal(TempDiag2, TempDiag2 , WS::tmp1());

    //compute outer product of TempDiag and TempDiag2
    for (int x = 0; x < parameters.Volume; ++x){
        for (int y = 0; y < parameters.Volume; ++y){
            DensityDensiy[x + y * parameters.Volume] = TempDiag[x] * TempDiag2[y];
        }
    }
}

template void CanonicalObservables<DiscreteBalanced>::GetDensityDensiy(std::vector<cdouble>& DensityDensiy);
template void CanonicalObservables<DiscretePolarised>::GetDensityDensiy(std::vector<cdouble>& DensityDensiy);

template <typename T>
void CanonicalObservables<T>::GetShotNoise(std::vector<cdouble>& ShotNoise, std::vector<cdouble>& MeanDens_up, std::vector<cdouble>& MeanDens_dn){
        
    eigenObdm_up.Q.ExecuteFwd();
    WS::tmp1().Truncate(eigenObdm_up.Q.Rows(), eigenObdm_up.Q.Cols());
    WS::tmp1().TransposeFrom(eigenObdm_up.R);
    WS::tmp1().ExecuteBwd();
    eigenObdm_up.R.TransposeFrom(WS::tmp1());

    for (auto& q : eigenObdm_up.Q)
            q /= sqrt(parameters.Volume);

        for (auto& r : eigenObdm_up.R)
            r /= sqrt(parameters.Volume);

    eigenObdm_down.Q.ExecuteFwd();
    WS::tmp1().Truncate(eigenObdm_down.Q.Rows(), eigenObdm_down.Q.Cols());
    WS::tmp1().TransposeFrom(eigenObdm_down.R);
    WS::tmp1().ExecuteBwd();
    eigenObdm_down.R.TransposeFrom(WS::tmp1());

    for (auto& q : eigenObdm_down.Q)
            q /= sqrt(parameters.Volume);

        for (auto& r : eigenObdm_down.R)
            r /= sqrt(parameters.Volume);

    WS::tmp1().ResetSize();
    
        for (int x = 0; x < parameters.Volume; ++x){
        TempDiag[x] = DiagStore_up[x];
        TempDiag2[x] = DiagStore_down[x];
    }

    eigenObdm_up.RecomposeDiagonal(TempDiag, TempDiag , WS::tmp1());
    eigenObdm_down.RecomposeDiagonal(TempDiag2, TempDiag2 , WS::tmp1());

    //compute outer product of TempDiag and TempDiag2
    for (int x = 0; x < parameters.Volume; ++x){
        for (int y = 0; y < parameters.Volume; ++y){
            ShotNoise[x + y * parameters.Volume] = (TempDiag[x] - MeanDens_up[x] * Tmp_Weight_up)  * (TempDiag2[y] - MeanDens_dn[y] * Tmp_Weight_down);
        }
    }

}

template void CanonicalObservables<DiscreteBalanced>::GetShotNoise(std::vector<cdouble>& ShotNoise, std::vector<cdouble>& MeanDens_up, std::vector<cdouble>& MeanDens_dn);
template void CanonicalObservables<DiscretePolarised>::GetShotNoise(std::vector<cdouble>& ShotNoise, std::vector<cdouble>& MeanDens_up, std::vector<cdouble>& MeanDens_dn);


//Computes pair momentum in n(k,k')n(k,k') in whatever basis the obdm is in (momentum or position space)
template <typename T>
void CanonicalObservables<T>::GetPairMomentum(std::vector<cdouble>& PairMomentum){
    for (int x = 0; x < parameters.Volume; ++x){
        TempDiag[x] = DiagStore_up[x];
        TempDiag2[x] = DiagStore_down[x];
    }

    //Recompose full matrix drom eigenObdm_up and eigenObdm_down
    eigenObdm_up.ReconstructU(WS::tmp1(), WS::tmp_obdm(0).Q);
    eigenObdm_down.ReconstructU(WS::tmp2() , WS::tmp_obdm(0).Q);

    for (int x = 0; x < parameters.Volume * parameters.Volume; ++x){
        PairMomentum[x] = WS::tmp1()[x] * WS::tmp2()[x];
    }
}

template void CanonicalObservables<DiscreteBalanced>::GetPairMomentum(std::vector<cdouble>& PairMomentum);
template void CanonicalObservables<DiscretePolarised>::GetPairMomentum(std::vector<cdouble>& PairMomentum);
