/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "analysis.h"

//auto constexpr fftw_planner = FFTW_ESTIMATE;
auto constexpr fftw_planner = FFTW_MEASURE;
//auto constexpr fftw_planner = FFTW_PATIENT;
//auto constexpr fftw_planner = FFTW_EXHAUSTIVE;

//factor 2 is from pagging with zeroes.
autocorrelation::autocorrelation(int data_len, int number_observables)
    :
    data_len(data_len),
    number_observables(number_observables),
    data(data_len * number_observables)
{   
        int n[] = {data_len};
        //do ffts in place
        //rank 1 ffts on each list of observables (contiguous in memory!)
        pfwd = plan_ptr(fftw_plan_many_dft(
            1, n , number_observables,
            reinterpret_cast<fftw_complex*>(data.data()), NULL, 1, data_len,
            reinterpret_cast<fftw_complex*>(data.data()), NULL, 1, data_len,
            FFTW_FORWARD, fftw_planner), PlanDeleter());

        pbwd = plan_ptr(fftw_plan_many_dft(
            1, n, number_observables,
            reinterpret_cast<fftw_complex*>(data.data()), NULL, 1 , data_len,
            reinterpret_cast<fftw_complex*>(data.data()), NULL, 1 , data_len,
            FFTW_BACKWARD, fftw_planner), PlanDeleter()); 
}

void autocorrelation::AddData(int sample_index, std::vector<cdouble> const& dataIn) {
    for (int i = 0; i < number_observables; ++i) {
        data[sample_index + i * data_len] = dataIn[i];
    }
}

void autocorrelation::ComputeAutocorrelation() {
    fftw_execute(pfwd.get());
    for (int i = 0; i < data.size(); ++i) {
        data[i] /= sqrt((data_len));
    }
    for (int i = 0; i < data.size(); ++i) {
        data[i] *= std::conj(data[i]);
    }
    fftw_execute(pbwd.get());
    for (int i = 0; i < data.size(); ++i) {
        data[i] /= sqrt((data_len));
    }
}

void autocorrelation::OutputHDF5(std::string const& filename, std::string const& ObsName) {

    HighFive::File file(filename, HighFive::File::ReadWrite | HighFive::File::Create);
    if (file.exist(ObsName) == true && file.getGroup(ObsName).exist("AC") == true){
        std::cout << "Group " << ObsName << " already exists in file: " << filename << "\n"
        << "Skipping writing to file: " << filename << std::endl;
    }                             
	else{
        std::cout << "Writing to file: " << filename << std::endl;
        auto group = [&file, &ObsName]{
				if (file.exist(ObsName) == true)
					return file.getGroup(ObsName);
				else
					return file.createGroup(ObsName);
			}();
        auto InnerGroup = group.createGroup("AC");
        for (unsigned int i = 0; i < number_observables; ++i) {
            auto Dataset = InnerGroup.createDataSet<cdouble>("Obs_" + std::to_string(i) , HighFive::DataSpace(data_len));
            Dataset.write(data.data() + i * data_len);
        }
    }

}

//template <typename T>
statistics::statistics(int number_observables)
    :
    number_observables{number_observables},
    Mean(number_observables),
    Var(number_observables),
    VarImag(number_observables),
    sample_counter{0}
{ }

//template statistics<cdouble>::statistics(int number_observables);
//template statistics<double>::statistics(int number_observables);

//template <typename T>
statistics::statistics(std::vector<cdouble> const& MeanIn)
    :
    number_observables(MeanIn.size()),
    Mean{MeanIn},
    Var(MeanIn.size()),
    VarImag(MeanIn.size()),
    sample_counter(0)
{ }

//template statistics<cdouble>::statistics(std::vector<cdouble> const& MeanIn);
//template statistics<double>::statistics(std::vector<double> const& MeanIn);

//these are not normalized, that should be done before writing to file
//THE VARIANCE SHOULD BE DONE FOR REAL ANS IMAGINARY PART SEPARATELY!!!
void statistics::AddData(std::vector<cdouble> const& DataIn) {
    for (int i = 0; i < number_observables; ++i) {
        Mean[i] += DataIn[i];
        Var[i] += DataIn[i].real() * DataIn[i].real();
        VarImag[i] += DataIn[i].imag() * DataIn[i].imag();
    }
    ++sample_counter;
}

//template void statistics<cdouble>::AddData(std::vector<cdouble> const& DataIn);
//template void statistics<double>::AddData(std::vector<double> const& DataIn);

/*
template <typename T>
void statistics<T>::AddDataComplex(std::vector<cdouble> const& DataIn) {
    for (int i = 0; i < number_observables; ++i) {
        Mean[i] += DataIn[i];
        Var[i] += DataIn[i].real() * DataIn[i].real();
    }
    std::cout << "doing real shit" << std::endl;
    ++sample_counter;
}

template void statistics<cdouble>::AddDataComplex(std::vector<cdouble> const& DataIn);
*/

//normalize by sample_counter and subsctract mean^2
void statistics::Normalize() {
    for (int i = 0; i < number_observables; ++i) {
        Mean[i] /= sample_counter;
        Var[i] /= sample_counter;
        VarImag[i] /= sample_counter;
        Var[i] -= Mean[i].real() * Mean[i].real();
        VarImag[i] -= Mean[i].imag() * Mean[i].imag();
    }
}

//template void statistics<cdouble>::Normalize();
//template void statistics<double>::Normalize();

//create one dataset each for all mean and variance
void statistics::OutputHDF5(std::string const& filename, std::string const& ObsName) {
    HighFive::File file(filename, HighFive::File::ReadWrite | HighFive::File::Create);

	if (file.exist(ObsName) == true && file.getGroup(ObsName).exist("Mean") == true){
        std::cout << "Group " << ObsName << " already exists in file: " << filename << "\n"
        << "Skipping writing to file: " << filename << std::endl;
    }                             
	else{
        std::cout << "Writing to file: " << filename << std::endl;
        auto group = [&file, &ObsName]{
				if (file.exist(ObsName) == true)
					return file.getGroup(ObsName);
				else
					return file.createGroup(ObsName);
			}();
        group.createDataSet("Mean", Mean);
        group.createDataSet("Var", Var);
        group.createDataSet("VarImag", VarImag);
    }
}


//template void statistics<cdouble>::OutputHDF5(std::string const& filename, std::string const& ObsName);
//template void statistics<double>::OutputHDF5(std::string const& filename, std::string const& ObsName);

//create one dataset each for all mean and variance
void statistics::OutputHDF5(std::string const& filename, std::string const& ObsName, unsigned int const& Vol, unsigned int const& Vol2) {
    HighFive::File file(filename, HighFive::File::ReadWrite | HighFive::File::Create);

	if (file.exist(ObsName) == true && file.getGroup(ObsName).exist("Mean") == true){
        std::cout << "Group " << ObsName << " already exists in file: " << filename << "\n"
        << "Skipping writing to file: " << filename << std::endl;
    }                             
	else{
        std::cout << "Writing to file: " << filename << std::endl;
        auto group = [&file, &ObsName]{
				if (file.exist(ObsName) == true)
					return file.getGroup(ObsName);
				else
					return file.createGroup(ObsName);
			}();
        
        auto Dataset = group.createDataSet<cdouble>("Mean", HighFive::DataSpace(Vol, Vol2));
        Dataset.write_raw(Mean.data());

        auto Dataset2 = group.createDataSet<cdouble>("Var", HighFive::DataSpace(Vol, Vol2));
        Dataset2.write_raw(Var.data());
        
        auto Dataset3 = group.createDataSet<cdouble>("VarImag", HighFive::DataSpace(Vol, Vol2));
        Dataset3.write_raw(VarImag.data());
        


        /*auto Dataset = group.template createDataSet<T>("Mean", HighFive::DataSpace(Vol, Vol2));
        Dataset.write_raw(Mean.data());

        auto Dataset2 = group.template createDataSet<T>("Var", HighFive::DataSpace(Vol, Vol2));
        Dataset2.write_raw(Var.data());
        
        auto Dataset3 = group.template createDataSet<T>("VarImag", HighFive::DataSpace(Vol, Vol2));
        Dataset3.write_raw(VarImag.data());*/
    }
}

//template void statistics<cdouble>::OutputHDF5(std::string const& filename, std::string const& ObsName, unsigned int const& Vol, unsigned int const& Vol2);
//template void statistics<double>::OutputHDF5(std::string const& filename, std::string const& ObsName,  unsigned int const& Vol, unsigned int const& Vol2);
