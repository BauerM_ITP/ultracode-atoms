/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef FAKEITER_H
#define FAKEITER_H

template <typename T>
struct FakeBaseIter {
	int operator* () const { return static_cast<T const&>(*this).operator*(); }
	FakeBaseIter<T>& operator++() { return static_cast<T&>(*this).operator++(); }
	FakeBaseIter<T>& operator--() { return static_cast<T&>(*this).operator--(); }

	bool operator< (FakeBaseIter const& other) const
	{ return static_cast<T const&>(*this).operator<(other); }
	bool operator!= (FakeBaseIter const& other) const
	{ return static_cast<T const&>(*this).operator!=(other); }

	T operator+ (int const offset) const
	{ return static_cast<T const&>(*this).operator+(offset); }
	T operator- (int const offset) const
	{ return static_cast<T const&>(*this).operator-(offset); }

protected:
	int val;
};

struct FakeFwdIter : public FakeBaseIter<FakeFwdIter> {
	inline FakeFwdIter(int const v) { val = v; }

	inline int operator* () const { return val; }
	inline FakeFwdIter& operator++ () { ++val; return *this; }
	inline FakeFwdIter& operator-- () { --val; return *this; }

	inline bool operator<  (FakeBaseIter<FakeFwdIter> const& other) const { return val <  *other; }
	inline bool operator!= (FakeBaseIter<FakeFwdIter> const& other) const { return val != *other; }

	inline FakeFwdIter operator+ (int const offset) const { return FakeFwdIter{val + offset}; }
	inline FakeFwdIter operator- (int const offset) const { return FakeFwdIter{val - offset}; }
};

struct FakeRevIter : public FakeBaseIter<FakeRevIter> {
	inline FakeRevIter(int const v) { val = v; }

	inline int operator* () const { return val; }
	inline FakeRevIter& operator++ () { --val; return *this; }
	inline FakeRevIter& operator-- () { ++val; return *this; }

	inline bool operator<  (FakeBaseIter<FakeRevIter> const& other) const { return val <  *other; }
	inline bool operator!= (FakeBaseIter<FakeRevIter> const& other) const { return val != *other; }

	inline FakeRevIter operator+ (int const offset) const { return FakeRevIter{val - offset}; }
	inline FakeRevIter operator- (int const offset) const { return FakeRevIter{val + offset}; }
};

struct FakeIota {
	inline FakeIota(int const i0, int const i1)
	: init{i0}
	, max{i1}
	{ }

	inline auto cbegin() const { return FakeFwdIter{init}; }
	inline auto cend()   const { return FakeFwdIter{max};  }

	inline auto crbegin() const { return FakeRevIter{max-1}; }
	inline auto crend()   const { return FakeRevIter{init-1};  }

protected:
	int const init;
	int const max;
};

#endif
