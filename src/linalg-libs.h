/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef LINALG_LIBS_H
#define LINALG_LIBS_H

#if __has_include(<mkl.h>)
    #define MKL_Complex16 __complex__ double
	#include <mkl.h>
	#include <mkl_lapack.h>
	#include <mkl_blacs.h>
	#include <mkl_types.h>
	using myComplex = MKL_Complex16;
	#define LinAlg(func) func##_
#else
	#include <cblas.h>
	#include <lapack.h>
	#define LinAlg(func) LAPACK_##func
	using myComplex = __complex__ double;
#endif

extern "C" {
void zgemm_(char const *transa, char const *transb, lapack_int const *m, lapack_int const *n, lapack_int const *k,
		lapack_complex_double const *alpha,
		lapack_complex_double const *a, int const *lda,
		lapack_complex_double const *b, int const *ldb,
		lapack_complex_double const *beta,
		lapack_complex_double *c, lapack_int const *ldc);
}
#define LAPACK_zgemm zgemm_


#endif
