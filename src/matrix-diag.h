/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef xMATRIX_DIAG_H
#define xMATRIX_DIAG_H

#include <vector>

#include "params.h"
#include "matrix_base.h"
#include "matrix_diag.h"

template <typename T, typename U>
void LeftMultiplyByDiagonal(CMatrixBase<T>& mat, std::vector<U> const& Diagonal, double const factor = 1.0){
	int const Cols = mat.Cols();
	int const Rows = mat.Rows();
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				mat[j+i*Rows] *= Diagonal[j] * factor;
}

template <typename T, typename U>
void LeftMultiplyByDiagonal(CMatrixBase<T>& mat, DiagonalMatrix<U> const& Diagonal, double const factor = 1.0){
	int const Cols = mat.Cols();
	int const Rows = mat.Rows();
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				mat[j+i*Rows] *= Diagonal[j] * factor;
}

template <typename T, typename U>
void LeftMultiplyByDiagonal(T* Matrix, U* Diagonal, int Rows, int Cols){
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				Matrix[j+i*Rows] *= Diagonal[j];
}

template <typename T, typename U>
void RightMultiplyByDiagonal(CMatrixBase<T>& mat, DiagonalMatrix<U> const& Diagonal) {
	int const Cols = mat.Cols();
	int const Rows = mat.Rows();
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				mat[j+i*Rows] *= Diagonal[i];
}

template <typename T, typename U>
void RightMultiplyByDiagonal(T* Matrix, U* Diagonal, int Rows, int Cols){
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				Matrix[j+i*Rows] *= Diagonal[i];
}

template <typename T, typename U>
void LeftMultiplyByInverseDiagonal(CMatrixBase<T>& mat, std::vector<U> const& Diagonal, double const factor = 1.0) {
	int const Cols = mat.Cols();
	int const Rows = mat.Rows();
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				mat[j+i*Rows] /= (Diagonal[j] * factor);
}

template <typename T, typename U>
void LeftMultiplyByInverseDiagonal(CMatrixBase<T>& mat, DiagonalMatrix<U> const& Diagonal, double const factor = 1.0) {
	int const Cols = mat.Cols();
	int const Rows = mat.Rows();
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				mat[j+i*Rows] /= (Diagonal[j] * factor);
}

template <typename T, typename U>
void LeftMultiplyByInverseDiagonal(T* Matrix, U* Diagonal, int Rows, int Cols){
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				Matrix[j+i*Rows] /= Diagonal[j];
}

template <typename T, typename U>
void RightMultiplyByInverseDiagonal(T* Matrix, U* Diagonal, int Rows, int Cols){
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				Matrix[j+i*Rows] /= Diagonal[i];
}

template <typename T, typename U>
void RightMultiplyByInverseDiagonal(CMatrixBase<T>& mat, DiagonalMatrix<U> const& Diagonal){
	int const Cols = mat.Cols();
	int const Rows = mat.Rows();
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				mat[j+i*Rows] /= Diagonal[i];
}

/*
template <typename T, typename U>
void LeftMultiplyByInverseDiagonalNormalize(T* Matrix, U* Diagonal, int Rows, int Cols){
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				Matrix[j+i*Rows] /= Diagonal[j] * pow(Msize, 2);
}
*/

template <typename T, typename U>
void LeftMultiplyByInverseDiagonalNormalize(CMatrixBase<T>& mat,
		std::vector<U> const& Diagonal, double const factor = 1.0) {
	int const Cols = mat.Cols();
	int const Rows = mat.Rows();
	double const myFactor = pow(std::max(Rows, Cols), 2) * factor;
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				mat[j+i*Rows] /= Diagonal[j] * myFactor;
}

template <typename T, typename U>
void LeftMultiplyByInverseDiagonalNormalize(CMatrixBase<T>& mat,
		DiagonalMatrix<U> const& Diagonal, double const factor = 1.0) {
	int const Cols = mat.Cols();
	int const Rows = mat.Rows();
	double const myFactor = pow(std::max(Rows, Cols), 2) * factor;
	#pragma omp parallel for
		for (int i = 0; i < Cols; i++)
			for (int j = 0; j < Rows; j++)
				mat[j+i*Rows] /= Diagonal[j] * myFactor;
}

#endif
