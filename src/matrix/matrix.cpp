/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "matrix.h"
#include "matrix_fft.h"
#include "measureTime.h"

Matrix::Matrix(int const Rows, int const Cols)
	: MatrixBase<Matrix, cdouble>{}
	, data_{std::vector<cdouble, AlignmentAllocator<cdouble>>(Rows * Cols)}
	, originalCols{Cols}
	, originalRows{Rows}
	, currentCols{Cols}
	, currentRows{Rows}
{ }

Matrix::Matrix(int const Volume)
	: Matrix(Volume, Volume)
{ }

void Matrix::Truncate(int newRows, int newCols) {
	currentRows = std::min(originalRows, newRows);
	currentCols = std::min(originalCols, newCols);
}

void Matrix::ResetSize() { currentCols = originalCols; currentRows = originalRows; }

//Runcates rows of a col major Matrix 
template <typename T>
void Matrix::TruncateRows(CMatrixBase<T> const& MatrixIn) {
	int const RowsOut = Rows();
	int const RowsIn  = MatrixIn.Rows();
	int const Cols	  = MatrixIn.Cols();	// same for both
	if (RowsOut >= RowsIn) {
		std::cout << "RowsOut >= RowsIn" << "\n";
	} else {
		#pragma omp parallel for
		for (int i = 0; i < Cols; ++i)
			for (int j = 0; j < RowsOut; ++j)
				data_[j+i*RowsOut] = MatrixIn[j+i*RowsIn];
	}
}
template void Matrix::TruncateRows(CMatrixBase<Matrix> const& MatrixIn);
template void Matrix::TruncateRows(CMatrixBase<MatrixFFT> const& MatrixIn);

template <typename T>
void Matrix::TransposeFrom(CMatrixBase<T> const& MatrixIn) {
	int const cols = Rows();
	int const rows = Cols();
    for (int j = 0; j < rows; j++){
        for (int i = 0; i < cols; i++){
            data_[i+j*cols] = MatrixIn[j+i*rows];
        }
    }
}
template void Matrix::TransposeFrom(CMatrixBase<Matrix> const& MatrixIn);
template void Matrix::TransposeFrom(CMatrixBase<MatrixFFT> const& MatrixIn);
