/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef MATRIX_H
#define MATRIX_H

#include <vector>
#include <complex>


#include "aligned_alloc.h"

#include "matrix_base.h"

class Matrix : public MatrixBase<Matrix, cdouble> {
public:

	friend struct MatrixBase<Matrix, cdouble>;
	inline int const& Cols() const { return currentCols; }
	inline int const& Rows() const { return currentRows; }

	inline int const& MaxCols() const { return originalCols; }
	inline int const& MaxRows() const { return originalRows; }

	inline cdouble* data() { return data_.data(); }
	inline cdouble const* data() const { return data_.data(); }
	inline myComplex* c_data()
	{ return reinterpret_cast<myComplex*>(data_.data()); }
	inline myComplex const* c_data() const
	{ return reinterpret_cast<myComplex const*>(data_.data()); }

	inline cdouble& operator[] (int i) { return data_[i]; }
	inline cdouble operator[] (int i) const { return data_[i]; }

	inline auto begin() { return data_.begin(); }
	inline auto end() { return data_.end(); }
	inline auto truncatedEnd() { return data_.begin() + currentRows * currentCols; }

	inline auto cbegin() const { return data_.cbegin(); }
	inline auto cend() const { return data_.cend(); }
	inline auto ctruncatedEnd() const { return data_.cbegin() + currentRows * currentCols; }

	inline auto& GetVector() { return data_; }

	void Truncate(int newRows, int newCols);
	void ResetSize();

	template <typename T>
	void TruncateRows(CMatrixBase<T> const& MatrixIn);

	template <typename T>
	void TransposeFrom(CMatrixBase<T> const& MatrixIn);

	Matrix(int const Volume);
	Matrix(int const Rows, int const Cols);

private:
	std::vector<cdouble, AlignmentAllocator<cdouble>> data_;
	int originalCols, originalRows;
	int currentCols, currentRows;
};

#endif
