/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef MATRIX_BASE_H
#define MATRIX_BASE_H

#include <vector>
#include <complex>
#include <fftw3.h>
#include <iostream>

#include "myVec.h"
#include "params.h"
#include "measureTime.h"
#include "aligned_alloc.h"
#include "linalg-libs.h"

template <typename T, typename U>
struct MatrixBase {
	inline int const& Cols() const
	{ return static_cast<T const&>(*this).Cols(); }
	inline int const& Rows() const
	{ return static_cast<T const&>(*this).Rows(); }

	inline int const& MaxCols() const
	{ return static_cast<T const&>(*this).MaxCols(); }
	inline int const& MaxRows() const
	{ return static_cast<T const&>(*this).MaxRows(); }

	inline U* data() { return static_cast<T&>(*this).data(); }
	inline U const* data() const { return static_cast<T const&>(*this).data(); }

	template<typename U_ = U,
		std::enable_if_t<is_complex_t<U_>::value
			&& std::is_same<typename U_::value_type, double>::value >* = nullptr>
	inline myComplex* c_data() { return static_cast<T&>(*this).c_data(); }
	template<typename U_ = U,
		std::enable_if_t<is_complex_t<U_>::value
			&& std::is_same<typename U_::value_type, double>::value >* = nullptr>
	inline myComplex const* c_data() const
	{ return static_cast<T const&>(*this).c_data(); }

	inline U& operator[] (int i)
	{ return static_cast<T&>(*this).operator[](i); }
	inline U operator[] (int i) const
	{ return static_cast<T const&>(*this).operator[](i); }

	void Truncate(int newRows, int newCols)
	{ static_cast<T&>(*this).Truncate(newRows, newCols); }

	void ResetSize() { static_cast<T&>(*this).ResetSize(); }

	auto begin() { return static_cast<T&>(*this).begin(); }
	auto end() { return static_cast<T&>(*this).end(); }
	auto truncatedEnd() { return static_cast<T&>(*this).truncatedEnd(); }

	auto cbegin() const { return static_cast<T const&>(*this).cbegin(); }
	auto cend() const { return static_cast<T const&>(*this).cend(); }
	auto ctruncatedEnd() const { return static_cast<T const&>(*this).ctruncatedEnd(); }

	auto& GetVector() { return static_cast<T&>(*this).GetVector(); }

	template <typename T2>
	void TruncateRows(MatrixBase<T2, U> const& MatrixIn)
	{ static_cast<T&>(*this).TruncateRows(MatrixIn); }

	template <typename T2>
	void TransposeFrom(MatrixBase<T2, U> const& MatrixIn)
	{ static_cast<T&>(*this).TransposeFrom(MatrixIn); }

	/*
	MatrixBase() { }
	MatrixBase(MatrixBase&&) = default;
	MatrixBase& operator= (MatrixBase const&) = delete;
	MatrixBase& operator= (MatrixBase&&) = default;
	*/
};

template <typename T>
using CMatrixBase = MatrixBase<T, cdouble>;

#endif
