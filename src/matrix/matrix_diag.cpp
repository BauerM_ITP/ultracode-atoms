/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "matrix_diag.h"

template <typename T>
DiagonalMatrix<T>::DiagonalMatrix(int const Volume)
	: MatrixBase<DiagonalMatrix<T>, T>{}
	, data_{std::vector<T, AlignmentAllocator<T>>(Volume)}
	, originalSize{Volume}
	, currentSize{Volume}
{ }

template <typename T>
void DiagonalMatrix<T>::Truncate(int newSize) {
	currentSize = std::min(originalSize, newSize);
}

template <typename T>
void DiagonalMatrix<T>::ResetSize() { currentSize = originalSize; }

template class DiagonalMatrix<double>;
template class DiagonalMatrix<cdouble>;
