/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef MATRIX_DIAG_H
#define MATRIX_DIAG_H

#include "matrix_base.h"

template <typename T>
class DiagonalMatrix : public MatrixBase<DiagonalMatrix<T>, T> {
public:

	friend struct MatrixBase<DiagonalMatrix<T>, T>;
	inline int const& Cols() const { return currentSize; }
	inline int const& Rows() const { return currentSize; }

	inline int const& MaxCols() const { return originalSize; }
	inline int const& MaxRows() const { return originalSize; }

	inline T* data() { return data_.data(); }
	inline T const* data() const { return data_.data(); }
    inline myComplex* c_data()
        { return reinterpret_cast<myComplex*>(data_.data()); }

	inline T& operator[] (int i) { return data_[i]; }
	inline T operator[] (int i) const { return data_[i]; }

	inline auto begin() { return data_.begin(); }
	inline auto end() { return data_.end(); }
	inline auto truncatedEnd() { return data_.begin() + currentSize; }

	inline auto cbegin() const { return data_.cbegin(); }
	inline auto cend() const { return data_.cend(); }
	inline auto ctruncatedEnd() const { return data_.cbegin() + currentSize; }

	inline auto& GetVector() { return data_; }

	void Truncate(int newSize);
	void ResetSize();

	DiagonalMatrix(int const Volume);

private:
	std::vector<T, AlignmentAllocator<T>> data_;
	int originalSize;
	int currentSize;
};
#endif
