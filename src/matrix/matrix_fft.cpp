/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "matrix_fft.h"
#include "measureTime.h"

//auto constexpr fftw_planner = FFTW_ESTIMATE;
auto constexpr fftw_planner = FFTW_MEASURE;
//auto constexpr fftw_planner = FFTW_PATIENT;
//auto constexpr fftw_planner = FFTW_EXHAUSTIVE;

void PlanDeleter::operator()(fftw_plan plan) const {
	fftw_destroy_plan(plan);
}

template <typename T>
auto MultiplyVector = [](std::vector<T> const vals) {
	auto out = static_cast<T>(1.0);
	for (auto e : vals) out *= e;
	return out;
};

MatrixFFT::MatrixFFT(std::vector<int> const Sizes, int const Cols)
	: MatrixBase<MatrixFFT, cdouble>{}
	, data_{std::vector<cdouble, AlignmentAllocator<cdouble>>(MultiplyVector<int>(Sizes) * Cols)}
	, originalCols{Cols}
	, originalRows{MultiplyVector<int>(Sizes)}
	, currentCols{Cols}
	, currentRows{MultiplyVector<int>(Sizes)}
	, FFTPlansFWD{std::vector<plan_ptr>(originalCols + 1)}
	, FFTPlansBWD{std::vector<plan_ptr>(originalCols + 1)}
{
    MeasureTimeX<1>([&]{
    for (int i = 1; i < originalCols + 1; i++){
        FFTPlansFWD[i]      =   plan_ptr(fftw_plan_many_dft(Sizes.size(), Sizes.data(), i,
                                    reinterpret_cast<fftw_complex*>(data_.data()), NULL,
                                    1, originalRows,
                                    reinterpret_cast<fftw_complex*>(data_.data()), NULL,
                                    1, originalRows,
                                    FFTW_FORWARD, fftw_planner), PlanDeleter());

        FFTPlansBWD[i]      =   plan_ptr(fftw_plan_many_dft(Sizes.size(), Sizes.data(), i,
                                    reinterpret_cast<fftw_complex*>(data_.data()), NULL,
                                    1, originalRows,
                                    reinterpret_cast<fftw_complex*>(data_.data()), NULL,
                                    1, originalRows,
                                    FFTW_BACKWARD, fftw_planner), PlanDeleter());
                 
		/*if (originalRows > 40
			if (i % (originalRows / 20) == 0){
				std::cout << (float) i / (float) originalRows * 100 << "% done" << std::endl; 
		}
        */
    }
    },"Create all FFT Plans");
}

MatrixFFT::MatrixFFT(std::vector<int> const Sizes)
	: MatrixFFT(Sizes, MultiplyVector<int>(Sizes))
{ }

void MatrixFFT::Truncate(int newRows, int newCols) {
	currentRows = std::min(originalRows, newRows);
	currentCols = std::min(originalCols, newCols);
}

void MatrixFFT::ExecuteFwd() { fftw_execute(FFTPlansFWD[currentCols].get()); }
void MatrixFFT::ExecuteBwd() { fftw_execute(FFTPlansBWD[currentCols].get()); }

void MatrixFFT::ResetSize() { currentCols = originalCols; currentRows = originalRows; }

//Runcates rows of a col major Matrix 
template <typename T>
void MatrixFFT::TruncateRows(CMatrixBase<T> const& MatrixIn) {
	int const RowsOut = Rows();
	int const RowsIn  = MatrixIn.Rows();
	int const Cols	  = MatrixIn.Cols();	// same for both
	if (RowsOut >= RowsIn) {
		std::cout << "RowsOut >= RowsIn" << "\n";
	} else {
		#pragma omp parallel for
		for (int i = 0; i < Cols; ++i)
			for (int j = 0; j < RowsOut; ++j)
				data_[j+i*RowsOut] = MatrixIn[j+i*RowsIn];
	}
}
template void MatrixFFT::TruncateRows(CMatrixBase<Matrix> const& MatrixIn);
template void MatrixFFT::TruncateRows(CMatrixBase<MatrixFFT> const& MatrixIn);

template <typename T>
void MatrixFFT::TransposeFrom(CMatrixBase<T> const& MatrixIn) {
	int const cols = Rows();
	int const rows = Cols();
    for (int j = 0; j < rows; j++){
        for (int i = 0; i < cols; i++){
            data_[i+j*cols] = MatrixIn[j+i*rows];
        }
    }
}
template void MatrixFFT::TransposeFrom(CMatrixBase<Matrix> const& MatrixIn);
template void MatrixFFT::TransposeFrom(CMatrixBase<MatrixFFT> const& MatrixIn);
