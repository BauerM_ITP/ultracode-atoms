/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef MEASURE_TIME_H
#define MEASURE_TIME_H

#include <chrono>
#include <iostream>

int constexpr MEASURE_LEVEL = 0;

template <int level, typename T>
void MeasureTimeX (T t, std::string name = "") {
	if constexpr(level <= MEASURE_LEVEL) {
	auto timerStart = std::chrono::high_resolution_clock::now();
	t();
	auto timerStop  = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(timerStop - timerStart);
	std::cout << "# " << name << " -- Run time: " << duration.count() << " mus\n";
	}
	else
		t();
}

#endif
