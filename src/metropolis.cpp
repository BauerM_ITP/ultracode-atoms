/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "params.h"

#include <mpi.h>
#include <omp.h>
#include <vector>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <complex>
#include <iostream>
#include <algorithm>

#include "rng.h"
#include "obdm.h"
#include "fakeIter.h"
#include "workspace.h"
#include "measureTime.h"
#include "matrix-diag.h"

#include "HStransforms.h"

template <typename T>
void PrintColMatrix(int Rows, int Cols, T* Matrix, int TruncatePrint = 8){
	int PrintRows = (Rows <= TruncatePrint? Rows : TruncatePrint);
	int PrintCols = (Cols <= TruncatePrint? Cols : TruncatePrint);

	std::cout << "prints are truncated at " << TruncatePrint << " Rows/Cols\n";
	for (int i = 0; i < PrintRows; i++){
		for (int j = 0; j < PrintCols; j++){
			std::cout << Matrix[i+j*Rows] << "\t";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}


template <typename T>
void SaveDensities(HSTransform<T>& hst, std::vector<std::ofstream>& densProf,
		std::vector<std::ofstream>& momDensProf) {
	int const Msize = hst.GetParams().Volume;
	auto& RPrime = WS::tmp2();
	auto& QPrime = WS::tmp1();

	hst.ComputeReducedDensityMatrix(RPrime);
	hst.ComposeDensityMatrixDiagonal(RPrime);

	hst.SaveToHDF();
	RNG::SaveState(hst.GetParams().hdfOutName);

	for (int spin = 0; spin < hst.NumberOfOBDMs(); ++spin) {
		hst.ConstructDensityMatrix(spin, QPrime, RPrime);

		for(int i = 0; i < Msize; i++){
			densProf[spin] << QPrime[i + Msize * i].real() << '\t';
		}
		densProf[spin] << '\n';

		QPrime.ResetSize();
		RPrime.ResetSize();
		QPrime.ExecuteFwd();
		RPrime.TransposeFrom(QPrime);
		std::copy(RPrime.cbegin(), RPrime.cend(), QPrime.begin());
		QPrime.ExecuteBwd();
		RPrime.TransposeFrom(QPrime);

		for (int ii = 0; ii < Msize; ++ii) {
			momDensProf[spin] << RPrime[ii * (Msize + 1)].real() / Msize << '\t';
		}
		momDensProf[spin] << '\n';
	}
}

template void SaveDensities(HSTransform<DiscreteBalanced>& hst,
		std::vector<std::ofstream>& densProf, std::vector<std::ofstream>& momDensProf);

template void SaveDensities(HSTransform<DiscretePolarised>& hst,
		std::vector<std::ofstream>& densProf, std::vector<std::ofstream>& momDensProf);

template <typename T>
void Metropolis(HSTransform<T>& hst, int const nSweeps, double const threshold){
	std::vector<std::ofstream> densProf;
	std::vector<std::ofstream> momDensProf;
	int const rank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();
	for (int i = 0; i < hst.NumberOfOBDMs(); ++i) {
		std::stringstream nameDens, nameMom;
		nameDens << rank << "_ProfileMetr_" << i << ".out";
		nameMom  << rank << "_ProfileMomMetr_" << i << ".out";
		densProf.emplace_back(std::ofstream(nameDens.str()));
		momDensProf.emplace_back(std::ofstream(nameMom.str()));
	}

	int const Msize = hst.GetParams().Volume;
	int const Decompositions = hst.GetParams().Nt / hst.GetParams().DecomposeEvery;
	int const DecomposeEvery = hst.GetParams().DecomposeEvery;

	auto& RPrime = WS::tmp2();
	auto& QPrime = WS::tmp1();

	//

	std::cout << "\n\n#####################\nTest Metropolis U\n\n";

	cdouble ProposalRatio = 0;
	int accepts = 0;
	int steps = 0;

	hst.Reset();
	for (int spin = 0; spin < hst.NumberOfOBDMs(); ++spin)
		WS::Storage(spin).Reset();
	std::vector<int> StoredTruncation(hst.NumberOfOBDMs());
	std::fill(StoredTruncation.begin(), StoredTruncation.end(), Msize);
	std::vector<int> TruncationTracker(hst.NumberOfOBDMs());
	std::fill(TruncationTracker.begin(), TruncationTracker.end(), Msize);

	std::cout << "Initial size RightEvolve: " << TruncationTracker[hst.NumberOfOBDMs()-1] << "\n";
	MeasureTimeX<1>([&]{
		for (int i = Decompositions - 1; i >= 0; --i){
			MeasureTimeX<2>([&]{
			MeasureTimeX<3>([&]{
				auto range = FakeIota{DecomposeEvery * i, DecomposeEvery * (i + 1)};
				hst.EvolveRightByTranspose(range.crbegin(), range.crend(), QPrime);
			},"Evolve Right");

			MeasureTimeX<3>([&]{
				hst.DecomposeEvolutionRight(TruncationTracker, threshold, RPrime, QPrime);
				// after this points: matrices within obdm know they have been truncated
			},"DecomposeEvolution Right");
			}, "Propagation + Stabiliisation Right");
		}
	}, "Full U Decompose Right");

	std::cout << "Final truncated size Right: " << TruncationTracker[hst.NumberOfOBDMs()-1] << "\n";

	hst.OnePlusRLD(RPrime);
	cdouble LogDet = hst.GetDeterminant(TruncationTracker);

	std::cout << std::setprecision(16);
	std::cout << "Log det (1 + U) Original = " << LogDet.real() << std::endl;

	cdouble OldLogDet = LogDet;



	//#####################
	//Start of Metropolis Loops 

	for(int sweep = 0; sweep < nSweeps; ++sweep){
	MeasureTimeX<1>([&]{
		for (int spin = 0; spin < hst.NumberOfOBDMs(); ++spin)
			WS::Storage(spin).Reset();
		std::fill(StoredTruncation.begin(), StoredTruncation.end(), Msize);

		//
		// global updates here
		//

		for (int block = 0; block < Decompositions; block++){
		//Loop over DecomposeEvery time slices
			for (int slice = block*DecomposeEvery; slice < (block + 1)*DecomposeEvery; slice++) {
				MeasureTimeX<2>([&]{
					hst.ComputeReducedDensityMatrix(RPrime);
				}, "Compute reduced density matrix");
				
				MeasureTimeX<2>([&]{
					hst.ComposeDensityMatrixDiagonal(RPrime);
				}, "Recompose Diagonal");

				hst.ProposeUpdate(ProposalRatio, slice);

				hst.ApplyUpdate();

				hst.OnePlusRLD(RPrime);

				LogDet = hst.GetDeterminant(TruncationTracker);

				if (exp(LogDet.real() - OldLogDet.real()) * ProposalRatio.real()
						* hst.GetHSWeight().real() > RNG::Uniform()) {
					hst.Accept(slice);
					++accepts;
					++steps;
					OldLogDet = LogDet;
				}
				else {
					hst.RevertUpdate();
					++steps;
				}

				hst.ReverseWrap(slice+1, slice, QPrime);
			}
			
			hst.CopyFromStorage();
			hst.Truncate(StoredTruncation);

			auto range = FakeIota{block * DecomposeEvery, (block + 1) * DecomposeEvery};
			hst.Evolve(range.cbegin(), range.cend());
			hst.DecomposeEvolution(StoredTruncation, threshold, RPrime, QPrime);

			hst.CopyToStorage();

			std::copy(StoredTruncation.cbegin(), StoredTruncation.cend(),
					TruncationTracker.begin());

			for (int block2 = Decompositions - 1; block2 > block; --block2){
				auto range = FakeIota{DecomposeEvery * block2, DecomposeEvery * (block2 + 1)};
				hst.EvolveRightByTranspose(range.crbegin(), range.crend(), QPrime);

				hst.DecomposeEvolutionRight(TruncationTracker, threshold, RPrime, QPrime);
			}
		}

		hst.OnePlusRLD(RPrime);

		LogDet = hst.GetDeterminant(TruncationTracker);

		std::cout << "#Sweep " << sweep <<  " Log det (1 + U) Proposal = "
			<< LogDet.real() << "\n";

		if (sweep > hst.GetParams().ThermalisationSweeps)
			if (sweep % hst.GetParams().SaveEvery == 0)
				SaveDensities(hst, densProf, momDensProf);

		//Test: Record the density after every sweep

	}, "Single Sweep");
	} // done with Metropolis sweeps 

	for (int i = 0; i < hst.NumberOfOBDMs(); ++i) {
		densProf[i].close();
		momDensProf[i].close();
	}

	std::cout << "Accept/Reject" <<
		static_cast<double>(accepts) / static_cast<double>(steps) << "\n";
	
	hst.Reset();
	std::fill(StoredTruncation.begin(), StoredTruncation.end(), Msize);
    std::fill(TruncationTracker.begin(), TruncationTracker.end(), Msize);

	std::cout << "Initial size LeftEvolve: " << TruncationTracker[hst.NumberOfOBDMs()-1] << "\n";
	MeasureTimeX<1>([&]{
		for (int i = 0; i < Decompositions; i++){
			MeasureTimeX<2>([&]{
			MeasureTimeX<3>([&]{
				auto range = FakeIota{DecomposeEvery * i, DecomposeEvery * (i + 1)};
				hst.Evolve(range.cbegin(), range.cend());
			},"Evolve Left");

			MeasureTimeX<3>([&]{
				hst.DecomposeEvolution(TruncationTracker, threshold, RPrime, QPrime);
			},"DecomposeEvolution Left");
			}, "Propagation + Stabiliisation Left");
		}
	}, "Full U Decompose Left");

	std::cout << "Final truncated size Left: " << TruncationTracker[hst.NumberOfOBDMs()-1] << "\n";

	//Compute determinant Prior to spin flipping

	hst.OnePlusRLD(RPrime);

	LogDet = hst.GetDeterminant(TruncationTracker);

	std::cout << "Log det (1 + U) New Phi = " << LogDet.real() << std::endl;
}

template
void Metropolis(HSTransform<DiscreteBalanced>& hst, int const nSweeps, double const threshold);
template
void Metropolis(HSTransform<DiscretePolarised>& hst, int const nSweeps, double const threshold);
