/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef MYVEC_H
#define MYVEC_H

#include <vector>
#include <complex>
#include <type_traits>

template<typename T>
struct is_complex_t : public std::false_type {};

template<typename T>
struct is_complex_t<std::complex<T>> : public std::true_type {};

template <typename T>
class myVec : public std::vector<T> {
public:
	using std::vector<T>::vector;

	template<typename T_ = T,
		std::enable_if_t<is_complex_t<T_>::value
			&& std::is_same<typename T_::value_type, double>::value >* = nullptr>
	inline __complex__ double* c_data()
	{ return reinterpret_cast<__complex__ double*>(this->data()); }
};

#endif
