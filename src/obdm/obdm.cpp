/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include <algorithm>

#include "obdm.h"
#include "workspace.h"
#include "matrix-diag.h"

template <typename U>
mini_OBDM<U>::mini_OBDM(Parameters const& params, int const smallerSideSize)
	: Q{MatrixFFT{params.Sizes, smallerSideSize}}
	, D{DiagonalMatrix<U>{std::min(params.Volume, smallerSideSize)}}
	, R{Matrix{smallerSideSize, params.Volume}}
{ }

template mini_OBDM<>::mini_OBDM(Parameters const& params, int const smallerSideSize);
template mini_OBDM<cdouble>::mini_OBDM(Parameters const& params, int const smallerSideSize);

template <typename U>
mini_OBDM<U>::mini_OBDM(Parameters const& params)
	: mini_OBDM(params, params.Volume)
{ }
template mini_OBDM<>::mini_OBDM(Parameters const& params);
template mini_OBDM<cdouble>::mini_OBDM(Parameters const& params);

template <typename U>
template <typename T>
void mini_OBDM<U>::CopyFrom(mini_OBDM<T> const& other) {
    if (other.Q.Cols() > Q.MaxCols())
        throw std::runtime_error("mini_OBDM::CopyFrom: other.Q.Cols() > Q.MaxCols()");
	std::copy(other.Q.cbegin(), other.Q.ctruncatedEnd(), Q.begin());
	std::copy(other.R.cbegin(), other.R.ctruncatedEnd(), R.begin());
	//std::copy(other.D.cbegin(), other.D.ctruncatedEnd(), D.begin());
    for (int d = 0; d < other.D.Cols(); ++d)
        D[d] = other.D[d];
}

template void mini_OBDM<>::CopyFrom(mini_OBDM<> const& other);
template void mini_OBDM<cdouble>::CopyFrom(mini_OBDM<double> const& other);

template <typename U>
template <typename T>
void mini_OBDM<U>::CopyFromAndTruncate(mini_OBDM<T> const& other) {
    if (other.Q.Cols() > Q.MaxCols())
        throw std::runtime_error("mini_OBDM::CopyFromAndTruncate: other.Q.Cols() > Q.MaxCols()");

    std::copy(other.Q.cbegin(), other.Q.ctruncatedEnd(), Q.begin());
	std::copy(other.R.cbegin(), other.R.ctruncatedEnd(), R.begin());
	//std::copy(other.D.cbegin(), other.D.ctruncatedEnd(), D.begin());
    for (int d = 0; d < other.D.Cols(); ++d)
        D[d] = other.D[d];

    this->Truncate(other.Q.Cols());
}

template void mini_OBDM<>::CopyFromAndTruncate(mini_OBDM<> const& other);
template void mini_OBDM<cdouble>::CopyFromAndTruncate(mini_OBDM<double> const& other);

//sets Q, R, D = Id
template <typename U>
void mini_OBDM<U>::Reset() {
	int const Msize = Q.MaxCols(); // = MaxRows()
	for (int j = 0; j < Msize; j++) {
		D[j] = 1;
		for (int i = 0; i < Msize; i++){
			if(i == j) {
				Q[i+Msize*i] = 1;
				R[i+Msize*i] = 1;
			} else {
				Q[i+Msize*j] = 0;
				R[i+Msize*j] = 0;
			}
		}
	}
	Q.ResetSize();
	R.ResetSize();
	D.ResetSize();
}

template void mini_OBDM<>::Reset();
template void mini_OBDM<cdouble>::Reset();


template <typename U>
void mini_OBDM<U>::Truncate(int const truncSize) {
	int const Msize = Q.MaxCols(); // = MaxRows()
	Q.Truncate(Msize, truncSize);
	R.Truncate(truncSize, Msize);
	D.Truncate(truncSize);
}

template void mini_OBDM<>::Truncate(int const truncSize);
template void mini_OBDM<cdouble>::Truncate(int const truncSize);


OBDM::OBDM(Parameters const& params, HSfield& _hsfield)
	: mini_OBDM{params}
	, hsfield{_hsfield}
{ }


//Computes (1 + RLD), only returns the diagonal of this for now.
//The RLD has scales in colums and should be cheap to pivot
//Needed for calculating the determinant.
//Tested for Free Hubbard at half filling
template <typename T>
void OBDM::OnePlusRLD(DiagonalMatrix<double>& Dout, CMatrixBase<T>& Work) const {
	
	//R*L
	int const Size = R.MaxRows();		// = MaxCols(), same for all matrices
	int const TruncSize = R.Rows();	// = Qin.Cols()
	Work.Truncate(D.Rows(), D.Cols());

	LinAlg(zgemm)(&ta, &tb, &TruncSize, &TruncSize, &Size,
			&alpha, R.c_data(), &TruncSize,
			Q.c_data(), &Size,
			&beta, Work.c_data(), &TruncSize);
	
	//(RL)*D
	RightMultiplyByDiagonal(Work, D);

	//1+DRL should be save like this, scales are in cols and there are no values below
	//the trunc size either
	for (int i = 0; i < TruncSize; ++i){
		Work[i+TruncSize*i] += 1.0; 
	}

	//Perform QR
	WS::PerformTruncatedQR(Work);

	for (int i = 0; i < TruncSize; ++i){
		Dout[i] = Work[i+TruncSize*i].real();
	}
	Work.ResetSize();
}

template void OBDM::OnePlusRLD(DiagonalMatrix<double>& Dout, CMatrixBase<Matrix>& Work) const;
template void OBDM::OnePlusRLD(DiagonalMatrix<double>& Dout, CMatrixBase<MatrixFFT>& Work) const;


//Computes sign(1+DLR) and returns the log of the absolute value of the determinant
template <typename T>
cdouble OBDM::SignedOnePlusRLD(mini_OBDM& out, CMatrixBase<T>& Work) const {

	//R*L
	int const Size = R.MaxRows();   // = MaxCols(), same for all matrices
	int const TruncSize = R.Rows();	// = Qin.Cols()
	Work.Truncate(D.Rows(), D.Cols());

	LinAlg(zgemm)(&ta, &tb, &TruncSize, &TruncSize, &Size,
			&alpha, R.c_data(), &TruncSize,
			Q.c_data(), &Size,
			&beta, Work.c_data(), &TruncSize);
	
	//(RL)*D
	RightMultiplyByDiagonal(Work, D);

	//1+DRL should be save like this, scales are in cols and there are no values below
	//the trunc size either
	for (int i = 0; i < TruncSize; ++i){
		Work[i+TruncSize*i] += 1.0; 
	}

	//Perform QR
	WS::PerformTruncatedQR(Work);

    out.R.Truncate(D.Rows(), D.Rows());
    out.D.Truncate(D.Rows());
    out.Q.Truncate(D.Rows(), D.Rows());

    WS::SeparateTruncatedQR(Work, out.D, out.Q);
    WS::RecoverTruncatedQ(Work);
    WS::PermuteColumns(out.R, out.Q);

    WS::RecoverTruncatedQ(Work);
    std::copy(Work.cbegin(), Work.ctruncatedEnd(), out.Q.begin());


    cdouble DummySign = 1.0;

	for (int i = 0; i < TruncSize; ++i){
        DummySign *= out.D[i] / std::abs(out.D[i]);
	}

    //std::cout << "Print within SignedOnePlusRLD DummySign" << "\n";
    //std::cout << DummySign << "\n";


    //Multipy R*Q and perform a LU, giving their det sign, check if this is indeed unit size!
    LinAlg(zgemm)(&ta, &tb, &TruncSize, &TruncSize, &TruncSize,
            &alpha, out.R.c_data(), &TruncSize,
            out.Q.c_data(), &TruncSize,
            &beta, Work.c_data(), &TruncSize);

    //perform LU
    int xxxx = 97;
	std::fill(WS::pivots().begin(), WS::pivots().end(), 0);
	LinAlg(zgetrf)(&TruncSize, &TruncSize, Work.c_data(), &TruncSize, WS::pivots().data(), &xxxx);

    //compute sign
    for (int i = 0; i < TruncSize; ++i){
        DummySign *= Work[i+TruncSize*i] / std::abs(Work[i+TruncSize*i]);
    }
    
    //sign from pivots
    for (int i = 0; i < TruncSize; ++i){
        if (WS::pivots()[i] != i+1)
            DummySign *= -1.0;
    }
        
	Work.ResetSize();
    out.Reset();

    std::cout << "Dummy Sign in SignedOnePlusRLD" << DummySign << "\n";


    return DummySign;
}

template cdouble OBDM::SignedOnePlusRLD(mini_OBDM& out, CMatrixBase<Matrix>& Work) const;
template cdouble OBDM::SignedOnePlusRLD(mini_OBDM& out, CMatrixBase<MatrixFFT>& Work) const;
