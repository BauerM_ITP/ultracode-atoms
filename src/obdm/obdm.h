/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef OBDM_H
#define OBDM_H

#include <algorithm>

#include "params.h"
#include "matrix.h"
#include "HSfield.h"
#include "fakeIter.h"
#include "matrix_fft.h"
#include "matrix_base.h"
#include "matrix_diag.h"
#include "linalg-libs.h"


template <typename U = double>
struct mini_OBDM {
	MatrixFFT Q;
	DiagonalMatrix<U> D;
	Matrix R;

	mini_OBDM(Parameters const& params);
	mini_OBDM(Parameters const& params, int const smallerSideSize);

	template <typename T>
	void RecomposeDiagonal(std::vector<cdouble>& Dout, CMatrixBase<T>& Work) const;
    
    //overload RecomposeDiagonal to work with an external Diagonal
    template <typename T>
    void RecomposeDiagonal(std::vector<cdouble>& Dout, std::vector<cdouble>& Din , CMatrixBase<T>& Work) const;

    template <typename T1, typename T2>
	void ReconstructU(CMatrixBase<T1>& Uout, CMatrixBase<T2>& Work) const;

    template <typename T>
	void CopyFrom(mini_OBDM<T> const& other);

    template <typename T>
    void CopyFromAndTruncate(mini_OBDM<T> const& other);

	void Reset();

	void Truncate(int const truncSize);
};

struct OBDM : public mini_OBDM<> {
	HSfield& hsfield;

	OBDM(Parameters const& params, HSfield& _hsfield);

	template <typename dir>
	void Evolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, double const mu);
	template <typename dir>
	void Devolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, double const mu);

	template <typename dir>
	void EvolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work, double const mu);
	template <typename dir>
	void DevolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work, double const mu);
	void ReverseWrap(int const tmax, int const t0, MatrixFFT& Work, double const mu);
	void Wrap(int const tmax, int const t0, MatrixFFT& Work, double const mu);

	template <typename T1, typename T2>
	int DecomposeEvolution(double tolerance, CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2);
	template <typename T1, typename T2>
	int DecomposeEvolutionRight(double tolerance, CMatrixBase<T1>& Work,CMatrixBase<T2>& Work2);
    template <typename T1, typename T2>
    int DecomposeProduct(double tolerance, mini_OBDM<>& StoredDecomp ,CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2);


	template <typename T>
	void ComputeReducedDensityMatrix(mini_OBDM& out, CMatrixBase<T>& Work) const;

    template <typename T>
    void PartialReducedDensityMatrix(mini_OBDM& out, CMatrixBase<T>& Work, const int timeslice);

    template <typename T>
	void OnePlusRLD(DiagonalMatrix<double>& Dout, CMatrixBase<T>&  Work) const;

    template <typename T>
    cdouble SignedOnePlusRLD(mini_OBDM& out, CMatrixBase<T>& Work) const;


    template <typename T>
    void PerformEigenDRL(mini_OBDM<cdouble>& out, CMatrixBase<T>& Work);

    template <typename T1, typename T2>
    void RecoverEigenVectors(mini_OBDM<cdouble>& EigenDecomp, CMatrixBase<T1>& Work1, CMatrixBase<T2>& Work2);

};


#endif
