/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "obdm.h"
#include "workspace.h"
#include "matrix-diag.h"


template <typename T>
void PrintColMatrix(int Rows, int Cols, T* Matrix, int TruncatePrint = 8){
	int PrintRows = (Rows <= TruncatePrint? Rows : TruncatePrint);
	int PrintCols = (Cols <= TruncatePrint? Cols : TruncatePrint);

	std::cout << "prints are truncated at " << TruncatePrint << " Rows/Cols\n";
	for (int i = 0; i < PrintRows; i++){
		for (int j = 0; j < PrintCols; j++){
			std::cout << Matrix[i+j*Rows] << "\t";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}

//Decomposes an evolved Q, includes reconstruction of the new Q, D and R,
//Truncates with a new truncation Size, set by tolerance, overwrites old Truncation size.
//TruncSize Does not go below 2.
template <typename T1, typename T2>
int OBDM::DecomposeEvolution(double tolerance, CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2) {
	
	int const Size = Q.Rows();
	int const TruncSize = Q.Cols();
	int const Msize = Q.MaxCols();

	//Q = Q*D
	MeasureTimeX<3>([&]{
		RightMultiplyByDiagonal(Q, D);
	}, "RightMultiplyByDiagonal");

	// separates Q*D into Q, D, Work2 
	MeasureTimeX<3>([&]{
		// Q has dimensions Msize x TruncSize
		MeasureTimeX<4>([&]{WS::PerformTruncatedQR(Q);},"QR");
		// D, Work, Work2 have dimensions TruncSize x TruncSize
		Work.Truncate(D.Rows(), D.Cols());
		Work2.Truncate(D.Rows(), D.Cols());
		WS::SeparateTruncatedQR(Q, D, Work);
		WS::PermuteColumns(Work2, Work);
		WS::RecoverTruncatedQ(Q);
	}, "QR, separate, permute, get Q");

	//Compute Work = Work2 * R
	MeasureTimeX<3>([&]{
		LinAlg(zgemm)(&ta, &tb, &TruncSize, &Msize, &TruncSize, 
				&alpha, Work2.c_data(), &TruncSize,
				R.c_data(), &TruncSize,
				&beta, Work.c_data(), &TruncSize);
	}, "zgemm_, R'* R ");

	int const ProposedNewTrunc = std::count_if(D.cbegin(), D.ctruncatedEnd(),
			[tolerance](double const d) { return std::abs(d) > tolerance; });

    int const NewTrunc = std::max(ProposedNewTrunc, 3);

	Work.Truncate(TruncSize, Size);
	int out = TruncSize;
	if(NewTrunc != TruncSize && NewTrunc > 2){
		R.Truncate(NewTrunc, Msize);
		Q.Truncate(Msize, NewTrunc);
		D.Truncate(NewTrunc);

		R.TruncateRows(Work);
		out = NewTrunc;
		//std::cout << "New Truncated Size: " << NewTrunc << "\n";
	}
	else
		std::copy(Work.cbegin(), Work.ctruncatedEnd(), R.begin());

	Work.ResetSize();
	Work2.ResetSize();

	return out;
}

template int OBDM::DecomposeEvolution(double tolerance,
		CMatrixBase<Matrix>& Work, CMatrixBase<Matrix>& Work2);
template int OBDM::DecomposeEvolution(double tolerance,
		CMatrixBase<MatrixFFT>& Work, CMatrixBase<Matrix>& Work2);
template int OBDM::DecomposeEvolution(double tolerance,
		CMatrixBase<Matrix>& Work, CMatrixBase<MatrixFFT>& Work2);
template int OBDM::DecomposeEvolution(double tolerance,
		CMatrixBase<MatrixFFT>& Work, CMatrixBase<MatrixFFT>& Work2);

//Decomposes the object D*R and goes back into L*D*R form
template <typename T1, typename T2>
int OBDM::DecomposeEvolutionRight(double tolerance, CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2) {

	int const Size = Q.Rows();
	int const TruncSize = Q.Cols();
	int const Msize = Q.MaxCols();

	//Do Work = R^T
	Work.Truncate(R.Cols(), R.Rows());
	Work.TransposeFrom(R);

	//Work = Work*D
	MeasureTimeX<3>([&]{
		RightMultiplyByDiagonal(Work, D);
	}, "RightMultiplyByDiagonal");

	//Performs QR and returns (R*D)^T = Work * d * Work2
	MeasureTimeX<3>([&]{
		// Work has dimensions Msize x TruncSize
		MeasureTimeX<4>([&]{WS::PerformTruncatedQR(Work);},"QR");
		// D, R, Work2 has dimensions TruncSize x TruncSize
		R.Truncate(D.Rows(), D.Cols());
		Work2.Truncate(D.Rows(), D.Cols());
		WS::SeparateTruncatedQR(Work, D, R);
		WS::PermuteColumns(Work2, R);
		WS::RecoverTruncatedQ(Work);
	}, "QR, separate, permute, get Q");

	// restore R to original shape
	R.Truncate(Q.Cols(), Q.Rows());
	//do R = Q * Work2^T
	MeasureTimeX<3>([&]{
		LinAlg(zgemm)(&ta, &tbT, &Size, &TruncSize, &TruncSize, 
				&alpha, Q.c_data(), &Size,
				Work2.c_data(), &TruncSize,
				&beta, R.c_data(), &Size);
	}, "zgemm_, Q * Work2^T ");

	//Copy Q <- R
	std::copy(R.cbegin(), R.ctruncatedEnd(), Q.begin());

	//Set the new truncation threshold and R <- Work^T
	int const NewTrunc = std::count_if(D.cbegin(), D.ctruncatedEnd(),
			[tolerance](double const d) { return std::abs(d) > tolerance; });

	int out = TruncSize;
	if(NewTrunc != TruncSize && NewTrunc > 2){
		//std::cout << "New Truncated Size: " << NewTrunc << "\n";
		R.Truncate(NewTrunc, Msize);
		Q.Truncate(Msize, NewTrunc);
		D.Truncate(NewTrunc);

		out = NewTrunc;
	}

	R.TransposeFrom(Work);
	Work.ResetSize();
	Work2.ResetSize();

	return out;
}

template int OBDM::DecomposeEvolutionRight(double tolerance,
		CMatrixBase<Matrix>& Work, CMatrixBase<Matrix>& Work2);
template int OBDM::DecomposeEvolutionRight(double tolerance,
		CMatrixBase<MatrixFFT>& Work, CMatrixBase<Matrix>& Work2);
template int OBDM::DecomposeEvolutionRight(double tolerance,
		CMatrixBase<Matrix>& Work, CMatrixBase<MatrixFFT>& Work2);
template int OBDM::DecomposeEvolutionRight(double tolerance,
		CMatrixBase<MatrixFFT>& Work, CMatrixBase<MatrixFFT>& Work2);

//Multiplies two decompositions (QDR)(Q_sD_sR_s)
//and decomposes the product, preserves stored data.
template <typename T1, typename T2>
int OBDM::DecomposeProduct(double tolerance, mini_OBDM<>& StoredDecomp ,CMatrixBase<T1>& Work, CMatrixBase<T2>& Work2){
    auto& StoredR = StoredDecomp.R;
    auto& StoredD = StoredDecomp.D;
    auto& StoredQ = StoredDecomp.Q;

    int const StoredSize = StoredQ.Rows();
    int const StoredTruncSize = StoredQ.Cols();
    int const Size = Q.Rows();
    int const TruncSize = Q.Cols();

    int const MinTrunc = std::min(TruncSize, StoredTruncSize);

    //PrintColMatrix(StoredTruncSize, Size, R.data(), 4);

    // Work = R*Q_s, Work is thus  
    zgemm_(&ta, &tb, &TruncSize, &StoredTruncSize, &Size, 
            &alpha, R.c_data(), &TruncSize,
            StoredQ.c_data(), &Size,
            &beta, Work.c_data(), &TruncSize);

    Work.Truncate(TruncSize, StoredTruncSize);


    //Work = D*Work*D_s
    LeftMultiplyByDiagonal(Work, D);
    RightMultiplyByDiagonal(Work, StoredD);

    //Work = Work * D * Work2
    WS::PerformTruncatedQR(Work);
    R.Truncate(MinTrunc, StoredTruncSize);
    Work2.Truncate(MinTrunc, StoredTruncSize);
    D.Truncate(MinTrunc);
    WS::SeparateTruncatedQR(Work, D, R);
    WS::PermuteColumns(Work2, R);
    Work.Truncate(TruncSize, MinTrunc);
    WS::RecoverTruncatedQ(Work);

    //R = Q * Work
    zgemm_(&ta, &tb, &Size, &MinTrunc, &TruncSize, 
            &alpha, Q.c_data(), &Size,
            Work.c_data(), &TruncSize,
            &beta, R.c_data(), &Size);
    
    R.Truncate(Size, MinTrunc);
    Q.Truncate(Size, MinTrunc);
    std::copy(R.cbegin(), R.ctruncatedEnd(), Q.begin());
    R.Truncate(MinTrunc, Size);

    //Work = Work2 * R_s
    zgemm_(&ta, &tb, &MinTrunc, &Size, &StoredTruncSize, 
        &alpha, Work2.c_data(), &MinTrunc,
        StoredR.c_data(), &StoredTruncSize,
        &beta, Work.c_data(), &MinTrunc);

    Work.Truncate(MinTrunc, Size);

	int const NewTrunc = std::count_if(D.cbegin(), D.ctruncatedEnd(),
			[tolerance](double const d) { return std::abs(d) > tolerance; });

	int out = TruncSize;

    //check if is smaller than both UP and Down

	if(NewTrunc != MinTrunc && NewTrunc > 2){
		R.Truncate(NewTrunc, Size);
		Q.Truncate(Size, NewTrunc);
		D.Truncate(NewTrunc);

		R.TruncateRows(Work);
		out = NewTrunc;
	}
	else{
		std::copy(Work.cbegin(), Work.ctruncatedEnd(), R.begin());
    }

	Work.ResetSize();
	Work2.ResetSize();

	return out;
}

template int OBDM::DecomposeProduct(double tolerance, mini_OBDM<>& StoredDecomp ,CMatrixBase<Matrix>& Work, CMatrixBase<Matrix>& Work2);
template int OBDM::DecomposeProduct(double tolerance, mini_OBDM<>& StoredDecomp ,CMatrixBase<Matrix>& Work, CMatrixBase<MatrixFFT>& Work2);
template int OBDM::DecomposeProduct(double tolerance, mini_OBDM<>& StoredDecomp ,CMatrixBase<MatrixFFT>& Work, CMatrixBase<Matrix>& Work2);
template int OBDM::DecomposeProduct(double tolerance, mini_OBDM<>& StoredDecomp ,CMatrixBase<MatrixFFT>& Work, CMatrixBase<MatrixFFT>& Work2);
