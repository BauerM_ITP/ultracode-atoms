/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "obdm.h"
#include "workspace.h"
#include "matrix-diag.h"

//UNTESTED
//On entry LeftEv, RightEv are the results of ComputeEigen, while Qin and Rin in are
//the matrices Q and R. 
//On exit, LeftEv = P_{Size*Truncsize}, RightEv = P^-1{Truncsize*Size},
//such that U = Q*D*R = P*Lambda*P^-1
//this function overwrites the pivits!
template <typename T1, typename T2>
void OBDM::RecoverEigenVectors(mini_OBDM<cdouble>& EigenDecomp, CMatrixBase<T1>& Work1, CMatrixBase<T2>& Work2) {

    int xxxx = 97;

    auto& LeftEv = EigenDecomp.Q;
    auto& RightEv = EigenDecomp.R;

    int const Size = R.Cols();
    int const TruncSize = R.Rows();

    Work1.Truncate(TruncSize,TruncSize);
    Work2.Truncate(TruncSize,TruncSize);

    //Multiply Qin * RightEv = LeftEv = P  
    LinAlg(zgemm)(&ta, &tb, &Size, &TruncSize, &TruncSize, &alpha,
            Q.c_data(), &Size,
            RightEv.c_data(), &TruncSize,
            &beta, LeftEv.c_data(), &Size);

    //compute Work1 = R*L
    LinAlg(zgemm)(&ta, &tb, &TruncSize, &TruncSize, &Size,
            &alpha, R.c_data(), &TruncSize,
            Q.c_data(), &Size,
            &beta, Work1.c_data(), &TruncSize);

    //Compute Work2 = Work1 * RightEv
    LinAlg(zgemm)(&ta, &tb, &TruncSize, &TruncSize, &TruncSize,
            &alpha, Work1.c_data(), &TruncSize,
            RightEv.c_data(), &TruncSize,
            &beta, Work2.c_data(), &TruncSize);

    //Compute LU inverse of Work2
    std::fill(WS::pivots().begin(), WS::pivots().end(), 0);
    LinAlg(zgetrf)(&TruncSize, &TruncSize,
            Work2.c_data(), &TruncSize,
            WS::pivots().data(), &xxxx);

    LinAlg(zgetri)(&TruncSize, Work2.c_data(), &TruncSize, 
            WS::pivots().data(), WS::work().c_data(),
			&WS::lwork(), &xxxx);

    //compute Work2 * R = RightEv = P^-1

    LinAlg(zgemm)(&ta, &tb, &TruncSize, &Size, &TruncSize, &alpha,
            Work2.c_data(), &TruncSize,
            R.c_data(), &TruncSize,
            &beta, RightEv.c_data(), &TruncSize);
    
    LeftEv.Truncate(Size, TruncSize);
    RightEv.Truncate(TruncSize, Size);
    
    Work1.ResetSize();
    Work2.ResetSize();

}

template void OBDM::RecoverEigenVectors(mini_OBDM<cdouble>& EigenDecomp, CMatrixBase<Matrix>& Work1, CMatrixBase<Matrix>& Work2);
template void OBDM::RecoverEigenVectors(mini_OBDM<cdouble>& EigenDecomp, CMatrixBase<MatrixFFT>& Work1, CMatrixBase<Matrix>& Work2);
template void OBDM::RecoverEigenVectors(mini_OBDM<cdouble>& EigenDecomp, CMatrixBase<Matrix>& Work1, CMatrixBase<MatrixFFT>& Work2);
template void OBDM::RecoverEigenVectors(mini_OBDM<cdouble>& EigenDecomp, CMatrixBase<MatrixFFT>& Work1, CMatrixBase<MatrixFFT>& Work2);


//UNTESTED
template<typename T>
void OBDM::PerformEigenDRL(mini_OBDM<cdouble>& out, CMatrixBase<T>& Work){
    int xxxx = 97;

    int const Size = R.MaxRows();		// = MaxCols(), same for all matrices
	int const TruncSize = R.Rows();	    // = Qin.Cols()

    out.Q.Truncate(Size, Size);
    out.R.Truncate(Size, Size);
    out.D.Truncate(Size);
    Work.Truncate(TruncSize,TruncSize);

    //Work=R*Q
    zgemm_(&ta, &tb, &TruncSize, &TruncSize, &Size,
            &alpha, R.c_data(), &TruncSize,
            Q.c_data(), &Size,
            &beta, Work.c_data(), &TruncSize);

    LeftMultiplyByDiagonal(Work, D);

    WS::ComputeEigen(Work, out.Q, out.R, out.D);


    //Enties beyond TruncSize in LambdaOut are set to zero

    for(int i = TruncSize; i < Size; i++){
        out.D[i] = 0.0;
    }

    Work.ResetSize();
};

template void OBDM::PerformEigenDRL(mini_OBDM<cdouble>& out, CMatrixBase<Matrix>& Work);
template void OBDM::PerformEigenDRL(mini_OBDM<cdouble>& out, CMatrixBase<MatrixFFT>& Work);
