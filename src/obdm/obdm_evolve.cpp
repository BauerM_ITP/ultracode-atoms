/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "obdm.h"
#include "matrix-diag.h"

//Evolves the matrix on which the FFTs defined, acting on the left
//Constructs B_{tmax-1} .... B_0 if reverse = false
//constructs B_0 .... B_{tmax-1} if reverse = true
//FINAL K IS ON THE LEFT 
template <typename dir>
void OBDM::Evolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, double const fugacity) {
	for (auto it = begin; it != end; ++it) {
		MeasureTimeX<3>([&]{
			hsfield.FillHsPotentialDiscrrete_BDM(*it);
			LeftMultiplyByDiagonal(Q, hsfield.Potential);
		}, "LeftMultiply Potential");
		MeasureTimeX<3>([&]{
			Q.ExecuteFwd();
		}, "Fwd FFT");
		MeasureTimeX<3>([&]{
			LeftMultiplyByDiagonal(Q, hsfield.EnergyValues, fugacity);
		}, "Left Multiply Kinetic");
		MeasureTimeX<3>([&]{
			Q.ExecuteBwd();
		}, "BWD FFT");
	}
}

template void OBDM::Evolve(FakeBaseIter<FakeFwdIter> begin, FakeBaseIter<FakeFwdIter> end, double const fugacity);
template void OBDM::Evolve(FakeBaseIter<FakeRevIter> begin, FakeBaseIter<FakeRevIter> end, double const fugacity);


template <typename dir>
void OBDM::EvolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work, double const fugacity) {
	Work.Truncate(R.Cols(), R.Rows());
	Work.TransposeFrom(R);

	for (auto it = begin; it != end; ++it) {
		MeasureTimeX<3>([&]{
			Work.ExecuteFwd();
		}, "Fwd FFT");
		MeasureTimeX<3>([&]{
			LeftMultiplyByDiagonal(Work, hsfield.EnergyValues, fugacity);
		}, "Left Multiply Kinetic");
		MeasureTimeX<3>([&]{
			Work.ExecuteBwd();
		}, "BWD FFT");

		MeasureTimeX<3>([&]{
			hsfield.FillHsPotentialDiscrrete_BDM(*it);
			LeftMultiplyByDiagonal(Work, hsfield.Potential);
		}, "LeftMultiply Potential");
	}

	R.TransposeFrom(Work);
	Work.ResetSize();
}

template void OBDM::EvolveRightByTranspose(FakeBaseIter<FakeFwdIter> begin, FakeBaseIter<FakeFwdIter> end, MatrixFFT& Work, double const fugacity);
template void OBDM::EvolveRightByTranspose(FakeBaseIter<FakeRevIter> begin, FakeBaseIter<FakeRevIter> end, MatrixFFT& Work, double const fugacity);


//undoes EvolveRightByTranspose
//Computes B^-1_0 .... B^-1_{tmax-1} if reverse = false
//Computes B^-1_{tmax-1} .... B^-1_0 if reverse = true
////FINAL K^-1 IS ON THE RIGHT 
template <typename dir>
void OBDM::DevolveRightByTranspose(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, MatrixFFT& Work, double const fugacity) {
	Work.Truncate(R.Cols(), R.Rows());

	Work.TransposeFrom(R);

	for (auto it = begin; it != end; ++it) {
			MeasureTimeX<3>([&]{
				hsfield.FillHsPotentialDiscrrete_BDM(*it);
				LeftMultiplyByInverseDiagonal(Work, hsfield.Potential);
			}, "LeftMultiply Potential");

			MeasureTimeX<3>([&]{
			Work.ExecuteFwd();
			}, "Fwd FFT");
			MeasureTimeX<3>([&]{
				LeftMultiplyByInverseDiagonalNormalize(Work, hsfield.EnergyValues, fugacity);
			}, "Left Multiply Kinetic");
			MeasureTimeX<3>([&]{
			Work.ExecuteBwd();
			}, "BWD FFT");
		}

	R.TransposeFrom(Work);
	Work.ResetSize();
}

template void OBDM::DevolveRightByTranspose(FakeBaseIter<FakeFwdIter> begin, FakeBaseIter<FakeFwdIter> end, MatrixFFT& Work, double const fugacity);
template void OBDM::DevolveRightByTranspose(FakeBaseIter<FakeRevIter> begin, FakeBaseIter<FakeRevIter> end, MatrixFFT& Work, double const fugacity);


//Wraps a decomposition from the left and right via (B_tmax-1 .... B_t0) LDR (Bt^-1_0 .... B^-1_{tmax-1})
void OBDM::ReverseWrap(int const tmax, int const t0, MatrixFFT& Work, double const fugacity) {
	auto range = FakeIota{t0, tmax};
	DevolveRightByTranspose(range.cbegin(), range.cend(), Work, fugacity);
	Evolve(range.cbegin(), range.cend(), fugacity);
}


//undoes Evolve Evolve(i,j)*Devolve(i,j) = Id (up to precision problems)
//Computes B^-1_0 .... B^-1_{tmax-1} if reverse = false
//Computes B^-1_{tmax-1} .... B^-1_0 if reverse = true
////FINAL K^-1 IS ON THE RIGHT 
template <typename dir>
void OBDM::Devolve(FakeBaseIter<dir> begin, FakeBaseIter<dir> end, double const fugacity) {
	// TODO: UNTESTED!!!!!
	for (auto it = begin; it != end; ++it) {
		MeasureTimeX<3>([&]{
			Q.ExecuteFwd();
		}, "Fwd FFT");
		MeasureTimeX<3>([&]{
			LeftMultiplyByInverseDiagonal(Q, hsfield.EnergyValues, fugacity);
		}, "LeftMultiplyInverse Kinetic");
		MeasureTimeX<3>([&]{
			Q.ExecuteBwd();
		}, "BWD FFT");
		MeasureTimeX<3>([&]{
			hsfield.FillHsPotentialDiscrrete_BDM(*it);
			LeftMultiplyByInverseDiagonalNormalize(Q, hsfield.Potential);
		}, "LeftMultiplyInverse Potential");
	}
}

//Wraps a decomposition from the left and right via (B_tmax^-1 .... B_0^-1) LDR (Bt_0 .... B_tmax)
//Plans must be defines on Q, PlansRight must be defined on Work
//NOTE: There appears to be an issue with the wrapping,
//if the trap length is a LOT smaller than the system size
//estimate: L_s/L_t no bigger than 40.
//Whenever wrapping is done, compare the relevant part to the recomputed result afterwards!
void OBDM::Wrap(int tmax, int t0, MatrixFFT& Work, double const fugacity) {
	
	auto range = FakeIota{t0, tmax};
	Devolve(range.cbegin(), range.cend(), fugacity);
	EvolveRightByTranspose(range.cbegin(), range.cend(), Work, fugacity);
}
