/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "obdm.h"
#include "workspace.h"
#include "matrix-diag.h"

//Constructs U from a decomposition. If decomposition is not needed anymore,
//the output matrix can be set to Rin 
template <typename U>
template <typename T1, typename T2>
void mini_OBDM<U>::ReconstructU(CMatrixBase<T1>& Uout, CMatrixBase<T2>& Work) const {

	int const Size = R.MaxRows();
	int const TruncSize = R.Rows();

	std::copy(R.cbegin(), R.ctruncatedEnd(), Work.begin());
	//Compute Work = D * R, D is TruncSize*TruncSize, R is TruncSize*Size
	Work.Truncate(TruncSize, Size);
	LeftMultiplyByDiagonal(Work, D);

	//Compute Uout = Qin * Work, Qin is Size*TruncSize, Work is TruncSize*Size
	LinAlg(zgemm)(&ta, &tb, &Size, &Size, &TruncSize,
			&alpha, Q.c_data(), &Size,
			Work.c_data(), &TruncSize,
			&beta, Uout.c_data(), &Size);

	Work.ResetSize();
}

//template <typename U>
template void mini_OBDM<double>::ReconstructU(CMatrixBase<Matrix>& Uout, CMatrixBase<Matrix>& Work) const;
template void mini_OBDM<double>::ReconstructU(CMatrixBase<MatrixFFT>& Uout, CMatrixBase<Matrix>& Work) const;
template void mini_OBDM<double>::ReconstructU(CMatrixBase<Matrix>& Uout, CMatrixBase<MatrixFFT>& Work) const;
template void mini_OBDM<double>::ReconstructU(CMatrixBase<MatrixFFT>& Uout, CMatrixBase<MatrixFFT>& Work) const;
template void mini_OBDM<cdouble>::ReconstructU(CMatrixBase<Matrix>& Uout, CMatrixBase<Matrix>& Work) const;
template void mini_OBDM<cdouble>::ReconstructU(CMatrixBase<MatrixFFT>& Uout, CMatrixBase<Matrix>& Work) const;
template void mini_OBDM<cdouble>::ReconstructU(CMatrixBase<Matrix>& Uout, CMatrixBase<MatrixFFT>& Work) const;
template void mini_OBDM<cdouble>::ReconstructU(CMatrixBase<MatrixFFT>& Uout, CMatrixBase<MatrixFFT>& Work) const;


//THIS MIGHT HAVE TO BE DONE VIA THE LOH METHOD
//DO MORE TESTING!
//Computes the truncated decomposition of L*(D^-1 + RL)^-1*R
template <typename T>
void OBDM::ComputeReducedDensityMatrix(mini_OBDM& out, CMatrixBase<T>& Work) const {

	auto& Rout = out.R;
	auto& Dout = out.D;
	auto& Qout = out.Q;

	int const Size = R.MaxRows();		// = MaxCols(), same for all matrices
	int const TruncSize = R.Rows();	// = Qin.Cols()
	//Qout R*L
	LinAlg(zgemm)(&ta, &tb, &TruncSize, &TruncSize, &Size,
			&alpha, R.c_data(), &TruncSize,
			Q.c_data(), &Size,
			&beta, Qout.c_data(), &TruncSize);

	// Qout = D^-1 + RL
	// These are at most of size 1 / Truncation threshold
	for (int i = 0; i < TruncSize; i++){
		Qout[i+TruncSize*i] += 1.0 / D[i];
	}

	//QR and separation of this Qout = q,d,r
	Qout.Truncate(TruncSize, TruncSize);
	WS::PerformTruncatedQR(Qout);
	// Dout, Rout, Work have dimensions TruncSize x TruncSize
	Rout.Truncate(D.Rows(), D.Cols());
	Work.Truncate(D.Rows(), D.Cols());
	WS::SeparateTruncatedQR(Qout, Dout, Rout);
	WS::PermuteColumns(Work, Rout);
	WS::RecoverTruncatedQ(Qout);

	//Invert Dout
	for (int i = 0; i < TruncSize; i++){
		Dout[i] = 1.0 / Dout[i];
	}

	//compute LU inverse of Qout = q
	//pivots are used, so they can be overwritten here
	int xxxx = 97;
	std::fill(WS::pivots().begin(), WS::pivots().end(), 0);
	LinAlg(zgetrf)(&TruncSize, &TruncSize, Qout.c_data(), &TruncSize, WS::pivots().data(), &xxxx);
	LinAlg(zgetri)(&TruncSize, Qout.c_data(), &TruncSize, WS::pivots().data(), WS::work().c_data(),
			&WS::lwork(), &xxxx);
	
	//Multiplies  Rout=q^-1*Rin
	LinAlg(zgemm)(&ta, &tb, &TruncSize, &Size, &TruncSize, &alpha, Qout.c_data(), &TruncSize, 
			R.c_data(), &TruncSize, &beta, Rout.c_data(), &TruncSize);

	//compute LU inverse of Work = r
	//pivots are used, so they can be overwritten here here
	std::fill(WS::pivots().begin(), WS::pivots().end(), 0);
	LinAlg(zgetrf)(&TruncSize, &TruncSize, Work.c_data(), &TruncSize, WS::pivots().data(), &xxxx);
	LinAlg(zgetri)(&TruncSize, Work.c_data(), &TruncSize, WS::pivots().data(), WS::work().c_data(),
			&WS::lwork(), &xxxx);

	//Multiplies  Qout=Qin * r^-1
	LinAlg(zgemm)(&ta, &tb, &Size, &TruncSize, &TruncSize, &alpha, Q.c_data(), &Size, Work.c_data(),
			&TruncSize, &beta, Qout.c_data(), &Size);

	Rout.Truncate(R.Rows(), R.Cols());
	Qout.Truncate(Q.Rows(), Q.Cols());
	Dout.Truncate(D.Rows());

	Work.ResetSize();
};

template void OBDM::ComputeReducedDensityMatrix(mini_OBDM& out, CMatrixBase<Matrix>& Work) const;
template void OBDM::ComputeReducedDensityMatrix(mini_OBDM& out, CMatrixBase<MatrixFFT>& Work) const;

template <typename T>
void OBDM::PartialReducedDensityMatrix(mini_OBDM& out, CMatrixBase<T>& Work, const int timeslice){

    auto& Rout = out.R;
	auto& Dout = out.D;
	auto& Qout = out.Q;

	int const Size = R.MaxRows();		// = MaxCols(), same for all matrices
	int const TruncSize = R.Rows();	// = Qin.Cols()

    //Right Multiply e^{-Hs part} on R, do not include trap

    hsfield.FillHsPotential_BDM(timeslice);

    RightMultiplyByInverseDiagonal(R.GetVector().data() , hsfield.Potential.data(), TruncSize, Size);

	//Qout R*L
	LinAlg(zgemm)(&ta, &tb, &TruncSize, &TruncSize, &Size,
			&alpha, R.c_data(), &TruncSize,
			Q.c_data(), &Size,
			&beta, Qout.c_data(), &TruncSize);

	// Qout = D^-1 + RL
	// These are at most of size 1 / Truncation threshold
	for (int i = 0; i < TruncSize; i++){
		Qout[i+TruncSize*i] += 1.0 / D[i];
	}

	//QR and separation of this Qout = q,d,r
	Qout.Truncate(TruncSize, TruncSize);
	WS::PerformTruncatedQR(Qout);
	// Dout, Rout, Work have dimensions TruncSize x TruncSize
	Rout.Truncate(D.Rows(), D.Cols());
	Work.Truncate(D.Rows(), D.Cols());
	WS::SeparateTruncatedQR(Qout, Dout, Rout);
	WS::PermuteColumns(Work, Rout);
	WS::RecoverTruncatedQ(Qout);

	//Invert Dout
	for (int i = 0; i < TruncSize; i++){
		Dout[i] = 1.0 / Dout[i];
	}

	//compute LU inverse of Qout = q
	//pivots are used, so they can be overwritten here here
	int xxxx = 97;
	std::fill(WS::pivots().begin(), WS::pivots().end(), 0);
	LinAlg(zgetrf)(&TruncSize, &TruncSize, Qout.c_data(), &TruncSize, WS::pivots().data(), &xxxx);
	LinAlg(zgetri)(&TruncSize, Qout.c_data(), &TruncSize, WS::pivots().data(), WS::work().c_data(),
			&WS::lwork(), &xxxx);
	
	//Multiplies  Rout=q^-1*Rin
	LinAlg(zgemm)(&ta, &tb, &TruncSize, &Size, &TruncSize, &alpha, Qout.c_data(), &TruncSize, 
			R.c_data(), &TruncSize, &beta, Rout.c_data(), &TruncSize);

	//compute LU inverse of Work = r
	//pivots are used, so they can be overwritten here here
	std::fill(WS::pivots().begin(), WS::pivots().end(), 0);
	LinAlg(zgetrf)(&TruncSize, &TruncSize, Work.c_data(), &TruncSize, WS::pivots().data(), &xxxx);
	LinAlg(zgetri)(&TruncSize, Work.c_data(), &TruncSize, WS::pivots().data(), WS::work().c_data(),
			&WS::lwork(), &xxxx);

	//Multiplies  Qout=Qin * r^-1
	LinAlg(zgemm)(&ta, &tb, &Size, &TruncSize, &TruncSize, &alpha, Q.c_data(), &Size, Work.c_data(),
			&TruncSize, &beta, Qout.c_data(), &Size);

	Rout.Truncate(R.Rows(), R.Cols());
	Qout.Truncate(Q.Rows(), Q.Cols());
	Dout.Truncate(D.Rows());

	Work.ResetSize();

    //Right Multiply on R again to have full U again on Output
    RightMultiplyByDiagonal(R.GetVector().data() , hsfield.Potential.data(), TruncSize, Size);

}

template void OBDM::PartialReducedDensityMatrix(mini_OBDM& out, CMatrixBase<Matrix>& Work, const int timeslice);
template void OBDM::PartialReducedDensityMatrix(mini_OBDM& out, CMatrixBase<MatrixFFT>& Work, const int timeslice);

template <typename U>
template <typename T>
void mini_OBDM<U>::RecomposeDiagonal(std::vector<cdouble>& Dout, CMatrixBase<T>& Work) const {

	int const Size = R.MaxRows();		// = MaxCols(), same for all matrices
	int const TruncSize = R.Rows();	// = Qin.Cols()
	std::copy(R.cbegin(), R.ctruncatedEnd(), Work.begin());
	Work.Truncate(TruncSize, Size);
	LeftMultiplyByDiagonal(Work, D);

	#pragma omp parallel for
	for (int i = 0; i < Size; i++){
		Dout[i] = 0.0;
		for (int j = 0; j < TruncSize; j++)
			Dout[i] += Q[Size*j + i]*Work[TruncSize*i + j];
	}
	Work.ResetSize();
}

template void mini_OBDM<double>::RecomposeDiagonal(std::vector<cdouble>& Dout, CMatrixBase<Matrix>& Work) const;
template void mini_OBDM<double>::RecomposeDiagonal(std::vector<cdouble>& Dout, CMatrixBase<MatrixFFT>& Work) const;
template void mini_OBDM<cdouble>::RecomposeDiagonal(std::vector<cdouble>& Dout, CMatrixBase<Matrix>& Work) const;
template void mini_OBDM<cdouble>::RecomposeDiagonal(std::vector<cdouble>& Dout, CMatrixBase<MatrixFFT>& Work) const;

template <typename U>
template <typename T>
void mini_OBDM<U>::RecomposeDiagonal(std::vector<cdouble>& Dout, std::vector<cdouble>& Din , CMatrixBase<T>& Work) const {

	int const Size = R.MaxRows();		// = MaxCols(), same for all matrices
	int const TruncSize = R.Rows();	// = Qin.Cols()
	std::copy(R.cbegin(), R.ctruncatedEnd(), Work.begin());
	Work.Truncate(TruncSize, Size);
	LeftMultiplyByDiagonal(Work.data(), Din.data(), TruncSize, Size);

	#pragma omp parallel for
	for (int i = 0; i < Size; i++){
		Dout[i] = 0.0;
		for (int j = 0; j < TruncSize; j++)
			Dout[i] += Q[Size*j + i]*Work[TruncSize*i + j];
	}
	Work.ResetSize();
}

template void mini_OBDM<double>::RecomposeDiagonal(std::vector<cdouble>& Dout, std::vector<cdouble>& Din, CMatrixBase<Matrix>& Work) const;
template void mini_OBDM<double>::RecomposeDiagonal(std::vector<cdouble>& Dout, std::vector<cdouble>& Din, CMatrixBase<MatrixFFT>& Work) const;
template void mini_OBDM<cdouble>::RecomposeDiagonal(std::vector<cdouble>& Dout, std::vector<cdouble>& Din, CMatrixBase<Matrix>& Work) const;
template void mini_OBDM<cdouble>::RecomposeDiagonal(std::vector<cdouble>& Dout, std::vector<cdouble>& Din, CMatrixBase<MatrixFFT>& Work) const;

