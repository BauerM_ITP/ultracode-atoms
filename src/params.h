/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef PARAM_H
#define PARAM_H

#include <complex>

using cdouble = std::complex<double>;

//int constexpr Msize = 160;

//int constexpr Nt = 160; //THIS HAS TO BE A MULTIPLE OF  DecomposeEvery FOR NOW!
//int constexpr DecomposeEvery = 16;
//int constexpr Decompositions = Nt / DecomposeEvery;
//double constexpr betamu = -0.0;
//double constexpr anisotropy = 0.05;
//double constexpr mu = betamu/(anisotropy*Nt);
//NOTE: There appears to be an issue with the wrapping,
//if the trap length is a LOT smaller than the system size
//estimate: L_s/L_t no bigger than 40.
//double constexpr omega = 0.000125;
//cdouble constexpr lambda = 0.9;
//double constexpr ExtPotentialCutoff = 20.0;

//cdouble constexpr U = -2.0;
//cdouble const gam = std::acosh(std::exp(-anisotropy*U/2.));


// constants
__complex__ double constexpr alpha = 1.0;
__complex__ double constexpr beta  = 0.0;

char constexpr ta  = 'N';
char constexpr tb  = 'N';
char constexpr tbT = 'T';

int constexpr MV = 1; //I dont know what this does
char constexpr JOBA = 'G';
char constexpr JOBU = 'N';
char constexpr JOBV = 'N';

char constexpr BALANC = 'B';
char constexpr EEIGENQR = 'E';
char constexpr JOBVL = 'V';
char constexpr JOBVR = 'V';
char constexpr SENSE = 'N';
char constexpr COMPZ = 'N';

#endif
