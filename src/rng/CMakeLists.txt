include_directories(..)

add_library(rng rng.cpp)

set_property(TARGET rng PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
