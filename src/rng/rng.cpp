/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include <mpi.h>
#include <array>
#include <random>
#include <sstream>

#include "rng.h"
#include "pcg_random.hpp"
#include "highfive/H5File.hpp"

namespace HF = HighFive;

namespace RNG {
	using state_type = pcg32::state_type;

	int nProcs, rank;
	pcg32 rng;
	std::normal_distribution<> dist(0, 1);
	std::uniform_real_distribution<> unidist(0,1);

	void Initialise(uint32_t const seed) {
		nProcs = []{int x; MPI_Comm_size(MPI_COMM_WORLD, &x); return x;}();
		rank   = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();

		rng = seed + rank;
	}

	double Uniform() { return unidist(rng); }
	double Normal()  { return dist(rng);	}

	void SaveState(std::string const hdfOutName) {
		std::stringstream sstate; sstate << rng;
		std::array<state_type, 3> out;
		sstate >> out[0]; sstate >> out[1]; sstate >> out[2];

		try {
			auto file = HF::File{hdfOutName, HF::File::ReadWrite};
			auto group = [&file]{
				if (file.exist("RNG") == true)
					return file.getGroup("RNG");
				else
					return file.createGroup("RNG");
			}();

			auto dataset = [&group, &out]{
				if (group.exist("LastState"))
					return group.getDataSet("LastState");
				else
					return group.createDataSet<state_type>("LastState", HF::DataSpace::From(out));
			}();
			dataset.write_raw(out.data());
		} catch (std::exception& e) {
			std::cerr << "# -- Error saving RNG state to HDF file " << hdfOutName
				<< ".\n # -- Error message: " << e.what() << '\n';
		}
	}

	void ReadState(std::string const hdfOutName) {
		try {
			auto file = HF::File{hdfOutName, HF::File::ReadOnly};
			if (file.exist("RNG") == true) {
				auto group = file.getGroup("RNG");
				if (group.exist("LastState") == true) {
					auto ds = group.getDataSet("LastState");
					std::array<state_type, 3> state;
					ds.read(state.data());
					std::stringstream ss;
					ss << state[0] << ' ' << state[1] << ' ' << state[2];
					ss >> rng;
				} else
					std::cerr << "Unable to find random number state in HDF file.\n";
			} else
				std::cerr << "Unable to find random number state in HDF file.\n";
		} catch (std::exception& e) {
			std::cerr << "# -- Error reading RNG state from HDF file " << hdfOutName
				<< ".\n # -- Error message: " << e.what() << '\n';
		}
	}
}	// end namespace RNG
