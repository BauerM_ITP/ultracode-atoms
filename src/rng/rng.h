/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef RNG_H
#define RNG_H

#include <string>
#include <cstdint>

namespace RNG {
	void Initialise(uint32_t const seed);
	double Uniform();
	double Normal();
	void SaveState(std::string const hdfOutName);
	void ReadState(std::string const hdfOutName);
}	// end namespace RNG

#endif
