/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "params.h"

#include <omp.h>
#include <vector>
#include <fstream>
#include <iomanip>
#include <complex>
#include <numeric>
#include <iostream>
#include <algorithm>

#include "rng.h"
#include "obdm.h"
#include "fakeIter.h"
#include "workspace.h"
#include "measureTime.h"
#include "matrix-diag.h"
#include "HStransforms.h"

//NOT IMPLEMENTED
//Stupid temper is supposed to try different seeds and evolve them for some steps.
//Whichever has the highest weight is picked as a starting point for the actual simulation.
void StupidTemper(){}

//Temper provides a form of simulated annealing, slowly raising the temperature 
//to find a good starting configuration. Might want to expand this to do several runs
//starting from different field configs.
template <typename T>
void Temper(HSTransform<T>& hst, double const threshold) {
	auto& hsfield = hst.GetHSfield();
//

	int const Msize = hst.GetParams().Volume;
	int const Decompositions = hst.GetParams().Nt / hst.GetParams().DecomposeEvery;
	int const DecomposeEvery = hst.GetParams().DecomposeEvery;

	auto& RPrime = WS::tmp2();
	auto& QPrime = WS::tmp1();

	cdouble ProposalRatio = 0;

	std::vector<cdouble> dens_up(Msize);	// OBDM?
	
	hsfield.SetRandom();
	
	for (auto& n : dens_up)
		n = 0;
	
	std::vector<int> StoredTruncation(hst.NumberOfOBDMs());
	std::fill(StoredTruncation.begin(), StoredTruncation.end(), Msize);
	std::vector<int> TruncationTracker(hst.NumberOfOBDMs());
	std::fill(TruncationTracker.begin(), TruncationTracker.end(), Msize);

	std::cout << "################################\n" 
	<<  "Starting tempering using \n"
	<< "TemperingSteps =\t" << hsfield.params.TemperingSteps << "\n"
	<< "minAnisotropy =\t"<< hsfield.params.minAnisotropy << "\n"
	<< "MaxAnisotropy =\t"<< hsfield.params.beta / hsfield.params.Nt << "\n\n";

	double CurrentAnisotropy = hsfield.params.minAnisotropy;

	//
	for(int tempIndex = 0; tempIndex < hsfield.params.TemperingSteps; tempIndex++){
		CurrentAnisotropy = hst.GetParams().minAnisotropy
						+ (double) tempIndex / (double) (hst.GetParams().TemperingSteps - 1) 
						* (hst.GetParams().beta / hst.GetParams().Nt - hst.GetParams().minAnisotropy);
		hst.SetAnisotropy(CurrentAnisotropy);

		hst.Reset();
		for (int spin = 0; spin < hst.NumberOfOBDMs(); ++spin)
			WS::Storage(spin).Reset();
		std::fill(StoredTruncation.begin(), StoredTruncation.end(), Msize);
//		obdm.Reset();
//		Storage.Reset();

		MeasureTimeX<1>([&]{
			for (int i = Decompositions - 1; i >= 0; --i){
				MeasureTimeX<2>([&]{
				MeasureTimeX<3>([&]{
					auto range = FakeIota{DecomposeEvery * i, DecomposeEvery * (i + 1)};
					hst.EvolveRightByTranspose(range.crbegin(), range.crend(), QPrime);
				},"Evolve Right");

				MeasureTimeX<3>([&]{
					hst.DecomposeEvolutionRight(TruncationTracker, threshold, RPrime, QPrime);
					// after this points: matrices within obdm know they have been truncated
				},"DecomposeEvolution Right");
				}, "Propagation + Stabiliisation Right");
			}
		}, "Full U Decompose Right");
		std::cout << "Current truncation: " << TruncationTracker[0] << "\n";


		hst.OnePlusRLD(RPrime);
		cdouble LogDet = hst.GetDeterminant(TruncationTracker);
		std::cout << std::setprecision(16);

		cdouble OldLogDet = LogDet;


		for(int sweep = 0; sweep < hsfield.params.SweepsPerTemperature; ++sweep){
            std::cout << "Sweep " << sweep << "\n";
			for (int spin = 0; spin < hst.NumberOfOBDMs(); ++spin)
				WS::Storage(spin).Reset();
			std::fill(StoredTruncation.begin(), StoredTruncation.end(), Msize);

			//TruncationTracker = Msize; //REMOVE
			for (int block = 0; block < Decompositions; block++){
			//Loop over DecomposeEvery time slices
				for (int slice = block*DecomposeEvery; slice < (block + 1)*DecomposeEvery; slice++) {
					//Compute Density for force bias

					MeasureTimeX<2>([&]{
						hst.ComputeReducedDensityMatrix(RPrime);
					}, "Compute reduced density matrix");
					
					MeasureTimeX<2>([&]{
						hst.ComposeDensityMatrixDiagonal(RPrime);
					}, "Recompose Diagonal");

					hst.ProposeUpdate(ProposalRatio, slice);

					hst.ApplyUpdate();

					hst.OnePlusRLD(RPrime);

					LogDet = hst.GetDeterminant(TruncationTracker);

					if (exp(LogDet.real() - OldLogDet.real()) * ProposalRatio.real() *
							hst.GetHSWeight().real() > RNG::Uniform()){
						hsfield.Accept(slice);
						OldLogDet = LogDet;
					}
					else{
						hst.RevertUpdate();
					}

					hst.ReverseWrap(slice+1, slice, QPrime);
				}
				
				hst.CopyFromStorage();
				hst.Truncate(StoredTruncation);

				//PrintColMatrix(Msize, StoredTruncation, Q.data());

				auto range = FakeIota{block * DecomposeEvery, (block + 1) * DecomposeEvery};
				hst.Evolve(range.cbegin(), range.cend());
				hst.DecomposeEvolution(StoredTruncation, threshold, RPrime, QPrime);

				hst.CopyToStorage();

				std::copy(StoredTruncation.cbegin(), StoredTruncation.cend(),
						TruncationTracker.begin());
				for (int block2 = Decompositions - 1; block2 > block; block2--){
					auto range = FakeIota{DecomposeEvery * block2, DecomposeEvery * (block2 + 1)};
					hst.EvolveRightByTranspose(range.crbegin(), range.crend(), QPrime);

					hst.DecomposeEvolutionRight(TruncationTracker, threshold, RPrime, QPrime);
				}
			}

			hst.OnePlusRLD(RPrime);

			LogDet = hst.GetDeterminant(TruncationTracker);
		}

		hst.ComputeReducedDensityMatrix(RPrime);
		hst.ComposeDensityMatrixDiagonal(RPrime);

		double const NupParticles = std::accumulate(hst.DensUp().cbegin(), hst.DensUp().cend(),
				0.0, [](double acc, cdouble val){ return acc + val.real(); });
		double const NdnParticles = std::accumulate(hst.DensDn().cbegin(), hst.DensDn().cend(),
				0.0, [](double acc, cdouble val){ return acc + val.real(); });
		std::cout << "Final particle number step " << tempIndex
			<< " -- spin up: " << NupParticles
			<< " -- spin dn: " << NdnParticles << "\n\n";


	}
}

template void Temper(HSTransform<DiscreteBalanced>& hst, double const threshold);
template void Temper(HSTransform<DiscretePolarised>& hst, double const threshold);
