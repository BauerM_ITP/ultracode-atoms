/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "params.h"

#include <iostream>
#include <iomanip>
#include <omp.h>
#include <vector>
#include <complex>

#include "rng.h"
#include "obdm.h"
#include "toml-io.h"
#include "workspace.h"
#include "measureTime.h"
#include "matrix-diag.h"
#include "analysis.h"

int observables = 50;
int num_samples = 100000;
std::vector<cdouble> data(observables);
std::vector<cdouble> dataOld(observables);
statistics stats(observables);
autocorrelation ac(num_samples, observables);

void FillRandom(std::vector<cdouble>& data, std::vector<cdouble>& dataOld){
    for (int i = 0; i < data.size(); i++){
        data[i] = 0.5*RNG::Normal() + dataOld[i] * 0.9;
        dataOld[i] = data[i];
    }
}

int main(){
    RNG::Initialise(1);

    FillRandom(data, dataOld);

    for (int i = 0; i < num_samples; i++){
        FillRandom(data, dataOld);
        stats.AddData(data);
        ac.AddData(i, data);
    }

    ac.ComputeAutocorrelation();
    stats.Normalize();

    stats.OutputHDF5("test.hdf5", "Gaussians");
    ac.OutputHDF5("test.hdf5", "Gaussians");

    return 0;
}
