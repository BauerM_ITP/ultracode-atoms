/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "params.h"

#include <iostream>
#include <iomanip>
#include <omp.h>
#include <vector>
#include <complex>
#include "mpi.h"

#include "rng.h"
#include "obdm.h"
#include "toml-io.h"
#include "workspace.h"
#include "measureTime.h"
#include "matrix-diag.h"

template <typename T>
void PrintColMatrix(int Rows, int Cols, T* Matrix, int TruncatePrint = 8){
	int PrintRows = (Rows <= TruncatePrint? Rows : TruncatePrint);
	int PrintCols = (Cols <= TruncatePrint? Cols : TruncatePrint);

	std::cout << "prints are truncated at " << TruncatePrint << " Rows/Cols\n";
	for (int i = 0; i < PrintRows; i++){
		for (int j = 0; j < PrintCols; j++){
			std::cout << Matrix[i+j*Rows] << "\t";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}


void Run1(Parameters const& params){
    Matrix UMat(params.Volume);
    auto hsfield = HSfield{params};
	OBDM obdm(params, hsfield);
    mini_OBDM<> obdmStorage(params);

    double threshold = 10e-300;
    int maxPrint = (params.Volume < 10 ? params.Volume : 10);

    hsfield.SetRandom();

    obdm.Reset();


    cdouble InitialSign = obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1());

    std::cout << "InitialSign: " << InitialSign << "\n\n";

    std::cout << "Flipping single diagonal element\n";

    //do not flip to -1 here!, that leads to det = 0!
    obdm.D[0] = -0.9;

    cdouble SecondSign = obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1());

    std::cout << "SecondSign: " << SecondSign << "\n\n";

    obdm.Reset();

    std::cout << "Flipping single diagonal element of Q\n";

    obdm.Q[0] = -1.05;

    cdouble ThirdSign = obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1());

    std::cout << "ThirdSign: " << ThirdSign << "\n\n";


    //#############################
    //Testing a more complicated matrix

    obdm.Reset();

    //Evolving the matrix
    for (int i = 0; i < params.Nt; i++){
        auto range2 = FakeIota{i, i+1};
        obdm.Evolve(range2.cbegin(), range2.cend(), hsfield.params.muFactorUp);
        obdm.DecomposeEvolution(threshold, WS::tmp1(), WS::tmp2());
    }

    cdouble FourthSign = obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1());

    std::cout << "Constructing U with random fields\n";

    std::cout << "FourthSign: " << FourthSign << "\n\n";

    obdm.Reset();

    //Randomize Q

    //Evolving the matrix
    for (int i = 0; i < params.Nt; i++){
        auto range2 = FakeIota{i, i+1};
        obdm.Evolve(range2.cbegin(), range2.cend(), hsfield.params.muFactorUp);
        obdm.DecomposeEvolution(1e-3, WS::tmp1(), WS::tmp2());
    }

    std::cout << "Constructing U with trunc 1e-3\n";

    std::cout << "FourthSign: " << FourthSign << "\n\n";


    obdm.Reset();

    RNG::Initialise(7);

    for (int i = 0; i < obdm.Q.Cols(); i++){
        for (int j = 0; j < obdm.Q.Rows(); j++){
            obdm.Q[j + i*obdm.Q.Rows()] = (RNG::Uniform() - 0.5) * 1 + (RNG::Uniform() - 0.5)*cdouble(0,1);
        }
    }

    Matrix Store(obdm.Q.Rows(), obdm.Q.Cols());
    std::vector<double> StoreD(obdm.Q.Cols());

    std::copy(obdm.Q.cbegin(), obdm.Q.cend(), Store.begin());

    for (auto& d: obdm.D)
        d = pow(RNG::Uniform() , (RNG::Uniform() - 0.2) * 10.0); 

    std::copy(obdm.D.cbegin(), obdm.D.cend(), StoreD.begin());


    obdm.DecomposeEvolution(1e-20, WS::tmp1(), WS::tmp2());

        std::cout << "Obdm D size:\n";
    std::cout << obdm.D.Cols() << "\n\n"; 


    cdouble RandomSign = obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1());

    std::cout << "Randomizing elements of U\n";

    std::cout << "OBDM matrices:\n\n";
    std::cout << "Q:\n";
    PrintColMatrix(obdm.Q.Rows(), obdm.Q.Cols(), obdm.Q.data(), 4);
    std::cout << "R:\n";
    PrintColMatrix(obdm.R.Rows(), obdm.R.Cols(), obdm.R.data(), 4);
    std::cout << "D:\n";
    for (auto& d: obdm.D) std::cout << d << "\n";

    std::cout << "Randomized Matrix Sign " << RandomSign << "\n\n";

    //copy from store
    obdm.Reset();

    std::copy(Store.cbegin(), Store.cend(), obdm.Q.begin());
    std::copy(StoreD.cbegin(), StoreD.cend(), obdm.D.begin());

    obdm.DecomposeEvolution(0.1, WS::tmp1(), WS::tmp2());

    std::cout << "Obdm D size:\n";
    std::cout << obdm.D.Cols() << "\n\n"; 

    cdouble RandomSignTrunc = obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1());

    std::cout << "Randomized Matrix Sign Truncated" << RandomSignTrunc << "\n\n";

    //Fake truncate the random matrix, only drop the scales below truncation threshold, divide such that some are sure to be below!!!!


}

void Run2(Parameters const& params){
    Matrix UMat(params.Volume);
    auto hsfield = HSfield{params};
    OBDM obdm(params, hsfield);
    mini_OBDM<> obdmStorage(params);

    double threshold = 10e-300;
    int maxPrint = (params.Volume < 10 ? params.Volume : 10);

    hsfield.SetRandom();

    obdm.Reset();

    //Compare a bunch of truncated to untruncated matrices
    int MaxComp = 5000;
    Matrix Store(obdm.Q.Rows(), obdm.Q.Cols());
    std::vector<double> StoreD(obdm.Q.Cols());
    int ErrCounter = 0;

    for (int k = 0; k < MaxComp; ++k){
        for (int i = 0; i < obdm.Q.Cols(); i++){
            for (int j = 0; j < obdm.Q.Rows(); j++){
                obdm.Q[j + i*obdm.Q.Rows()] = (RNG::Uniform() - 0.5) * 1 + (RNG::Uniform() - 0.5)*cdouble(0,0);
            }
        }

        std::copy(obdm.Q.cbegin(), obdm.Q.cend(), Store.begin());

        for (auto& d: obdm.D)
            d = (RNG::Uniform() - 0.5) * pow(1.0 , (RNG::Uniform() - 0.2) * 10.0); 

        std::copy(obdm.D.cbegin(), obdm.D.cend(), StoreD.begin());


        obdm.DecomposeEvolution(1e-20, WS::tmp1(), WS::tmp2());

        cdouble RandomSign = obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1());

        obdm.Reset();

        std::copy(Store.cbegin(), Store.cend(), obdm.Q.begin());
        std::copy(StoreD.cbegin(), StoreD.cend(), obdm.D.begin());

        obdm.DecomposeEvolution(0.1, WS::tmp1(), WS::tmp2());

        cdouble RandomSignTrunc = obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1());

        std::cout << "Number of elements truncated: " << obdm.D.MaxCols() - obdm.D.Cols() << "\n";

        cdouble comperator = RandomSign / RandomSignTrunc;
        std::cout << "Sign Ratio " << comperator << "\n\n";

        if (abs((comperator - 1.0).real()) > .000001 || abs((comperator - 1.0).imag()) > .000001){
            ErrCounter++;
            std::cout << "Error in comparison\n\n";
        }

        obdm.Reset();
    }

    std::cout << "Final Error Count: " << ErrCounter << "\n\n";



}

void Run3(Parameters const& params){
    Matrix UMat(params.Volume);
    auto hsfield = HSfield{params};
    OBDM obdm(params, hsfield);
    mini_OBDM<> obdmStorage(params);

    double threshold = 10e-300;
    int maxPrint = (params.Volume < 10 ? params.Volume : 10);

    hsfield.SetRandom();

    obdm.Reset();
    RNG::Initialise(7);


    //Compare a bunch of truncated to untruncated matrices
    Matrix Store(obdm.Q.Rows(), obdm.Q.Cols());
    std::vector<double> StoreD(obdm.Q.Cols());

    for (int i = 0; i < obdm.Q.Cols(); i++){
        for (int j = 0; j < obdm.Q.Rows(); j++){
            obdm.Q[j + i*obdm.Q.Rows()] = (RNG::Uniform() - 0.5) * 1 + (RNG::Uniform() - 0.5)*cdouble(0,0);
        }
    }

    for (auto& d: obdm.D)
        d = (RNG::Uniform() - 0.5) * pow(10.0 , (RNG::Uniform() - 0.5) * 5.0); 


    std::copy(obdm.Q.cbegin(), obdm.Q.cend(), Store.begin());
    std::copy(obdm.D.cbegin(), obdm.D.cend(), StoreD.begin());

    //Save Q and D to a file
    std::ofstream Qfile;
    Qfile.open("Q_init.txt");
    Qfile << std::setprecision(16);
    for (int i = 0; i < obdm.Q.Cols(); i++){
        for (int j = 0; j < obdm.Q.Rows(); j++){
            Qfile << obdm.Q[i + j*obdm.Q.Cols()].real() << "\t";
        }
        Qfile << "\n";
    }
    Qfile.close();

    PrintColMatrix(obdm.Q.Rows(), obdm.Q.Cols(), obdm.Q.data(), 4);

    std::ofstream Dfile;
    Dfile.open("D_init.txt");
    Dfile << std::setprecision(16);
    for (auto& d: obdm.D){
        Dfile << d << "\n";
    }
    Dfile.close();

    std::cout << "Initial Sign " << obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1()) << "\n\n";

    obdm.DecomposeEvolution(1e-300, WS::tmp1(), WS::tmp2());

    std::cout << "Obdm D size:\n";
    std::cout << obdm.D.Cols() << "\n\n"; 

    //Save Q and D to a file
    std::ofstream Qfile2;
    Qfile2.open("Q_final.txt");
    Qfile2 << std::setprecision(16);
    for (int i = 0; i < obdm.Q.Cols(); i++){
        for (int j = 0; j < obdm.Q.Rows(); j++){
            Qfile2 << obdm.Q[i + j*obdm.Q.Cols()].real() << "\t";
        }
        Qfile2 << "\n";
    }
    Qfile2.close();

    std::ofstream Dfile2;
    Dfile2.open("D_final.txt");
    Dfile2 << std::setprecision(16);
    for (auto& d: obdm.D){
        Dfile2 << d << "\n";
    }
    Dfile2.close();

    std::ofstream Rfile2;
    Rfile2.open("R_final.txt");
    Rfile2 << std::setprecision(16);
    for (int i = 0; i < obdm.R.Cols(); i++){
        for (int j = 0; j < obdm.R.Rows(); j++){
            Rfile2 << obdm.R[i + j*obdm.R.Cols()].real() << "\t";
        }
        Rfile2 << "\n";
    }
    Rfile2.close();

    std::cout << "Final Sign " << obdm.SignedOnePlusRLD(WS::tmp_obdm(), WS::tmp1()) << "\n\n";


}

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Please provide input configuration file.\nExiting.\n";
		exit(1);
	}
    MPI_Init(NULL, NULL);

	fftw_init_threads();
	fftw_plan_with_nthreads(1);//omp_get_max_threads());
	#pragma omp barrier

    #if __has_include(<mkl.h>)
    #else
    std::cout << "FFTW planner threads: " << fftw_planner_nthreads() << std::endl;
    #endif
    
	std::cout << "\n";

	auto params = Parameters(argv[1]);

	params.SaveToHDF();
	WS::Initialise(params);
	RNG::Initialise(params.seed);
    WS::InitialisePP(params);

	std::cout << "Coupling U" << params.U << "\n"; 

	for (auto e : params.Sizes) std::cout << e << '\t';
	std::cout <<"\n\n\n\n"<< params.Volume << '\n';

	Run3(params);
    
	WS::Finalise();

	fftw_cleanup_threads();
	std::cout << "FFT final\n\n";
    MPI_Finalize();

	return 0;
}
