/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "params.h"

#include <iostream>
#include <iomanip>
#include <omp.h>
#include <vector>
#include <complex>
#include "mpi.h"

#include "rng.h"
#include "obdm.h"
#include "toml-io.h"
#include "workspace.h"
#include "measureTime.h"
#include "matrix-diag.h"

template <typename T>
void PrintColMatrix(int Rows, int Cols, T* Matrix, int TruncatePrint = 8){
	int PrintRows = (Rows <= TruncatePrint? Rows : TruncatePrint);
	int PrintCols = (Cols <= TruncatePrint? Cols : TruncatePrint);

	std::cout << "prints are truncated at " << TruncatePrint << " Rows/Cols\n";
	for (int i = 0; i < PrintRows; i++){
		for (int j = 0; j < PrintCols; j++){
			std::cout << Matrix[i+j*Rows] << "\t";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}

void Run1(Parameters const& params){
    Matrix UMat(params.Volume);
    auto hsfield = HSfield{params};
	OBDM obdm(params, hsfield);
    mini_OBDM<> obdmStorage(params);

    double threshold = 10e-300;
    int maxPrint = (params.Volume < 10 ? params.Volume : 10);

    if (params.Nt < 100){
        std::cout << "Illegal Nt \n";
        WS::Finalise();
	    fftw_cleanup_threads();
        MPI_Finalize();
	    std::cout << "FFT final\n\n";
        exit(1);
    }

    hsfield.SetRandom();
    /*
    obdm.Reset();
    for (int i = 0; i < 100; i++){
        auto range2 = FakeIota{i, i+1};
        obdm.Evolve(range2.cbegin(), range2.cend(), hsfield.params.muFactorUp);
        obdm.DecomposeEvolution(threshold, WS::tmp1(), WS::tmp2());
    }

    obdmStorage.R.Truncate(obdm.R.Rows(), obdm.R.Cols());
    obdmStorage.Q.Truncate(obdm.Q.Rows(), obdm.Q.Cols());
    obdmStorage.D.Truncate(obdm.D.Rows());

    obdmStorage.CopyFrom(obdm);

    std::cout << "\n\nUsing DecomposeProduct:\n"; 

    obdm.Reset();
    for (int i = 100; i < params.Nt; i++){
        auto range2 = FakeIota{i, i+1};
        obdm.Evolve(range2.cbegin(), range2.cend(), hsfield.params.muFactorUp);
        obdm.DecomposeEvolution(threshold, WS::tmp1(), WS::tmp2());
    }

    obdm.DecomposeProduct(threshold, obdmStorage, WS::tmp1(), WS::tmp2());

    obdm.ReconstructU(UMat, WS::tmp1());

    std::cout << "Umatrix: \n";
    PrintColMatrix(UMat.Rows(), UMat.Cols(), UMat.data(), 4);

    RightMultiplyByDiagonal(obdm.Q, obdm.D);

    std::cout << "Q*D: \n";
    PrintColMatrix(obdm.Q.Rows(), obdm.Q.Cols(), obdm.Q.data(), 4);

    std::cout << "QR Diagonals: \n";

    for (int i = 0; i < maxPrint; i++)
        std::cout << obdm.D[i] << "\n";
    std::cout << "\n";

    LeftMultiplyByDiagonal(obdm.R, obdm.D);

    WS::tmp1().Truncate(obdm.R.Cols(), obdm.R.Rows());
    WS::tmp1().TransposeFrom(obdm.R);

    WS::ComputeJacobiSVD(WS::tmp1(), obdm.D);
    WS::tmp1().Truncate(params.Volume, params.Volume);

    std::cout << "Singular values: \n";
    for (int i = 0; i < maxPrint; i++)
        std::cout << obdm.D[i] << "\n";
    std::cout << "\n\n";

    std::cout << "Using DecomposeEvolution:\n"; 
    obdm.Reset();
    for (int i = 0; i < params.Nt; i++){
        auto range2 = FakeIota{i, i+1};
        obdm.Evolve(range2.cbegin(), range2.cend(), hsfield.params.muFactorUp);
        obdm.DecomposeEvolution(threshold, WS::tmp1(), WS::tmp2());
    }

    obdm.ReconstructU(UMat, WS::tmp1());

    std::cout << "Umatrix: \n";
    PrintColMatrix(UMat.Rows(), UMat.Cols(), UMat.data(), 4);

    RightMultiplyByDiagonal(obdm.Q, obdm.D);

    std::cout << "Q*D: \n";
    PrintColMatrix(obdm.Q.Rows(), obdm.Q.Cols(), obdm.Q.data(), 4);

    std::cout << "QR Diagonals: \n";

    for (int i = 0; i < maxPrint; i++)
        std::cout << obdm.D[i] << "\n";
    std::cout << "\n";

    std::cout << "Singular values: \n";

    LeftMultiplyByDiagonal(obdm.R, obdm.D);

    WS::tmp1().Truncate(obdm.R.Cols(), obdm.R.Rows());
    WS::tmp1().TransposeFrom(obdm.R);

    WS::ComputeJacobiSVD(WS::tmp1(), obdm.D);

    for (int i = 0; i < maxPrint; i++)
        std::cout << obdm.D[i] << "\n";
    std::cout << "\n";

    */
    obdm.Reset();

    std::cout << "Difference between Evolve with and without decomposition: \n";

    for (int i = 0; i < params.Nt; i++){
        auto range2 = FakeIota{i, i+1};
        obdm.Evolve(range2.cbegin(), range2.cend(), hsfield.params.muFactorUp);
        obdm.DecomposeEvolution(threshold, WS::tmp1(), WS::tmp2());
    }

    obdm.ReconstructU(UMat, WS::tmp1());

    obdm.Reset();
    auto rangex = FakeIota{0, params.Nt};
    obdm.Evolve(rangex.cbegin(), rangex.cend(), hsfield.params.muFactorUp);

    double maxdiff = 0.0;

    for (int i = 0; i < params.Volume * params.Volume; i++){
            double diff = std::abs(UMat[i] - obdm.Q[i]) / std::abs(UMat[i]);
            if (diff > maxdiff) maxdiff = diff;
    }

    std::cout << "Max difference: " << maxdiff << "\n";


}

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Please provide input configuration file.\nExiting.\n";
		exit(1);
	}
    	MPI_Init(NULL, NULL);

	fftw_init_threads();
	fftw_plan_with_nthreads(1);//omp_get_max_threads());
	#pragma omp barrier

    #if __has_include(<mkl.h>)
    #else
    std::cout << "FFTW planner threads: " << fftw_planner_nthreads() << std::endl;
    #endif
    
	std::cout << "\n";

	auto params = Parameters(argv[1]);
	params.SaveToHDF();
	WS::Initialise(params);
	RNG::Initialise(params.seed);
    WS::InitialisePP(params);

	std::cout << "Coupling U" << params.U << "\n"; 

	for (auto e : params.Sizes) std::cout << e << '\t';
	std::cout <<"\n\n\n\n"<< params.Volume << '\n';

	Run1(params);
    
	WS::Finalise();

	fftw_cleanup_threads();
	std::cout << "FFT final\n\n";
    MPI_Finalize();

	return 0;
}
