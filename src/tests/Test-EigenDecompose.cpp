/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "params.h"

#include <iostream>
#include <iomanip>
#include <omp.h>
#include <vector>
#include <complex>
#include "mpi.h"

#include "rng.h"
#include "obdm.h"
#include "toml-io.h"
#include "workspace.h"
#include "measureTime.h"
#include "matrix-diag.h"

template <typename T>
void PrintColMatrix(int Rows, int Cols, T* Matrix, int TruncatePrint = 8){
	int PrintRows = (Rows <= TruncatePrint? Rows : TruncatePrint);
	int PrintCols = (Cols <= TruncatePrint? Cols : TruncatePrint);

	std::cout << "prints are truncated at " << TruncatePrint << " Rows/Cols\n";
	for (int i = 0; i < PrintRows; i++){
		for (int j = 0; j < PrintCols; j++){
			std::cout << Matrix[i+j*Rows] << "\t";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}

void Run(Parameters const& params){
    Matrix UMat(params.Volume);
    auto hsfield = HSfield{params};
	OBDM obdm(params, hsfield);
    mini_OBDM<cdouble> EigenStorage(params);
    std::vector<double> Scale(params.Volume);

    double threshold = 10e-6;
    int maxPrint = (params.Volume < 10 ? params.Volume :10);

    obdm.Reset();
    for (int i = 0; i < params.Nt; i++){
        auto range2 = FakeIota{i, i+1};
        obdm.Evolve(range2.cbegin(), range2.cend(), 1.0);
        obdm.DecomposeEvolution(threshold, WS::tmp1(), WS::tmp2());
    }

    //Copy data into a mini_obdm<cdouble> conatine, this should be moved inside the function!
    //EigenStorage.CopyFrom(obdm);


    obdm.PerformEigenDRL(EigenStorage, WS::tmp1());

    std::cout << "Eigenvalues: \n";

    for (int i = 0; i < maxPrint; i++)
        std::cout << EigenStorage.D[i] << "\n";
    std::cout << "\n";

    std::cout << "Diagonal Values: \n";

    for (int i = 0; i < maxPrint; i++)
        std::cout << obdm.D[i] << "\n";
    std::cout << "\n";

    WS::tmp1().Truncate(obdm.R.Cols(), obdm.R.Rows());
    WS::tmp1().TransposeFrom(obdm.R);
    RightMultiplyByDiagonal(WS::tmp1(), obdm.D);

    WS::ComputeJacobiSVD(WS::tmp1(), WS::tmp_obdm().D);

    std::cout << "Singular values: \n";
    for (int i = 0; i < maxPrint; i++)
        std::cout << WS::tmp_obdm().D[i] << "\n";
    std::cout << "\n\n";

    WS::tmp1().ResetSize();

    obdm.RecoverEigenVectors(EigenStorage, WS::tmp1(), WS::tmp2());

    EigenStorage.ReconstructU(UMat, WS::tmp1());
    
    std::cout << "Umat from Eigen: \n";
    PrintColMatrix(UMat.Cols(), UMat.Rows(), UMat.data(), 5);

    obdm.ReconstructU(UMat, WS::tmp1());
    
    std::cout << "Umat from original obdm: \n";
    PrintColMatrix(UMat.Cols(), UMat.Rows(), UMat.data(), 5);

}


int main(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Please provide input configuration file.\nExiting.\n";
		exit(1);
	}
	fftw_init_threads();
	fftw_plan_with_nthreads(1);//omp_get_max_threads());
	#pragma omp barrier

    #if __has_include(<mkl.h>)
    #else
    std::cout << "FFTW planner threads: " << fftw_planner_nthreads() << std::endl;
    #endif
    
	std::cout << "\n";

	auto params = Parameters(argv[1]);
	params.SaveToHDF();
	WS::Initialise(params);
	RNG::Initialise(params.seed);

	std::cout << "Coupling U" << params.U << "\n"; 

	for (auto e : params.Sizes) std::cout << e << '\t';
	std::cout <<"\n\n\n\n"<< params.Volume << '\n';

	Run(params);
    
	WS::Finalise();

	fftw_cleanup_threads();
	std::cout << "FFT final\n\n";

	return 0;
}
