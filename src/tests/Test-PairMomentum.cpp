/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "params.h"

#include <iostream>
#include <iomanip>
#include <omp.h>
#include <vector>
#include <complex>

#include "rng.h"
#include "obdm.h"
#include "toml-io.h"
#include "workspace.h"
#include "measureTime.h"
#include "matrix-diag.h"
#include "analysis.h"
#include "mpi.h"

void Run(Parameters const& params){

    std::vector<std::vector<cdouble>> Kvalues(params.Volume);

    for (int i = 0; i < params.Volume; i++){
        Kvalues[i] = std::vector<cdouble>(params.Sizes.size());
    }

    for (int i = 0; i < Kvalues.size(); i++){
        double p2 = 0;
        int tmp = static_cast<int>(i);
        std::transform(params.Sizes.crbegin(), params.Sizes.crend(), Kvalues[i].rbegin(),
                [&tmp, &p2](int const n) {
                int const intMom = tmp % n;
                double const p = (intMom <= std::floor(n/2) ? intMom : intMom - n) * 2.0 * M_PI / n;
                tmp /= n;

                return p;
                });

    }

    for (auto& k: Kvalues){
        for (auto& p: k){
            std::cout << p << "\t";
        }
        std::cout << "\n";
    }

    std::vector <int> tmp_vec(params.Sizes.size());
    int tmp_index;
    std::vector <int> tmp_vec2(params.Sizes.size());
    int tmp_index2;

    for (int i = 0; i < params.Volume; i++){
        tmp_vec = VectorFromIndex(i, params.Sizes);
        tmp_index = NegativeIndexFromVector(tmp_vec, params.Sizes);

        std::cout << "\'Positives\' are: \t";
        for (auto& k: Kvalues[i]){
            std::cout << k << "\t";
        }
        std::cout << "\t";
        std::cout << "Negatives are: \t";
        for (auto& k: Kvalues[tmp_index]){
            std::cout << k << "\t";
        }
        std::cout << "\t";
        std::cout << "Sum is: \t";
        for (int j = 0; j < params.Sizes.size(); j++){
            std::cout << Kvalues[tmp_index][j] + Kvalues[i][j] << "\t";
        }

        std::cout << "\n";
    }

    std::cout << "Now dont loop over the outermost negative momenta\n";
    for (int i = 0; i < std::ceil(params.Volume/2); i++){
        tmp_vec = VectorFromIndex(i, params.Sizes);
        tmp_index = NegativeIndexFromVector(tmp_vec, params.Sizes);

        std::cout << "\'Positives\' are: \t";
        for (auto& k: Kvalues[i]){
            std::cout << k << "\t";
        }
        std::cout << "\t";
        std::cout << "Negatives are: \t";
        for (auto& k: Kvalues[tmp_index]){
            std::cout << k << "\t";
        }
        std::cout << "\t";
        std::cout << "Sum is: \t";
        for (int j = 0; j < params.Sizes.size(); j++){
            std::cout << Kvalues[tmp_index][j] + Kvalues[i][j] << "\t";
        }

        std::cout << "\n";
    }


}

int main(int argc, char **argv){

  	if (argc < 2) {
		std::cerr << "Please provide input configuration file.\nExiting.\n";
		exit(1);
	}
    	MPI_Init(NULL, NULL);

	fftw_init_threads();
	fftw_plan_with_nthreads(1);//omp_get_max_threads());
	#pragma omp barrier

    #if __has_include(<mkl.h>)
    #else
    std::cout << "FFTW planner threads: " << fftw_planner_nthreads() << std::endl;
    #endif
    
	std::cout << "\n";

	auto params = Parameters(argv[1]);
	//params.SaveToHDF();
	WS::Initialise(params);
	RNG::Initialise(params.seed);
    WS::InitialisePP(params);

	std::cout << "Coupling U" << params.U << "\n"; 

	for (auto e : params.Sizes) std::cout << e << '\t';
	std::cout <<"\n\n\n\n"<< params.Volume << '\n';

	Run(params);
    
	WS::Finalise();

	fftw_cleanup_threads();
	std::cout << "FFT final\n\n";
    MPI_Finalize();


    return 0;
}
