/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include "params.h"

#include <iostream>
#include <iomanip>
#include <omp.h>
#include <vector>
#include <complex>

#include "rng.h"
#include "obdm.h"
#include "toml-io.h"
#include "workspace.h"
#include "measureTime.h"
#include "matrix-diag.h"
#include "analysis.h"
#include "mpi.h"

void Run(Parameters const& params){
    Matrix UMat(params.Volume);
    auto hsfield = HSfield{params};
    OBDM obdm(params, hsfield);
    mini_OBDM<> obdmStorage(params);
    mini_OBDM<cdouble> eigenObdm(params);

    //load specific configuration

    const int cfgNum = 72;

    hsfield.ReadFromHDF(cfgNum);

    hsfield.ComputeGlobalLogHSRatio_BDM();

    //Evolve Field very carefully

    int DecomposeEvery = 16;
    int Decompositions = params.Nt / DecomposeEvery;
    obdm.Reset();

    for (int i = 0; i < Decompositions; ++i)
        {
            auto range = FakeIota{DecomposeEvery * i, DecomposeEvery * (i + 1)};
			obdm.Evolve(range.cbegin(), range.cend(), hsfield.params.muFactorUp);

            obdm.DecomposeEvolution(1e-30, WS::tmp1(), WS::tmp2());
        }

    //Compute 1+DRL

    DiagonalMatrix<double> OnePlusDiags(params.Volume);
    obdm.OnePlusRLD(OnePlusDiags, WS::tmp1());

    cdouble WeightFromQR = 0.0;

    std::cout << "elements of the diag of 1 + DRL\n";
    for (int i = 0; i < obdm.D.Cols(); i++){
        WeightFromQR += std::log(abs(OnePlusDiags[i]));
        std::cout << OnePlusDiags[i] << "\n";
    }
    std::cout << std::endl;
    
    //print all Digaonal elements
    std::cout << "Diagonal elements after full evolve at size " << obdm.D.Cols() << "\n";
    for (int i = 0; i < obdm.D.Cols(); i++){
        std::cout << obdm.D[i] << "\n";
    }

    //Compute eigen decomp of U
    obdm.PerformEigenDRL(eigenObdm, WS::tmp1());
    obdm.RecoverEigenVectors(eigenObdm, WS::tmp1(), WS::tmp2());

    std::cout << eigenObdm.D.Cols() << "\n";

    // Compute the reduced density matrix on the eigenvalues

    //print all eigenvalues

    std::cout << "Eigenvalues after full evolve at size " << eigenObdm.D.Cols() << "\n";
    for (int i = 0; i < eigenObdm.D.Cols(); i++){
        std::cout << eigenObdm.D[i] << "\n";
    }

    //Compute det weigth
    cdouble DetWeight = 0.0;
    for (auto& ev: eigenObdm.D){
        DetWeight += std::log(1.0 + ev);
    }

    std::cout << std::setprecision(16);

    std::cout << "\nGlobal Log HS Weight: " << hsfield.HSWeight << "\n";

    std::cout << "Det Weight: " << 2.0*DetWeight << "\n";

    std::cout << "Weight from QR: " << 2.0*WeightFromQR << "\n";

    std::cout << "Full Weight:" << hsfield.HSWeight + DetWeight + DetWeight << "\n\n";

    for (auto& ev: eigenObdm.D){
        ev = 1.0 - 1.0 / (1.0 + ev);
    }

    //recompose the full matrix
    eigenObdm.ReconstructU(WS::tmp1(), WS::tmp2());

    std::cout << "Large elements in Reduced Density Matrix from eigen\n";

    for (auto& elem : WS::tmp1()){
        if (std::abs(elem) > 1){
            std::cout << elem << "\n";
        }
    }

    obdm.ComputeReducedDensityMatrix(obdmStorage, WS::tmp2());

    //print diagonals of the reduced density matrix

    std::cout << "Diagonal elements of Reduced Density Matrix\n";
    for (int i = 0; i < obdmStorage.D.Cols(); i++){
        std::cout << obdmStorage.D[i] << "\n";
    }

    obdmStorage.ReconstructU(WS::tmp1(), WS::tmp2());

    std::cout << "Large elements in Reduced Density Matrix\n";

    for (auto& elem : WS::tmp1()){
        if (std::abs(elem) > 1){
            std::cout << elem << "\n";
        }
    }

}

void Run_Scan_Samples(Parameters const& params){
    Matrix UMat(params.Volume);
    auto hsfield = HSfield{params};
    OBDM obdm(params, hsfield);
    mini_OBDM<> obdmStorage(params);
    mini_OBDM<cdouble> eigenObdm(params);

    //loop over configurations

    int num_Configs = hsfield.GetNumberOfConfigs();
    int DecomposeEvery = 16;
    int Decompositions = params.Nt / DecomposeEvery;

    std::ofstream small_ev_vs_rdm("small_ev_vs_rdm.txt");


    for(size_t config_num = 0; config_num < num_Configs; config_num++){
        cdouble smallest_ev = 1.0;
        cdouble weight = 0.0;

        cdouble MaxRDM = 0.0;

        //Load config
        hsfield.ReadFromHDF(config_num);

        //Evolve
        obdm.Reset();

        for (int i = 0; i < Decompositions; ++i)
            {
                auto range = FakeIota{DecomposeEvery * i, DecomposeEvery * (i + 1)};
                obdm.Evolve(range.cbegin(), range.cend(), hsfield.params.muFactorUp);

                obdm.DecomposeEvolution(1e-5, WS::tmp1(), WS::tmp2());
            }

        //Compute eigen decomp of U
        obdm.PerformEigenDRL(eigenObdm, WS::tmp1());
        obdm.RecoverEigenVectors(eigenObdm, WS::tmp1(), WS::tmp2());

        for (auto& ev: eigenObdm.D){
            weight += std::log(1.0 + ev);
            if (std::abs(ev + 1.0) < std::abs(smallest_ev)){
                smallest_ev = ev + 1.0;
            }
        }

        small_ev_vs_rdm << smallest_ev.real() << "\t"  << smallest_ev.imag() << "\t" << weight.real() << "\t"  << weight.imag() << "\t";
        std::cout << smallest_ev << "\t" << weight << "\t";


        // Compute the reduced density matrix on the eigenvalues
        for (auto& ev: eigenObdm.D){
            ev = 1.0 - 1.0 / (1.0 + ev);
        }

        eigenObdm.ReconstructU(WS::tmp1(), WS::tmp2());

        for (auto& P :WS::tmp1()){
            if (std::abs(P) > abs(MaxRDM)){
                MaxRDM = P;
            }
        }

        small_ev_vs_rdm << MaxRDM.real() << "\t"  << MaxRDM.imag() << "\t" <<  config_num << "\n";

    }

    small_ev_vs_rdm.close();

}

int main(int argc, char **argv){
    if (argc < 2) {
		std::cerr << "Please provide input configuration file.\nExiting.\n";
		exit(1);
	}
    MPI_Init(NULL, NULL);

	fftw_init_threads();
	fftw_plan_with_nthreads(1);//omp_get_max_threads());
	#pragma omp barrier

    #if __has_include(<mkl.h>)
    #else
    std::cout << "FFTW planner threads: " << fftw_planner_nthreads() << std::endl;
    #endif
    
	std::cout << "\n";

	auto params = Parameters(argv[1], true);
    
    WS::Initialise(params);
	RNG::Initialise(params.seed);

	WS::Initialise(params);
	RNG::Initialise(params.seed);
    WS::InitialisePP(params);

	std::cout << "Coupling U" << params.U << "\n"; 

	for (auto e : params.Sizes) std::cout << e << '\t';
	std::cout <<"\n\n\n\n"<< params.Volume << '\n';
    
    int const rank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;}();
 

    if (rank == 0){
        std::cout << "Running on rank " << rank << "\n";

        //Run(params);
        Run_Scan_Samples(params);
    }


	WS::Finalise();

	fftw_cleanup_threads();
	std::cout << "FFT final\n\n";
    MPI_Finalize();

	return 0;

}
