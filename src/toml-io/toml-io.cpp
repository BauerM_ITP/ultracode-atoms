/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include <mpi.h>
#include <fstream>
#include <iostream>

#include "toml-io.h"
#include "highfive/H5File.hpp"

/*
Parameters::Parameters(std::string const filename)
	: Parameters(toml::parse_file(filename))
{ }
*/

auto GetRank = []{int x; MPI_Comm_rank(MPI_COMM_WORLD, &x); return x;};

namespace HF = HighFive;

Parameters::Parameters(std::string const filename, bool HDFTruncateOverrideSwitch)
	: tbl{toml::parse_file(filename)}
	, Sizes{[this]{
		int nx = tbl["physics"]["Nx"].value_or(1);
		int ny = tbl["physics"]["Ny"].value_or(0);
		int nz = tbl["physics"]["Nz"].value_or(0);
		if (ny == 0) return std::vector<int>{nx};
		else if (nz == 0) return std::vector<int>{nx, ny};
		else return std::vector<int>{nx, ny, nz};
		}()}
	, Volume{[this]{
		int out = 1; for (auto e : Sizes) out *= e; return out;
		}()}
	, Nt{static_cast<int>(tbl["physics"]["Nt"].value_or(1))}
	, beta{tbl["physics"]["beta"].value_or(0.0)}
	, anisotropy{beta / Nt}
	, muUp{tbl["physics"]["muUp"].value_or(0.0)}
	, muDn{tbl["physics"]["muDn"].value_or(0.0)}
	, muFactorUp{std::exp(anisotropy * muUp)}
	, muFactorDn{std::exp(anisotropy * muDn)}
    , omegaInput{tbl["physics"]["omega"].value_or(0.0)}
	, omega{anisotropy / 2.0 * std::pow(omegaInput, 2)}
	, U{cdouble{tbl["physics"]["U_real"].value_or(0.0), tbl["physics"]["U_imag"].value_or(0.0)}}
	, ExtPotentialCutoff{tbl["physics"]["ExtPotentialCutoff"].value_or(1000000000.0)}
	, hdfOutName{std::to_string(GetRank()) + "_" + tbl["io"]["hdfOutName"].value_or("PLEASE_CHOOSE_OUTPUT_FILE_NAME.hdf5")}
	, overwriteHDF{tbl["io"]["overwriteHDF"].value_or(false)}
	, resumeFromHDF{tbl["io"]["resumeFromHDF"].value_or(false)}
	, useParamsFromHDF{tbl["io"]["useParamsFromHDF"].value_or(false)}
	, seed{static_cast<int>(tbl["simulation"]["seed"].value_or(0))}
	, DecomposeEvery{static_cast<int>(tbl["simulation"]["DecomposeEvery"].value_or(1))}
    , NumberOfSweeps{static_cast<int>(tbl["simulation"]["NumberOfSweeps"].value_or(1))}
    , ThermalisationSweeps{[&]{
		if (overwriteHDF == false && resumeFromHDF == true) return -1;
		else return static_cast<int>(tbl["simulation"]["ThermalisationSweeps"].value_or(1));
		}()}
    , SaveEvery{static_cast<int>(tbl["simulation"]["SaveEvery"].value_or(1))}
    , MaxPPTruncationUp{static_cast<int>(tbl["simulation"]["MaxPPTruncationUp"].value_or(Volume))}
    , MaxPPTruncationDown{static_cast<int>(tbl["simulation"]["MaxPPTruncationDown"].value_or(Volume))}
    , minAnisotropy{double(tbl["tempering"]["minAnisotropy"].value_or(0.005))}
    , TemperingSteps{static_cast<int>(tbl["tempering"]["TemperingSteps"].value_or(1))}
    , SweepsPerTemperature{static_cast<int>(tbl["tempering"]["SweepsPerTemperature"].value_or(1))}
    , TargetN_up{static_cast<int>(tbl["canonical"]["TargetN_up"].value_or(1))}
    , TargetN_down{static_cast<int>(tbl["canonical"]["TargetN_down"].value_or(1))}
    , FourierMax_up{static_cast<int>(tbl["canonical"]["FourierMax_up"].value_or(10))}
    , FourierMax_down{static_cast<int>(tbl["canonical"]["FourierMax_down"].value_or(10))}
    , n_min_up{static_cast<int>(tbl["canonical"]["n_min_up"].value_or(0))}
    , n_min_down{static_cast<int>(tbl["canonical"]["n_min_down"].value_or(0))}
    , n_max_up{static_cast<int>(tbl["canonical"]["n_max_up"].value_or(2))}
    , n_max_down{static_cast<int>(tbl["canonical"]["n_max_down"].value_or(2))}
{
	try {
		{
		// create file -- nothing happens if it already exists
		auto file = HF::File{hdfOutName, HF::File::Create};
		}

		// empties file in case we want to overwrite
		if (overwriteHDF == true && HDFTruncateOverrideSwitch == false)
			auto file2 = HF::File{hdfOutName, HF::File::Truncate};
	} catch (std::exception& e) {
		std::cerr << "# -- Error initialising HDF file " << hdfOutName
			<< ".\n # -- Error message: " << e.what() << '\n';
	}
}

template<typename T>
void SaveAttribute(HF::Group& group, std::string const name, T const value) {
	if (group.hasAttribute(name) == true) {
		auto attr = group.getAttribute(name);
		std::cout << attr.read<T>() << "\n\n";
		attr.write(value);
	} else {
		//std::cout << "HERE\n\n\n\n\n\n\n";
		auto attr = group.createAttribute<T>(name, HF::DataSpace::From(value));
		attr.write(value);
	}
}

template<typename T>
T ReadAttribute(HF::Group& group, std::string const name) {
	auto attr = group.getAttribute(name);
	return attr.read<T>();
}

void Parameters::SaveToHDF(std::string filename) const {
    	try {
		auto file = HF::File{filename, HF::File::ReadWrite | HF::File::Create};
		auto group = [&file]{
			if (file.exist("parameters") == true)
				return file.getGroup("parameters");
			else
				return file.createGroup("parameters");
		}();

		SaveAttribute(group, "Nx", Sizes[0]);
		if (Sizes.size() > 1) 
			SaveAttribute(group, "Ny", Sizes[1]);
		else 
			SaveAttribute(group, "Ny", 0);
        if (Sizes.size() > 2)
			SaveAttribute(group, "Nz", Sizes[2]);
		else
			SaveAttribute(group, "Nz", 0);
		SaveAttribute(group, "Nt", Nt);
		SaveAttribute(group, "beta", beta);
		SaveAttribute(group, "muUp", muUp);
		SaveAttribute(group, "muDn", muDn);
		SaveAttribute(group, "omegaInput", omegaInput);
		SaveAttribute(group, "U_real", U.real());
		SaveAttribute(group, "U_imag", U.imag());
		SaveAttribute(group, "ExtPotentialCutoff", ExtPotentialCutoff);
		SaveAttribute(group, "seed", seed);
		SaveAttribute(group, "DecomposeEvery", DecomposeEvery);
		SaveAttribute(group, "NumberOfSweeps", NumberOfSweeps);
		SaveAttribute(group, "ThermalisationSweeps", ThermalisationSweeps);
		SaveAttribute(group, "SaveEvery", SaveEvery);
        SaveAttribute(group, "TargetN_up", TargetN_up);
        SaveAttribute(group, "TargetN_down", TargetN_down);
        SaveAttribute(group, "FourierMax_up", FourierMax_up);
        SaveAttribute(group, "FourierMax_down", FourierMax_down);
        SaveAttribute(group, "n_min_up", n_min_up);
        SaveAttribute(group, "n_min_down", n_min_down);
        SaveAttribute(group, "n_max_up", n_max_up);
        SaveAttribute(group, "n_max_down", n_max_down);

	} catch (std::exception& e) {
		std::cerr << "# -- Error saving parameters to HDF file " << filename
			<< ".\n # -- Error message: " << e.what() << '\n';
	}
}

void Parameters::SaveToHDF() const {
    SaveToHDF(hdfOutName);
}

void Parameters::SetAnisotropy(double NewAnisotropy){
    anisotropy = NewAnisotropy;
    omega = anisotropy / 2.0 * std::pow(omegaInput, 2);
	muFactorUp = std::exp(anisotropy * muUp);
	muFactorDn = std::exp(anisotropy * muDn);

    std::cout << "Setting anisotropy to " << anisotropy << ", please reset Energy/Potential/Coupling\n";
    std::cout << "Setting omega (internal) to " << omega << "\n";

}

void Parameters::ReadFromHDF(std::string filename){
    	try {
		auto file = HF::File{filename, HF::File::ReadWrite};
		auto group = file.getGroup("parameters");
		auto nx = ReadAttribute<int>(group, "Nx");
		auto ny = ReadAttribute<int>(group, "Ny");
		auto nz = ReadAttribute<int>(group, "Nz");
		auto sizes = [nx, ny, nz]{
			if (ny == 0) return std::vector<int>{nx};
			else if (nz == 0) return std::vector<int>{nx, ny};
			else return std::vector<int>{nx, ny, nz};
		}();
		Sizes = sizes;
		Volume = [this]{int out = 1; for (auto e : Sizes) out *= e; return out;}();
		Nt = ReadAttribute<int>(group, "Nt");
		beta = ReadAttribute<double>(group, "beta");
		anisotropy = beta / Nt;
		muUp = ReadAttribute<double>(group, "muUp");
		muDn = ReadAttribute<double>(group, "muDn");
		muFactorUp = std::exp(anisotropy * muUp);
		muFactorDn = std::exp(anisotropy * muDn);
		omegaInput = ReadAttribute<double>(group, "omegaInput");
		omega = anisotropy / 2.0 * std::pow(omegaInput, 2);
		U = cdouble{ReadAttribute<double>(group, "U_real"),
			ReadAttribute<double>(group, "U_imag")};
		ExtPotentialCutoff = ReadAttribute<double>(group, "ExtPotentialCutoff");

		DecomposeEvery = ReadAttribute<int>(group, "DecomposeEvery");
		NumberOfSweeps = ReadAttribute<int>(group, "NumberOfSweeps");
		ThermalisationSweeps = ReadAttribute<int>(group, "ThermalisationSweeps");
		SaveEvery = ReadAttribute<int>(group, "SaveEvery");
        TargetN_up = ReadAttribute<int>(group, "TargetN_up");
        TargetN_down = ReadAttribute<int>(group, "TargetN_down");
        FourierMax_up = ReadAttribute<int>(group, "FourierMax_up");
        FourierMax_down = ReadAttribute<int>(group, "FourierMax_down");
        n_min_up = ReadAttribute<int>(group, "n_min_up");
        n_min_down = ReadAttribute<int>(group, "n_min_down");
        n_max_up = ReadAttribute<int>(group, "n_max_up");
        n_max_down = ReadAttribute<int>(group, "n_max_down");
		// TODO do other parameters?
		std::cout << "# Imported run parameters from HDF file: " << filename << '\n';
	} catch (std::exception& e) {
		std::cerr << "# -- Error reading data from HDF file " << filename
			<< ".\n # -- Error message: " << e.what() << '\n';
	}
}

void Parameters::ReadFromHDF() {
	ReadFromHDF(hdfOutName);
}
