/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef TOML_IO_H
#define TOML_IO_H

#include <vector>
#include <string>
#include <complex>

#include "params.h"
#include "toml.hpp"

struct Parameters {
private:
	toml::table tbl;
public:
	std::vector<int> Sizes;
	int Volume;
	int Nt;
	double beta;
	double anisotropy;
	double muUp;
	double muDn;
	double muFactorUp;
	double muFactorDn;
	double omegaInput;
	double omega;
	cdouble U;
	double ExtPotentialCutoff;

	std::string hdfOutName;
	bool overwriteHDF;
	bool resumeFromHDF;
	bool useParamsFromHDF;

	int seed;
	int DecomposeEvery;
	int NumberOfSweeps;
	int ThermalisationSweeps;
	int SaveEvery;
    int MaxPPTruncationUp;
    int MaxPPTruncationDown;


	double minAnisotropy;
	int TemperingSteps;
	int SweepsPerTemperature;

    // Parameters for the canonical projection
    int TargetN_up;
    int TargetN_down;
    int FourierMax_up;
    int FourierMax_down;
    int n_min_up;
    int n_min_down;
    int n_max_up;
    int n_max_down;

	Parameters(std::string filename, bool HDFTruncateOverrideSwitch = false);

    void ReadFromHDF(std::string filename);
    void SaveToHDF(std::string filename) const;

    void ReadFromHDF();
	void SaveToHDF() const;

	void SetAnisotropy(double NewAnisotropy);
};
#endif
