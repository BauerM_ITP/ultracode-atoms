/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#include <vector>
#include <memory>
#include "workspace.h"

namespace WS {
	std::unique_ptr<MatrixFFT> tmp1_;
	std::unique_ptr<Matrix> tmp2_;
	std::vector<std::unique_ptr<DiagonalMatrix<double>>> rDiag_;
	MatrixFFT& tmp1() { return *tmp1_; }
	Matrix& tmp2() { return *tmp2_; }
	DiagonalMatrix<double>& rDiag(int i) { return *(rDiag_[i]); }

	std::vector<std::unique_ptr<mini_OBDM<>>> tmp_obdm_;
	std::vector<std::unique_ptr<mini_OBDM<>>> Storage_;
	mini_OBDM<>& tmp_obdm(int i) { return *(tmp_obdm_[i]); }
	mini_OBDM<>& Storage(int i) { return *(Storage_[i]); }
	//
	std::vector<std::unique_ptr<mini_OBDM<>>>& TMP_obdm() { return tmp_obdm_; }
	//
    std::vector<std::unique_ptr<mini_OBDM<>>> PartialProductsUP;
    std::vector<bool> AvailabilityMaskUP;
    std::vector<std::unique_ptr<mini_OBDM<>>> PartialProductsDOWN;
    std::vector<bool> AvailabilityMaskDOWN;

    int UniqueOBDMs() { return (PartialProductsDOWN.size() == 0 ? 1 : 2); }


    mini_OBDM<>& PartialProducts(int spin, int ProductIndex) {
        if (spin == 0) {
            return *(PartialProductsUP[ProductIndex]);
        } 
        else {

            if (PartialProductsDOWN.size() == 0) {
                std::cout << "ERROR: PartialProductsDOWN has not been initialised, returning PartialProductsUP" << std::endl;
                return *(PartialProductsUP[ProductIndex]);
            } 
            else{
                return  *(PartialProductsDOWN[ProductIndex]);
            }
        }
    }

    std::vector<bool>& AvailabilityMask(int spin) {
        if (spin == 0) {
            return AvailabilityMaskUP;
        } 
        else {

            if (AvailabilityMaskDOWN.size() == 0) {
                std::cout << "ERROR: AvailabilityMaskDOWN has not been initialised, returning AvailabilityMaskUP" << std::endl;
                return AvailabilityMaskUP;
            } 
            else{
                return AvailabilityMaskDOWN;
            }
        }
    }

	int lwork_, lrwork_;
	int const& lwork()  { return lwork_;  }
	int const& lrwork() { return lrwork_; }

	std::vector<int> pivots_;
	myVec<cdouble> h_tau_;
	myVec<cdouble> work_;
	std::vector<double> rwork;

    std::vector<int>& pivots() { return pivots_; }
	myVec<cdouble>& h_tau() { return h_tau_; }
	myVec<cdouble>& work() { return work_; }

    int ilo_;
    int ihi_;
    double ABNRM_;
    double RCONDE_;
    double RCONDV_;

	// const& ?
    int& ilo() {return ilo_;};
    int& ihi() {return ihi_;};
    double& ABNRM() {return ABNRM_;};
    double& RCONDE() {return RCONDE_;};
    double& RCONDV() {return RCONDV_;};

    std::vector<double> scale_; 
    std::vector<double>& scale() { return scale_; }

	void Initialise(Parameters const& params) {
		int const Msize = params.Volume;
		tmp1_ = std::make_unique<MatrixFFT>(params.Sizes);
		tmp2_ = std::make_unique<Matrix>(Msize);
		rDiag_.emplace_back(std::make_unique<DiagonalMatrix<double>>(Msize));
		rDiag_.emplace_back(std::make_unique<DiagonalMatrix<double>>(Msize));
		tmp_obdm_.emplace_back(std::make_unique<mini_OBDM<>>(params));
		tmp_obdm_.emplace_back(std::make_unique<mini_OBDM<>>(params));
//		tmp_obdm_ = {std::make_unique<mini_OBDM<>>(params), std::make_unique<mini_OBDM<>>(params)};
		Storage_.emplace_back(std::make_unique<mini_OBDM<>>(params));
		Storage_.emplace_back(std::make_unique<mini_OBDM<>>(params));

		lrwork_ = 2*Msize;
		rwork.resize(lrwork_);

		std::array<cdouble, 4> arr;
		int tmp_work = -1;
		int xxxx = 97;
		int out = 0;
		LinAlg(zgesvj)(&JOBA, &JOBU, &JOBV,
			&Msize, &Msize, tmp2_->c_data(), &Msize,
			 rwork.data(),	// I don't understand why rwork needs to be allocated when tmp_work=-1 
			 &MV, tmp2_->c_data(), &Msize,
			 reinterpret_cast<__complex__ double*>(&arr[0]), &tmp_work,
			 rwork.data(), &lrwork_, &xxxx);

		LinAlg(zgeqp3)(&Msize, &Msize,
			tmp2_->c_data(), &Msize,
			NULL, h_tau_.c_data(),
			reinterpret_cast<__complex__ double*>(&arr[1]), &tmp_work,
			rwork.data(), &xxxx);

	    LinAlg(zungqr)(&Msize, &Msize,
				&Msize, tmp2_->c_data(), &Msize,
				h_tau_.c_data(),
				reinterpret_cast<__complex__ double*>(&arr[2]), &tmp_work, &xxxx);

        LinAlg(zgeevx)(&BALANC, &JOBVL , &JOBVR, &SENSE, &Msize,
            tmp2_->c_data(), &Msize,
            tmp2_->c_data(),
            tmp2_->c_data(), &Msize,
            tmp2_->c_data(), &Msize,
            &ilo_, &ihi_,
            scale_.data(), &ABNRM_, &RCONDE_, &RCONDV_,
            reinterpret_cast<__complex__ double*>(&arr[3]), &tmp_work,
            rwork.data(), &xxxx);

		for (auto const e : arr) out = std::max(out, static_cast<int>(e.real()));

		lwork_ = out;
		pivots_.resize(Msize);
		work_.resize(lwork_);
		h_tau_.resize(Msize);
        scale_.resize(Msize); 
	}

    void InitialisePP(Parameters const& params) {
        int const Decompositions = params.Nt / params.DecomposeEvery;

        if(params.muDn == params.muUp){
            std::cout << "WARNING: muDn == muUp, only one set of PartialProducts will be used" << std::endl;
            for (int i = 0; i < Decompositions; ++i) {
                //std::cout << "using " << params.MaxPPTruncationUp  << std::endl;
                PartialProductsUP.emplace_back(std::make_unique<mini_OBDM<>>(params, params.MaxPPTruncationUp));
            }
            AvailabilityMaskUP.resize(Decompositions, false);
        }
        else {
            std::cout << "WARNING: muDn != muUp, two sets of PartialProducts will be used" << std::endl;
            for (int i = 0; i < Decompositions; ++i) {
                PartialProductsUP.emplace_back(std::make_unique<mini_OBDM<>>(params, params.MaxPPTruncationUp));
                PartialProductsDOWN.emplace_back(std::make_unique<mini_OBDM<>>(params, params.MaxPPTruncationDown));
            }
            AvailabilityMaskUP.resize(Decompositions, false);
            AvailabilityMaskDOWN.resize(Decompositions, false);
        }

    }

	void Finalise() {
		tmp1_.reset();
		tmp2_.reset();
		for (auto& e : tmp_obdm_)
			e.reset();
		for (auto& e : Storage_)
			e.reset();
		for (auto& e : rDiag_)
			e.reset();
        for (auto& e : PartialProductsUP)
            e.reset();
        for (auto& e : PartialProductsDOWN)
            e.reset();
	}

    void CopyToPP(OBDM& UpIn, OBDM& DownIn, int WriteIndex){
        if (PartialProductsDOWN.size() == 0) {
            if (UpIn.Q.Cols() <= PartialProductsUP[WriteIndex]->Q.MaxCols()) {
                //std::cout << "Copying to PP " << WriteIndex << std::endl;
                PartialProductsUP[WriteIndex]->CopyFromAndTruncate(UpIn);
                AvailabilityMaskUP[WriteIndex] = true;
            }
            else {
                AvailabilityMaskUP[WriteIndex] = false;
            }
        }
        else{
            if (UpIn.Q.Cols() <= PartialProductsUP[WriteIndex]->Q.MaxCols() && DownIn.Q.Cols() <= PartialProductsDOWN[WriteIndex]->Q.MaxCols()) {
                PartialProductsUP[WriteIndex]->CopyFromAndTruncate(UpIn);
                PartialProductsDOWN[WriteIndex]->CopyFromAndTruncate(DownIn);
                AvailabilityMaskUP[WriteIndex] = true;
                AvailabilityMaskDOWN[WriteIndex] = true;
            }
            else {
                AvailabilityMaskUP[WriteIndex] = false;
                AvailabilityMaskDOWN[WriteIndex] = false;
            }
        }
    }

    bool ConstrctFromPP(OBDM& UpIn, OBDM& DownIn, int ReadIndex, double threshold, std::vector<int>& Truncation){
        if (ReadIndex >= PartialProductsUP.size()) {
            return false;
        }

        if (PartialProductsDOWN.size() == 0) {
            if (AvailabilityMaskUP[ReadIndex]) {
                //std::cout << "Copying from PP " << ReadIndex << std::endl;
                UpIn.DecomposeProduct(threshold , *(PartialProductsUP[ReadIndex]), tmp1(), tmp2());
                Truncation[0] = UpIn.Q.Cols();
                return true;
            }
            else {
                return false;
            }
        }
        else{
            if (AvailabilityMaskUP[ReadIndex] && AvailabilityMaskDOWN[ReadIndex]) {
                UpIn.DecomposeProduct(threshold , *(PartialProductsUP[ReadIndex]), tmp1(), tmp2());
                DownIn.DecomposeProduct(threshold , *(PartialProductsDOWN[ReadIndex]), tmp1(), tmp2());
                Truncation[0] = UpIn.Q.Cols();
                Truncation[1] = DownIn.Q.Cols();
                return true;
            }
            else {
                return false;
            }
        }
    }
    
	//performs QR on the first EffectiveCols of matrix Matrix
	template <typename T>
	int PerformTruncatedQR(CMatrixBase<T>& Matrix) {
		std::fill(pivots_.begin(), pivots_.end(), 0);
		int xxxx = 97;
		int const Rows = Matrix.Rows();
		int const Cols = Matrix.Cols();

		LinAlg(zgeqp3)(&Rows, &Cols, Matrix.c_data(), &Rows, pivots_.data(), h_tau_.c_data(),
				work_.c_data(), &lwork_, rwork.data(), &xxxx);
		
		return xxxx;
	}

	template int PerformTruncatedQR(CMatrixBase<Matrix>& Matrix);
	template int PerformTruncatedQR(CMatrixBase<MatrixFFT>& Matrix);


	//Takes a matrix containing Householder vectors and overwrites unitary with Q
	template <typename T>
	int RecoverTruncatedQ(CMatrixBase<T>& Matrix) {
		int xxxx = 97;
		int const Rows = Matrix.Rows();
		int const Cols = Matrix.Cols();

        int const MinRc = std::min(Rows, Cols);

		LinAlg(zungqr)(&Rows, &MinRc, &MinRc, Matrix.c_data(), &Rows, h_tau_.c_data(), work_.c_data(),
				&lwork_, &xxxx);

		return xxxx;
	}

	template int RecoverTruncatedQ(CMatrixBase<Matrix>& Matrix);
	template int RecoverTruncatedQ(CMatrixBase<MatrixFFT>& Matrix);
	
	//Separates the Rows*EffectiveCols output of PerformTruncatedQR into 
	//the EffectiveCols*EffectiveCols Matrix D^-1R and EffectiveCols Vector D
	template <typename T1, typename T2>
	void SeparateTruncatedQR(CMatrixBase<T1> const& matrix, DiagonalMatrix<double>& Dout,
			CMatrixBase<T2>& Rout){

		int const Rows = matrix.Rows();
		int const Cols = matrix.Cols();

        int const MinRC = std::min(Rows, Cols);
        
		#pragma omp parallel for
		for (int i = 0; i < Cols; ++i)
			for (int j = 0; j < Rows; ++j) {
				if (i==j){
					Dout[i] = matrix[j+Rows*i].real();
					Rout[j+MinRC*i] = 1.0;
				}
				else if(j < i)
					Rout[j+MinRC*i] = matrix[j+Rows*i] / matrix[j+Rows*j].real();
				else
					Rout[j+MinRC*i] = 0.0;
			}
            
	}

	template void SeparateTruncatedQR(CMatrixBase<Matrix> const& matrix,
			DiagonalMatrix<double>& Dout, CMatrixBase<Matrix>& Rout);
	template void SeparateTruncatedQR(CMatrixBase<MatrixFFT> const& matrix,
			DiagonalMatrix<double>& Dout, CMatrixBase<Matrix>& Rout);
	template void SeparateTruncatedQR(CMatrixBase<Matrix> const& matrix,
			DiagonalMatrix<double>& Dout, CMatrixBase<MatrixFFT>& Rout);
	template void SeparateTruncatedQR(CMatrixBase<MatrixFFT> const& matrix,
			DiagonalMatrix<double>& Dout, CMatrixBase<MatrixFFT>& Rout);


	//Permutes the Columns of the EffectiveRows*EffectiveCols RPrime matrix after truncation
	//but before multipication with the previous R //Make this general by handig it the pivots
	template <typename T1, typename T2> 
	void PermuteColumns(CMatrixBase<T1>& MatrixOut, CMatrixBase<T2> const& MatrixIn) {
		int const Rows = MatrixIn.Rows();
		int const Cols = MatrixIn.Cols();

		#pragma omp parallel for
		for (int i = 0; i < Cols; ++i)
			std::copy(MatrixIn.cbegin() + i * Rows,
				MatrixIn.cbegin() + (i + 1) * Rows,
				MatrixOut.begin() + (pivots_[i]-1) * Rows);
	}

	template void PermuteColumns(CMatrixBase<Matrix>& MatrixOut,
			CMatrixBase<Matrix> const& MatrixIn);
	template void PermuteColumns(CMatrixBase<MatrixFFT>& MatrixOut,
			CMatrixBase<Matrix> const& MatrixIn);
	template void PermuteColumns(CMatrixBase<Matrix>& MatrixOut,
			CMatrixBase<MatrixFFT> const& MatrixIn);
	template void PermuteColumns(CMatrixBase<MatrixFFT>& MatrixOut,
			CMatrixBase<MatrixFFT> const& MatrixIn);


	//Computes Jacobi SVD of Matrix. Presumably overwrites Matrix and writes the singulars into
	//Diagonal. Does not compute V or Q. 
	template <typename T>
	int ComputeJacobiSVD(CMatrixBase<T>& matrix, DiagonalMatrix<double>& Diagonal) {
		int xxxx = 97;
		int const Rows = matrix.Rows();
		int const Cols = matrix.Cols();

		LinAlg(zgesvj)(&JOBA, &JOBU, &JOBV,
			&Rows, &Cols, matrix.c_data(), &Rows,
			Diagonal.data(), 
			&MV, NULL, &Rows,	// array 'V' is not used. If it fails change NULL for something else
			work_.c_data(), &lwork_, 
			rwork.data(), &lrwork_, &xxxx);

		return xxxx;
	}

	template int ComputeJacobiSVD(CMatrixBase<Matrix>& matrix,DiagonalMatrix<double>& Diagonal);
	template int ComputeJacobiSVD(CMatrixBase<MatrixFFT>& matrix,DiagonalMatrix<double>& Diagonal);


    template <typename T1, typename T2, typename T3>
    int ComputeEigen(CMatrixBase<T1>& MatrixIn, CMatrixBase<T2>& LeftEv, CMatrixBase<T3>& RightEv,
                     DiagonalMatrix<cdouble>& Diagonal) {
        int xxxx = 97;

        int const TruncSize = MatrixIn.Rows();
		int const Cols = MatrixIn.Cols();

        LinAlg(zgeevx)(&BALANC, &JOBVL , &JOBVR, &SENSE, &TruncSize,
            MatrixIn.c_data(), &TruncSize,
            Diagonal.c_data(),
            LeftEv.c_data(), &TruncSize,
            RightEv.c_data(), &TruncSize,
            &ilo_, &ihi_,
            scale_.data(), &ABNRM_, &RCONDE_, &RCONDV_,
            work_.c_data(), &lwork_,
            rwork.data(), &xxxx);


    return xxxx;
    }

    template int ComputeEigen(CMatrixBase<Matrix>& MatrixIn, CMatrixBase<Matrix>& LeftEv, CMatrixBase<Matrix>& RightEv,
                            DiagonalMatrix<cdouble>& Diagonal);
    template int ComputeEigen(CMatrixBase<MatrixFFT>& MatrixIn, CMatrixBase<Matrix>& LeftEv, CMatrixBase<Matrix>& RightEv,
                            DiagonalMatrix<cdouble>& Diagonal);
    template int ComputeEigen(CMatrixBase<Matrix>& MatrixIn, CMatrixBase<MatrixFFT>& LeftEv, CMatrixBase<Matrix>& RightEv,
                            DiagonalMatrix<cdouble>& Diagonal);
    template int ComputeEigen(CMatrixBase<MatrixFFT>& MatrixIn, CMatrixBase<MatrixFFT>& LeftEv, CMatrixBase<Matrix>& RightEv,
                            DiagonalMatrix<cdouble>& Diagonal);
    template int ComputeEigen(CMatrixBase<Matrix>& MatrixIn, CMatrixBase<Matrix>& LeftEv, CMatrixBase<MatrixFFT>& RightEv,
                            DiagonalMatrix<cdouble>& Diagonal);
    template int ComputeEigen(CMatrixBase<MatrixFFT>& MatrixIn, CMatrixBase<Matrix>& LeftEv, CMatrixBase<MatrixFFT>& RightEv,
                            DiagonalMatrix<cdouble>& Diagonal);
    template int ComputeEigen(CMatrixBase<Matrix>& MatrixIn, CMatrixBase<MatrixFFT>& LeftEv, CMatrixBase<MatrixFFT>& RightEv,
                            DiagonalMatrix<cdouble>& Diagonal);
    template int ComputeEigen(CMatrixBase<MatrixFFT>& MatrixIn, CMatrixBase<MatrixFFT>& LeftEv, CMatrixBase<MatrixFFT>& RightEv,
                            DiagonalMatrix<cdouble>& Diagonal);


}	// end namespace WS
