/******************************************************************************
*
*  This Source Code Form is subject to the terms of the Mozilla Public
*  License, v. 2.0. If a copy of the MPL was not distributed with this
*  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*
*******************************************************************************/

#ifndef WORKSPACE_H
#define WORKSPACE_H

#include <array>
#include <vector>
#include <complex>

#include "obdm.h"
#include "myVec.h"
#include "params.h"
#include "matrix.h"
#include "matrix_fft.h"
#include "matrix_base.h"
#include "matrix_diag.h"
#include "linalg-libs.h"

using cdouble = std::complex<double>;

namespace WS {
	MatrixFFT& tmp1();
	Matrix& tmp2();
	DiagonalMatrix<double>& rDiag(int i = 0);

	std::vector<std::unique_ptr<mini_OBDM<>>>& TMP_obdm();
	mini_OBDM<>& tmp_obdm(int i = 0);
	mini_OBDM<>& Storage(int i = 0);
    int UniqueOBDMs();

    mini_OBDM<>& PartialProducts(int spin, int ProductIndex);
    std::vector<bool>& AvailabilityMask(int spin = 0);
    
	int const& lwork();
	int const& lrwork();

	std::vector<int>& pivots();
	myVec<cdouble>& h_tau();

	myVec<cdouble>& work();

    int& ilo();
    int& ihi();
    double& ABNRM();
    double& RCONDE();
    double& RCONDV();

	void Initialise(Parameters const& params);
    void InitialisePP(Parameters const& params);
	void Finalise();

    void CopyToPP(OBDM& UpIn, OBDM& DownIn, int WriteIndex);
    bool ConstrctFromPP(OBDM& UpIn, OBDM& DownIn, int ReadIndex, double threshold, std::vector<int>& Truncation);

	template <typename T>
	int PerformTruncatedQR(CMatrixBase<T>& Matrix);
	template <typename T>
	int RecoverTruncatedQ(CMatrixBase<T>& Matrix);
	template <typename T1, typename T2>
	void SeparateTruncatedQR(CMatrixBase<T1> const& matrix, DiagonalMatrix<double>& Dout,
		CMatrixBase<T2>& Rout);
	template <typename T1, typename T2> 
	void PermuteColumns(CMatrixBase<T1>& MatrixOut, CMatrixBase<T2> const& MatrixIn);

	template <typename T>
	int ComputeJacobiSVD(CMatrixBase<T>& matrix, DiagonalMatrix<double>& Diagonal);

    template <typename T1, typename T2, typename T3>
    int ComputeEigen(CMatrixBase<T1>& MatrixIn, CMatrixBase<T2>& LeftEv, CMatrixBase<T3>& RightEv,
                     DiagonalMatrix<cdouble>& Diagonal);

};	// end namespace WS

#endif
